import { UserEmail, UserId, UserPassword, UserRole, UserStatus, UserType } from "@domains/user";
export declare type CreateUserIn = {
    email: UserEmail;
    password: UserPassword;
    name: string;
    status?: UserStatus;
    role?: UserRole;
};
export declare type SaveUserIn = {
    id: UserId;
    name?: string;
    password?: UserPassword;
    phone?: string;
    position?: string;
    status?: UserStatus;
    role?: UserRole;
    approvedAt?: Date;
    lastLoginTime?: Date;
    deletedAt?: Date;
};
export declare type DeleteUserIn = {
    id: UserId;
    name?: string;
    password?: UserPassword;
    phone?: string;
    position?: string;
    status?: UserStatus;
    role?: UserRole;
    approvedAt?: Date;
    lastLoginTime?: Date;
    deletedAt?: Date;
};
export declare type GetUserIn = Required<UserType>;
export declare type GetUserListIn = Partial<Omit<UserType, "id">> & {
    id: number;
};
