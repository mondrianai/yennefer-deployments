import { WithdrawalUser } from "@domains/user/withdrawal-user";
export declare type CreateWithdrawUserIn = WithdrawalUser;
