import GPUResourceInfo from "core/domains/gpu-resource-info";
export interface ResourceService {
    updateUserGpuResources(PuserId: number, deviceIds: number[], token: string): Promise<{
        userId: number;
        updatedAllocationMap: number[];
    }>;
    getGPUResourcesByUserId(userId: number, { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
}
export default ResourceService;
