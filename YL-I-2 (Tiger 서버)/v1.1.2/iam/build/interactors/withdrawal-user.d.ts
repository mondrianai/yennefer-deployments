import { WithdrawalUser } from "@domains/user/withdrawal-user";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import { User, UserId } from "@domains/user";
import { WithdrawUserInteractorIface, CreateWithdrawUserIn } from "./interfaces/user";
import { WithdrawalUserRepositoryIface, UserRepositoryIface } from "./interfaces/repositories";
import { ResourceService } from "./interfaces/services/resource";
declare class WithdrawalUserInteractor implements WithdrawUserInteractorIface {
    protected withdrawUserRepository: WithdrawalUserRepositoryIface;
    protected userRepository: UserRepositoryIface;
    protected resourceService: ResourceService;
    constructor({ withdrawUserRepository, userRepository, resourceService, }: {
        withdrawUserRepository: WithdrawalUserRepositoryIface;
        userRepository: UserRepositoryIface;
        resourceService: ResourceService;
    });
    withdraw: (user: CreateWithdrawUserIn) => Promise<WithdrawalUser>;
    getListByUserProperty: (pagination: {
        withdrawalPagination: Pagination<WithdrawalUser>;
        userPagination: Pagination<User>;
    }) => Promise<import("yennefer-suite-core/dist/server/list-output").ListOutput<import("./interfaces/user").WithdrawalUserOut>>;
    getById: (userId: UserId) => Promise<WithdrawalUser | null>;
    cancelWithdrawal: (userId: UserId, { token }: {
        token: string;
    }) => Promise<User>;
}
declare const _default: WithdrawalUserInteractor;
export default _default;
