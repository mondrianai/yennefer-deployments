import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class WithdrawalUserController extends Base {
    private userInteractor;
    private withdrawalUserInterctor;
    constructor();
    private convertSearchOptions;
    withdraw: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    withdrawByAdmin: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getList: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    restore: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
}
declare const _default: WithdrawalUserController;
export default _default;
