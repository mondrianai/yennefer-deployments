import SequelizeUserRepository from "./pg/user";
import SequelizeWithdrawalUserRepository from "./pg/withdrawal-user";
import { TransactionType } from "./transaction";
export declare const UserRepository: typeof SequelizeUserRepository;
export declare const WithdrawalUserRepository: typeof SequelizeWithdrawalUserRepository;
export * from "./transaction";
export declare type Options = {
    transaction?: TransactionType;
};
