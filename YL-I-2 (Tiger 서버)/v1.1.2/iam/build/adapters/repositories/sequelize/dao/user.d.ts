import { Model, Optional, BelongsToManyAddAssociationMixin, BelongsToManyGetAssociationsMixin } from "sequelize";
import { User, UserRoleType, UserType, UserStatusType } from "@domains/user";
interface UserCreation extends Optional<UserType, "id" | "createdAt" | "updatedAt" | "role" | "status"> {
}
declare class UserDAO extends Model<User, UserCreation> {
    readonly id: number;
    name: string;
    password: string;
    email: string;
    lastLoginTime: Date;
    status: UserStatusType;
    phone: string;
    position: string;
    role: UserRoleType;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    approvedAt: Date;
    deletedAt: Date;
    addProject: BelongsToManyAddAssociationMixin<any, number>;
    getProjects: BelongsToManyGetAssociationsMixin<any>;
    static associate(models: any): void;
}
export default UserDAO;
