export declare class UserEmail {
    readonly value: string;
    constructor(email: string);
}
export default UserEmail;
