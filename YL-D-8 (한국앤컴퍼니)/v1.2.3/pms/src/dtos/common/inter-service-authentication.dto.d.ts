export declare class InterServiceAuthenticationDto {
    token: string;
    nodeName?: string;
    constructor(token: string, nodeName?: string);
}
