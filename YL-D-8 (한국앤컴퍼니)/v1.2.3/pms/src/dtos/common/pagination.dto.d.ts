export default class PaginationDto {
    limit?: number;
    offset?: number;
    sortBy?: string;
    orderBy?: "asc" | "desc";
    constructor(limit?: number, offset?: number, sortBy?: string, orderBy?: string);
}
