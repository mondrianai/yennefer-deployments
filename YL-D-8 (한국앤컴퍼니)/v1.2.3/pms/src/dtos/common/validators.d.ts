export declare function isValidId(value: number): boolean;
export declare function isValidPathValue(path: string, basePath: string): boolean;
