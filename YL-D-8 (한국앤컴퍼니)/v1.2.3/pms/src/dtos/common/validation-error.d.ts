export declare class ValidationError extends Error {
    key: string;
    expected: string;
    received: string;
    constructor(key: string, expected: string, received: string);
}
