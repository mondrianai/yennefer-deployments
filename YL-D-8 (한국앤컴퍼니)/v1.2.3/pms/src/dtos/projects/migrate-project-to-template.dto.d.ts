export declare class MigrateProjectToTemplateDto {
    projectId: number;
    templateId: number;
    constructor(projectId: number, templateId: number);
}
