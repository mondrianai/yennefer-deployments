export declare class MigrateTemplateToProjectDto {
    projectId: number;
    templateId: number;
    constructor(projectId: number, templateId: number);
}
