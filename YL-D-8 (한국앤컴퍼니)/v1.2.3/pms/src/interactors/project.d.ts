import { ProjectId } from "../core/domains/project";
import { ProjectInteractor, ActivateParams, DeactivateParams, DeleteParams, GetActivatedProjectIdsByUserId, MapActivatedProjectIdsParams, CreateCookieForHubAuthParams } from "./interfaces/project";
import HubUserRepo from "./adapters/repositories/hub-user/hub-user";
import SpawnerRepo from "./adapters/repositories/spawner";
import { HubService } from "./adapters/services/hub";
import { ContainerPlatform } from "./adapters/container";
import { MigrateProjectToTemplateDto } from "../dtos/projects/migrate-project-to-template.dto";
import { FileSystemManager } from "./adapters/services/file-system-manager";
import { MigrateTemplateToProjectDto } from "../dtos/projects/migrate-template-to-project.dto";
import { InterServiceAuthenticationDto } from "../dtos/common/inter-service-authentication.dto";
export declare class ProjectInteractorImpl implements ProjectInteractor {
    private containerPlatform;
    private hubUserRepo;
    private spawnerRepo;
    private hubService;
    private fileSystemManager;
    constructor(containerPlatform: ContainerPlatform, hubUserRepo: HubUserRepo, spawnerRepo: SpawnerRepo, hubService: HubService, fileSystemManager: FileSystemManager);
    private getJhubCookieSecret;
    activate(parameters: ActivateParams): Promise<true>;
    deactivate(parameters: DeactivateParams): Promise<true>;
    delete(parameters: DeleteParams): Promise<boolean>;
    clearDatasets(parameters: {
        projectId: ProjectId;
    }): Promise<unknown[]>;
    mapActivatedProjectIds(parameters: MapActivatedProjectIdsParams): Promise<{
        [key: string]: boolean;
    }>;
    makeRequiredFolder(path: string): Promise<void>;
    private makeDirectoryAccessibleByAnyone;
    getIPynbFileList(parameters: {
        path: string;
    }): Promise<string[]>;
    getActivatedProjectIdsByUserId(parameters: GetActivatedProjectIdsByUserId): Promise<import("yennefer-suite-core/dist/server/list-output").ListOutput<number>>;
    createCookieForHubAuth(parameters: CreateCookieForHubAuthParams): Promise<string | null>;
    migrateProjectToTemplate(migrateProjectToTemplateDto: MigrateProjectToTemplateDto, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
    migrateTemplateToProject(migrateTemplateToProjectDto: MigrateTemplateToProjectDto, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
}
