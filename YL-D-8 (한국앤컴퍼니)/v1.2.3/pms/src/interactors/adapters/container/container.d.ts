import { ContainerImage } from "core/domains/container/image";
import { Container } from "../../../core/domains/container";
import { StartType, StopType, RemoveType, ImageTaggingInfo } from "./container.in";
export declare type ConnectionProtocol = "http" | "https" | "ssh";
export interface ContainerPlatform {
    start: (parameters: StartType) => Promise<unknown>;
    stop: (parameters: StopType) => Promise<unknown>;
    remove: (parameters: RemoveType) => Promise<unknown>;
    findContainerByProjectId(projectId: number): Promise<Container | null>;
    commit(container: Container, projectId: number): Promise<{
        createdImage: {
            id: string;
        };
        previousImage: {
            id: string;
            isProjectSpecific: boolean;
        };
    }>;
    createTag(imageId: string, newTaggingInfo: ImageTaggingInfo): Promise<string>;
    removeImage(imageId: string): Promise<string>;
    findProjectImageById(projectId: number): Promise<ContainerImage | null>;
    findTemplateImageById(templateId: number): Promise<ContainerImage | null>;
}
