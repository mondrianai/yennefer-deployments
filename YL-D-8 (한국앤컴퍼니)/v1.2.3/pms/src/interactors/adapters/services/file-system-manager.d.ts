import { InterServiceAuthenticationDto } from "../../../dtos/common/inter-service-authentication.dto";
export interface FileSystemManager {
    copyToTemplate(projectId: number, templateId: number, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
    copyToProject(templateId: number, projectId: number, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
}
