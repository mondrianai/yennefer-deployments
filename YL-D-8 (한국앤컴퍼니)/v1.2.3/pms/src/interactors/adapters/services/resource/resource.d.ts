import { AssignResourceIn, FreeResourceIn, GetByIdIn, GetListIn } from "./resource.in";
import { AssignResourceOut, FreeResourceOut, GetByIdOut, GetListOut } from "./resource.out";
export interface ResourceService {
    getById(parameters: GetByIdIn): Promise<GetByIdOut>;
    getList(parameters: GetListIn): Promise<GetListOut>;
    assignResource(parameters: AssignResourceIn): Promise<AssignResourceOut>;
    freeResource(parameters: FreeResourceIn): Promise<FreeResourceOut>;
}
export default ResourceService;
