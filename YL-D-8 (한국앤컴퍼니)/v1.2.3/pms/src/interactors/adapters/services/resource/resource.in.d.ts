import { ProjectId, ProjectNodeName } from "yennefer-suite-core/dist/domains/studio/project";
import { ResourceId } from "yennefer-suite-core/dist/domains/studio/resource";
export declare type GetByIdIn = {
    token: string;
    nodeName: ProjectNodeName | string;
    resourceId: ResourceId | number;
};
export declare type GetListIn = {
    token: string;
    nodeName: ProjectNodeName | string;
};
export declare type AssignResourceIn = {
    token: string;
    nodeName: ProjectNodeName | string;
    projectId: ProjectId | number;
    resourceId: ResourceId | number;
};
export declare type FreeResourceIn = {
    token: string;
    nodeName: ProjectNodeName | string;
    projectId: ProjectId | number;
};
