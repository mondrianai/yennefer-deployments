import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import { UserId } from "../../../core/domains/user";
import { ProjectId } from "../../../core/domains/project";
export interface SpawnerRepo {
    isRunning(projectId: ProjectId): Promise<boolean>;
    filterActivatedProjectIds(projects: Array<ProjectId>): Promise<ListOutput<number>>;
    mapActivatedProjectIds(projectIds: Array<ProjectId>): Promise<{
        [key: string]: boolean;
    }>;
    getAllActivatedProjectIds(): Promise<ListOutput<number>>;
    getActivatedProjectIdsByUserIds(userIds: Array<UserId> | null): Promise<ListOutput<number>>;
}
export default SpawnerRepo;
