import { AssignedInfo, ContainerImage, ProjectId, VolumeInfos } from "../../../core/domains/project";
import { UserId, UserRole } from "../../../core/domains/user";
export declare type ActivateParams = {
    userId: UserId;
    projectId: ProjectId;
    image: ContainerImage;
    role: UserRole;
    volumes?: VolumeInfos;
    assignedInfo?: AssignedInfo;
};
export declare type DeactivateParams = {
    userId: UserId;
    projectId: ProjectId;
    role: UserRole;
    remove?: boolean;
};
export declare type DeleteParams = {
    userId: UserId;
    projectId: ProjectId;
};
export declare type MapActivatedProjectIdsParams = {
    projectIds: Array<ProjectId>;
};
export declare type GetIPynbFileListParams = {
    path: string;
};
export declare type GetActivatedProjectIdsByUserId = {
    userIds: Array<UserId> | null;
};
export declare type CreateCookieForHubAuthParams = {
    userId: UserId;
};
