import { InterServiceAuthenticationDto } from "dtos/common/inter-service-authentication.dto";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { MigrateProjectToTemplateDto } from "../../../dtos/projects/migrate-project-to-template.dto";
import { MigrateTemplateToProjectDto } from "../../../dtos/projects/migrate-template-to-project.dto";
import { ActivateParams, DeactivateParams, DeleteParams, GetIPynbFileListParams, MapActivatedProjectIdsParams, CreateCookieForHubAuthParams, GetActivatedProjectIdsByUserId } from "./project.in";
export interface ProjectInteractor {
    activate: (parameters: ActivateParams) => Promise<boolean>;
    deactivate: (parameters: DeactivateParams) => Promise<boolean>;
    delete: (parameters: DeleteParams) => Promise<boolean>;
    getIPynbFileList(parameters: GetIPynbFileListParams): Promise<Array<string>>;
    getActivatedProjectIdsByUserId(parameters: GetActivatedProjectIdsByUserId): Promise<ListOutput<number>>;
    makeRequiredFolder(path: string): Promise<void>;
    mapActivatedProjectIds(parameters: MapActivatedProjectIdsParams): Promise<{
        [key: string]: boolean;
    }>;
    createCookieForHubAuth(parameters: CreateCookieForHubAuthParams): Promise<string | null>;
    migrateProjectToTemplate(migrateProjectToTemplateDto: MigrateProjectToTemplateDto, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
    migrateTemplateToProject(migrateTemplateToProjectDto: MigrateTemplateToProjectDto, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
}
