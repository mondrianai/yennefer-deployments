import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
import { ProjectInteractor } from "../../../interactors/interfaces/project";
export declare class ProjectsController extends Base {
    private projectInteractor;
    constructor(projectInteractor: ProjectInteractor);
    private checkProjectAuth;
    private commonErrorException;
    getProjectAuthToken(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    createCookieByProjectOwnerId(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    activate(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    deactivate(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    delete(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    getActivatedList(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    initWorkspace(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    migrateProjectToTemplate(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    migrateTemplateToProject(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
}
declare const _default: ProjectsController;
export default _default;
