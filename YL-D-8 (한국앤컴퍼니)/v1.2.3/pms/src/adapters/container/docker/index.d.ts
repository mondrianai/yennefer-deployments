import { ContainerImage } from "../../../core/domains/container/image";
import { Container } from "../../../core/domains/container";
import { ContainerPlatform, StartType, StopType, RemoveType, ImageTaggingInfo } from "../../../interactors/adapters/container";
export default class DockerContainerPlatform implements ContainerPlatform {
    private dockerInstance;
    private namePrefix?;
    constructor();
    start(parameters: StartType): Promise<any>;
    stop(parameters: StopType): Promise<any>;
    remove(parameters: RemoveType): Promise<void>;
    /**
     * 이름으로 컨테이너 필터링 해서 출력
     * @param userIdNumber {string} 사용자 ID
     * @param projectNumber {string} 프로젝트 ID
     * @returns 주피터 컨테이너
     */
    private getByName;
    /**
     * 이름으로 컨테이너 필터링 해서 출력
     * @param userIdNumber {string} 사용자 ID
     * @param projectNumber {string} 프로젝트 ID
     * @returns 주피터 컨테이너
     */
    private getContainerByUserIdAndProjectNumber;
    findContainerByProjectId(projectId: number): Promise<Container | null>;
    commit(container: Container, projectId: number): Promise<{
        createdImage: {
            id: string;
        };
        previousImage: {
            id: string;
            isProjectSpecific: boolean;
        };
    }>;
    createTag(imageId: string, newTaggingInfo: ImageTaggingInfo): Promise<string>;
    removeImage(imageId: string): Promise<string>;
    findProjectImageById(projectId: number): Promise<ContainerImage | null>;
    findTemplateImageById(templateId: number): Promise<ContainerImage | null>;
}
