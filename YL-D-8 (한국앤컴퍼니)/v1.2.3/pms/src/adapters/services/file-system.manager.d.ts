import { InterServiceAuthenticationDto } from "../../dtos/common/inter-service-authentication.dto";
import { FileSystemManager } from "../../interactors/adapters/services/file-system-manager";
export declare class HttpFileSystemManagerService implements FileSystemManager {
    private instance;
    constructor();
    copyToTemplate(projectId: number, templateId: number, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
    copyToProject(templateId: number, projectId: number, interServiceAuthenticationDto: InterServiceAuthenticationDto): Promise<unknown>;
}
