import { FileSystemManager } from "../../interactors/adapters/services/file-system-manager";
export declare class DummyFileSystemManager implements FileSystemManager {
    copyToProject(templateId: number, projectId: number): Promise<unknown>;
    copyToTemplate(projectId: number, templateId: number): Promise<unknown>;
}
