import { UserId } from "../../core/domains/user";
import { ActivateParams, DeactivateParams, HubService } from "../../interactors/adapters/services/hub";
export declare class AxiosHubService implements HubService {
    private url;
    constructor();
    protected getHeader: () => {
        Referer: string;
    };
    login: (userId: UserId) => Promise<void>;
    activate: (parameters: ActivateParams) => Promise<boolean>;
    deactivate: (parameters: DeactivateParams) => Promise<boolean>;
}
declare const _default: AxiosHubService;
export default _default;
