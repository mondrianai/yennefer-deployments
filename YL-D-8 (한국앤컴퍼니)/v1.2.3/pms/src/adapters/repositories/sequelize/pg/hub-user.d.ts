import { GetHubUserByUserIdParams, UpdateLastActivityParams } from "../../../../interactors/adapters/repositories/hub-user";
import { User as HubUser } from "../../../../core/domains/hub-user";
import HubUserDAO from "./dao/hub-user";
import Base from "../../../../interactors/adapters/repositories/hub-user/hub-user";
export default class HubUserRepo implements Base {
    hubUserDAO: typeof HubUserDAO;
    constructor();
    getHubUserByUserId: (userId: GetHubUserByUserIdParams) => Promise<HubUser | null>;
    updateLastActivity: (parameters: UpdateLastActivityParams) => Promise<HubUserDAO>;
}
