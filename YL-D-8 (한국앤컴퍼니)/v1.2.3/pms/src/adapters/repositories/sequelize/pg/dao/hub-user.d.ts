/// <reference types="node" />
import { Model, Optional } from "sequelize";
import { User } from "yennefer-suite-core/dist/domains/hub/user";
interface UserCreation extends Optional<User, "id"> {
}
export declare class UserDAO extends Model<Optional<User, "userInfo">, UserCreation> {
    readonly id: number;
    name: string;
    admin: boolean;
    state: string;
    encryptedAuthState: Buffer;
    cookieId: string;
    readonly created: Date;
    readonly lastActivity: Date;
    static associate(_models: any): void;
}
declare class DAO extends UserDAO {
}
export default DAO;
