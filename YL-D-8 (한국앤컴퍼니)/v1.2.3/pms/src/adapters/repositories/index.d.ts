import SequelizePgHubUserRepo from "./sequelize/pg/hub-user";
import SequelizePgSpawnerRepo from "./sequelize/pg/spawner";
export declare const HubUserRepoImpl: SequelizePgHubUserRepo;
export declare const SpawnerRepoImpl: SequelizePgSpawnerRepo;
