import { Sequelize, Transaction as SequelizeTransaction } from "sequelize";
export declare type TransactionType = SequelizeTransaction;
export declare class Transaction {
    transaction?: TransactionType;
    instance: Sequelize;
    constructor(instance: Sequelize);
    start: () => Promise<TransactionType>;
    commit: () => Promise<boolean>;
    rollback: () => Promise<boolean>;
}
