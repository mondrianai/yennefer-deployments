/// <reference types="node" />
export interface UserType {
    id: number;
    name: string;
    admin: boolean;
    state: string;
    encryptedAuthState?: Buffer;
    cookieId: string;
    lastActivity: Date;
    created: Date;
}
export declare class User {
    readonly id: number;
    name: string;
    admin: boolean;
    state: string;
    encryptedAuthState?: Buffer;
    cookieId: string;
    lastActivity?: Date;
    created: Date;
    constructor(parameters: UserType);
    static validate(object: any): object is User;
}
export default User;
