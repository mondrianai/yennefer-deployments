export declare type VolumeInfoType = {
    bind: string;
    mode: string;
};
export declare type VolumeInfosType = {
    [key: string]: VolumeInfoType;
};
export declare class VolumeInfo {
    readonly bind: string;
    readonly mode: string;
    constructor(parameters: VolumeInfoType);
    get value(): {
        bind: string;
        mode: string;
    };
    static validate(object: any): boolean;
}
export declare class VolumeInfos {
    readonly volumeInfos: {
        [key: string]: VolumeInfo;
    };
    constructor(parameters: VolumeInfosType);
    get value(): {};
    static validate(object: any): boolean;
}
