export declare type AssignedInfoType = {
    cpu?: any;
    ram?: any;
    gpu?: any;
};
export declare class AssignedInfo {
    readonly cpu?: any;
    readonly ram?: any;
    readonly gpu?: any;
    constructor(parameters: AssignedInfoType);
    get value(): {
        cpu: any;
        ram: any;
        gpu: any;
    };
    static validate(object: any): boolean;
}
