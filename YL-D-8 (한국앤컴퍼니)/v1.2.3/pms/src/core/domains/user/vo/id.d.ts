export declare class UserId {
    readonly value: number;
    constructor(value: number);
    static validate(object: any): boolean;
}
export default UserId;
