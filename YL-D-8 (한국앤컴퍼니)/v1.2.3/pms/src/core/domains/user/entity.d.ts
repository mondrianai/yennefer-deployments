import { UserRole, UserRoleType, UserId, UserPassword, UserEmail } from "./vo";
declare type UserType = {
    id: number;
    password?: string;
    name: string;
    email: string;
    lastLoginTime?: Date;
    isAllowed?: boolean;
    phone?: string;
    position?: string;
    role?: UserRoleType;
    createdAt: Date;
    updatedAt: Date;
    approvedAt?: Date;
};
export declare class User {
    readonly id: UserId;
    password?: UserPassword;
    name?: string;
    email?: UserEmail;
    lastLoginTime?: Date;
    isAllowed?: boolean;
    phone?: string;
    position?: string;
    role: UserRole;
    createdAt?: Date;
    updatedAt?: Date;
    approvedAt?: Date;
    constructor(parameters: UserType);
    get isSupervisor(): boolean;
    get isAdmin(): boolean;
}
export default User;
