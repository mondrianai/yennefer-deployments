export declare type SpawnerType = {
    id?: number;
    userId?: number;
    serverId?: number;
    state?: string;
    name?: string;
    started?: Date;
    lastActivity?: Date;
    userOptions?: string;
};
export declare class Spawner {
    id?: number;
    userId?: number;
    serverId?: number;
    state?: string;
    name?: string;
    started?: Date;
    lastActivity?: Date;
    userOptions?: string;
    constructor(parameters: SpawnerType);
    static verify(object: any): object is Spawner;
}
export default Spawner;
