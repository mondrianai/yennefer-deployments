export declare class Container {
    id: string;
    name: string;
    image: string;
    isRunning: boolean;
    constructor({ id, name, image, isRunning, }: {
        id: string;
        name: string;
        image: string;
        isRunning?: boolean;
    });
}
