export declare type JSendStatus = "error" | "failed" | "success";
export interface JSendBaseObject {
    status: JSendStatus;
}
export interface JSendErrorResponse<T> extends JSendBaseObject {
    status: "error";
    message: string;
    code?: number;
    data?: T;
}
export interface JSendSuccessResponse<T> extends JSendBaseObject {
    status: "success";
    data: T | null;
}
export interface JSendFailureResponse<T> extends JSendBaseObject {
    status: "failed";
    data: T | null;
}
export declare type JSendResponse<T> = JSendErrorResponse<T> | JSendSuccessResponse<T> | JSendFailureResponse<T>;
