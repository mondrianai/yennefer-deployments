declare const _default: {
    clearMocks: boolean;
    coverageDirectory: string;
    coverageProvider: string;
    coverageReporters: string[];
    moduleFileExtensions: string[];
    testEnvironment: string;
    transformIgnorePatterns: string[];
};
export default _default;
