export default abstract class WebServer {
    framework: any;
    port: number;
    constructor(port: number);
    abstract startServer(): void;
}
