export declare type ListOutput<T> = {
    count: number;
    data: Array<T>;
};
export default ListOutput;
