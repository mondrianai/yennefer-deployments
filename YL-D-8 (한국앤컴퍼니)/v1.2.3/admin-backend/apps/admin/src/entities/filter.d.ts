export interface UserListFilter {
    isAdmin?: boolean;
    isAuthorized?: boolean;
}
export interface QueryOption {
    search?: string;
    isActivated?: boolean;
    orderBy?: string;
    limit?: number;
    offset?: number;
}
