export interface ResourceType {
    time: Date;
    cpu: {
        total: number;
        used: number;
    };
    memory: {
        total: number;
        used: number;
    };
    storage: {
        total: number;
        used: number;
    };
    gpu: {
        total: number;
        used: number;
    };
    nodeId: number;
}
export interface ProjectResourceType extends ResourceType {
    projectId: number;
    traffic: {
        in: number;
        out: number;
    };
}
export interface GPUAllocationInfo {
    time: Date;
    gpuId: number;
    projectId: number | null;
    ownerId: number | null;
    total: number;
    used: number;
    nodeId: number;
}
