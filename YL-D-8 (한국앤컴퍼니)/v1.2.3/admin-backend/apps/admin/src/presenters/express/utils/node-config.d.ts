import { NodeWithServices } from "yennefer-suite-core/dist/domains/admin/node";
declare class NodeConfig {
    private nodeList;
    constructor();
    getNodeInfoFromKey(key: string): NodeWithServices | undefined;
}
declare const _default: NodeConfig;
export default _default;
