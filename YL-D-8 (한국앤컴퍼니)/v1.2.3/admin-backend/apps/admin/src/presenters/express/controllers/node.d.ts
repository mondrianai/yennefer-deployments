import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class NodeController extends Base {
    create: (req: Request, res: Response) => Promise<any>;
    getListByService: (req: Request, res: Response) => Promise<any>;
    updateById: (req: Request, res: Response) => Promise<any>;
    deleteById: (req: Request, res: Response) => Promise<any>;
}
declare const _default: NodeController;
export default _default;
