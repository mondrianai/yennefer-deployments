import { Request, Response } from "express";
export declare function getHardwareResourceCtrl(req: Request, res: Response): Promise<jsend.JSendObject>;
export declare function getProjectWithResourceByIdCtrl(req: Request, res: Response): Promise<jsend.JSendObject>;
export declare function getGPUInformationCtrl(req: Request, res: Response): Promise<jsend.JSendObject>;
export declare function addGPUAllocationInfo(req: Request, res: Response): Promise<jsend.JSendObject>;
export declare function bulkAddGPUAllocationInfo(req: Request, res: Response): Promise<jsend.JSendObject>;
