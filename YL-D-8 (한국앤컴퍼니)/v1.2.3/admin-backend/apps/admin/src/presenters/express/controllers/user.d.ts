import { Request, Response } from "express";
export declare function getUserListCtrl(req: Request, res: Response): Promise<jsend.JSendObject>;
export declare function approveUserCtrl(req: Request, res: Response): Promise<void | jsend.JSendObject>;
export declare function removeApprovalUserCtrl(req: Request, res: Response): Promise<void | jsend.JSendObject>;
export declare function grantAdminPrivilegesCtrl(req: Request, res: Response): Promise<void | jsend.JSendObject>;
export declare function revokeAdminPrivilegesCtrl(req: Request, res: Response): Promise<void | jsend.JSendObject>;
export declare function changeAdminPasswordCtrl(req: Request, res: Response): Promise<void | jsend.JSendObject>;
