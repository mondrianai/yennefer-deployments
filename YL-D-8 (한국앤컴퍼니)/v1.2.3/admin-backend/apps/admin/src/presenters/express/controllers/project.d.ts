import { Request, Response } from "express";
export declare function getProjectsWithResourceUsageCtrl(req: Request, res: Response): Promise<Response<any, Record<string, any>> | jsend.JSendObject>;
export declare function requestStartingProjectCtrl(req: Request, res: Response): Promise<Response<any, Record<string, any>> | jsend.JSendObject>;
export declare function requestStoppingProjectCtrl(req: Request, res: Response): Promise<Response<any, Record<string, any>> | jsend.JSendObject>;
export declare function getProjectWithOwnerCtrl(req: Request, res: Response): Promise<Response<any, Record<string, any>> | jsend.JSendObject>;
export declare function getLogsByProjectIdCtrl(req: Request, res: Response): Promise<Response<any, Record<string, any>> | jsend.JSendObject>;
