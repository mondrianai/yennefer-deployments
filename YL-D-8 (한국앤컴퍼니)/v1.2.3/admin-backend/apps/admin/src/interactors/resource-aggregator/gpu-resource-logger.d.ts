export default class GPULogAggregator {
    aggregate(commandExists: boolean, baseTime?: Date): Promise<({
        projectId: number;
        ownerId: null;
        time: Date;
        gpuId: number;
        total: number;
        used: number;
    } | {
        projectId: null;
        ownerId: null;
        time: Date;
        gpuId: number;
        total: number;
        used: number;
    })[] | null>;
    private getNvidiaStatusWithPID;
    private getDockerContainerListWithPID;
}
