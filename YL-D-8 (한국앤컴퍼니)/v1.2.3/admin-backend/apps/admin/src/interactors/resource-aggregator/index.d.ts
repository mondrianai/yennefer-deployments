export default class AggregateResourceLogs {
    constructor();
    execute(isNvidiaGPUAvailable: boolean): Promise<void>;
    private projectLogAggregator;
    private hardwareLogAggregator;
    private gpuLogAggregator;
}
