import { ContainerStats } from "dockerode";
export interface DockerContainerStats extends ContainerStats {
    name: string;
}
export interface DockerDF {
    name: string | undefined;
    size: number | undefined;
}
export interface Volumes {
    Name: string;
    UsageData: {
        Size: number;
    };
}
export interface DockerTopResult {
    Titles: ["UID", "PID"];
    Processes: [[string, string, string, string, string, string, string, string]];
}
