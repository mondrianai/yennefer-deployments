import NodeInteractor from "./node";
import ServiceInteractor from "./service";
export declare const nodeInteractor: NodeInteractor;
export declare const serviceInteractor: ServiceInteractor;
declare const _default: {
    nodeInteractor: NodeInteractor;
    serviceInteractor: ServiceInteractor;
};
export default _default;
