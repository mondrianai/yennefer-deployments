import User from "@yennefer/entities/user";
export default function changeAdminPassword(userId: number, password: string, token: string): Promise<User | null>;
