import User from "@yennefer/entities/user";
export declare function grantAdminPrivileges(id: number, token: string): Promise<User | null>;
export declare function revokeAdminPrivileges(id: number, token: string): Promise<User | null>;
