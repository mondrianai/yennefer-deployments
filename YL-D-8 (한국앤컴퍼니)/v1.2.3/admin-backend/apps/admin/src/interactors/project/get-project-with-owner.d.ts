import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import User from "@yennefer/entities/user";
import Project from "@yennefer/entities/project";
export default function getProjectWithOwner(id: number, { token, node }: {
    token: string;
    node: Node;
}): Promise<{
    project: Project;
    user: User;
} | null>;
