import { Node } from "yennefer-suite-core/dist/domains/admin/node";
export default function getLogsByProjectId(id: number, { node }: {
    node: Node;
}): Promise<{
    time: Date;
    cpu: {
        total: number;
        used: number;
    };
    memory: {
        total: number;
        used: number;
    };
    storage: {
        total: number;
        used: number;
    };
    gpu: {
        total: number;
        used: number;
    };
    traffic: {
        in: number;
        out: number;
    };
    projectId: number;
    nodeId: number;
}[]>;
