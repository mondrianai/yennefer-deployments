import { Node } from "yennefer-suite-core/dist/domains/admin/node";
export declare function requestStartingProject(projectId: number, userId: number, node?: Node): Promise<boolean>;
export declare function requestStoppingProject(projectId: number, userId: number, node?: Node): Promise<boolean>;
