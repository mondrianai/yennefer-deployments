import { GPUAllocationInfo } from "../../entities/resource";
export declare function createGPUAllocationInfo(info: GPUAllocationInfo): Promise<import("../../adapters/databases/models/gpuAllocationInfo").default>;
export declare function bulkCreateGPUAllocationInfo(infoList: GPUAllocationInfo[], token: string): Promise<import("../../adapters/databases/models/gpuAllocationInfo").default[]>;
