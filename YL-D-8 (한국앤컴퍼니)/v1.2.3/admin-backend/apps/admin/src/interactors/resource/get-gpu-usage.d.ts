import { Node } from "yennefer-suite-core/dist/domains/admin/node";
export default function getGPUUsage(token: string, node?: Node): Promise<{
    activatedTime: Date | null | undefined;
    time: Date;
    gpuId: number;
    projectId: number | null;
    ownerId: number | null;
    total: number;
    used: number;
    nodeId: number;
}[]>;
