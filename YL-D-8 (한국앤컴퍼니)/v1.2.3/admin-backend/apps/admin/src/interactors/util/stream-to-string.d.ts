/// <reference types="node" />
import { Readable } from "stream";
export default function streamToString(readable: Readable): Promise<string>;
