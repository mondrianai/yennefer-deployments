export declare function endsWith(compareTo: string | RegExp, value: string): boolean;
export declare function testJupyterNameMatch(name: string): boolean;
export declare const match: (regex: RegExp) => (str: string | null) => string | null;
