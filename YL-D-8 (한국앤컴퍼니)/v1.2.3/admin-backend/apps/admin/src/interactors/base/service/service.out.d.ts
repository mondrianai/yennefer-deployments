import { Service } from "yennefer-suite-core/dist/domains/admin/service";
export declare type ServiceCreateOut = Service;
export declare type ServiceGetByPropertyOut = Service;
export declare type ServiceUpdateByIdOut = Service;
export declare type ServiceDeleteByPropertyOut = number;
