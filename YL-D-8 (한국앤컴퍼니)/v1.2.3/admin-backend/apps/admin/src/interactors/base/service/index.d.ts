import { ServiceCreateIn, ServiceGetByPropertyIn, ServiceUpdateByIdIn, ServiceDeleteByPropertyIn } from "./service.in";
import { ServiceCreateOut, ServiceGetByPropertyOut, ServiceUpdateByIdOut, ServiceDeleteByPropertyOut } from "./service.out";
export default interface ServiceInteractorBase {
    create(parameters: ServiceCreateIn): Promise<ServiceCreateOut>;
    getByProperty(parameters: ServiceGetByPropertyIn): Promise<ServiceGetByPropertyOut>;
    updateById(parameters: ServiceUpdateByIdIn): Promise<ServiceUpdateByIdOut>;
    deleteByProperty(parameters: ServiceDeleteByPropertyIn): Promise<ServiceDeleteByPropertyOut>;
}
