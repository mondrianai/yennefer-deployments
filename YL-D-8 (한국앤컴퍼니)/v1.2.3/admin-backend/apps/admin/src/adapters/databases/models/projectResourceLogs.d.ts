import { Model } from "sequelize";
interface ProjectResourceLogAttributes {
    time: Date;
    cpuTotal: number;
    cpuUsed: number;
    memTotal: number;
    memUsed: number;
    storageTotal: number;
    storageUsed: number;
    gpuTotal: number;
    gpuUsed: number;
    trafficIn: number;
    trafficOut: number;
    projectId: number;
    nodeId: number;
}
interface ProjectResourceLogCreationAttributes extends ProjectResourceLogAttributes {
}
export default class ProjectResourceLogsModel extends Model<ProjectResourceLogAttributes, ProjectResourceLogCreationAttributes> implements ProjectResourceLogAttributes {
    readonly time: Date;
    cpuTotal: number;
    cpuUsed: number;
    memTotal: number;
    memUsed: number;
    storageTotal: number;
    storageUsed: number;
    gpuTotal: number;
    gpuUsed: number;
    trafficIn: number;
    trafficOut: number;
    projectId: number;
    nodeId: number;
}
export {};
