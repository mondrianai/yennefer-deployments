import { QueryOption } from "../../../entities/filter";
import ProjectResourceLogsModel from "../models/projectResourceLogs";
export interface ProjectResourceRepository {
    findAllByProjectId: (id: number) => Promise<ProjectResourceLogsModel[]>;
    findAllWithCount: (options?: QueryOption) => Promise<{
        rows: ProjectResourceLogsModel[];
        count: number;
    }>;
    findLatestLogsByIdArray: (ids: number[]) => Promise<ProjectResourceLogsModel[]>;
}
