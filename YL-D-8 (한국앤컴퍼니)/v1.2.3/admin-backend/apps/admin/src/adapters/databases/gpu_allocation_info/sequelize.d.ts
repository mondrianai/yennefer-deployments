import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import { GPUAllocationInfo } from "../../../entities/resource";
import GPUAllocationInfoModel from "../models/gpuAllocationInfo";
import { GPUAllocationInfoRepository } from "./interface";
export default class SequelizeGPUAllocationInfoRepository implements GPUAllocationInfoRepository {
    findAll: (node?: Node | undefined) => Promise<GPUAllocationInfoModel[]>;
    createGPUAllocationLog: (info: GPUAllocationInfo) => Promise<GPUAllocationInfoModel>;
    bulkCreateGPUAllocationLog: (infoList: GPUAllocationInfo[]) => Promise<GPUAllocationInfoModel[]>;
}
