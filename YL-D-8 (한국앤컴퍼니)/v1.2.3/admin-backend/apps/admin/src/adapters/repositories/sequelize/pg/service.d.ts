import { Service } from "yennefer-suite-core/dist/domains/admin/service";
import { ServiceCreateIn, ServiceGetByPropertyIn, ServiceUpdateByIdIn, ServiceDeleteByPropertyIn } from "../../base/service/service.in";
import Base from "../../base/service";
export default class ServiceRepository implements Base {
    create: ({ service }: ServiceCreateIn) => Promise<Service>;
    getByProperty: ({ service, condition, }: ServiceGetByPropertyIn) => Promise<Service>;
    updateById: ({ service }: ServiceUpdateByIdIn) => Promise<Service>;
    deleteByProperty: ({ service, condition, }: ServiceDeleteByPropertyIn) => Promise<number>;
}
