import SequelizePgNodeRepository from "./sequelize/pg/node";
import SequelizePgServiceRepository from "./sequelize/pg/service";
export declare const nodeRepository: SequelizePgNodeRepository;
export declare const serviceRepository: SequelizePgServiceRepository;
declare const _default: {
    nodeRepository: SequelizePgNodeRepository;
    serviceRepository: SequelizePgServiceRepository;
};
export default _default;
