import { Node, NodeWithServices } from "yennefer-suite-core/dist/domains/admin/node";
import { NodeCreateIn, NodeGetByPropertyIn, NodeGetListByPropertyIn, NodeUpdateByIdIn, NodeDeleteByPropertyIn } from "../../base/node/node.in";
import Base from "../../base/node";
export default class NodeRepository implements Base {
    create: ({ node }: NodeCreateIn) => Promise<Node>;
    getByProperty: ({ node, condition }: NodeGetByPropertyIn) => Promise<Node>;
    getListByProperty: ({ pagination, node, condition, service, }: NodeGetListByPropertyIn) => Promise<{
        count: number;
        data: NodeWithServices[];
    }>;
    updateById: ({ node }: NodeUpdateByIdIn) => Promise<Node>;
    deleteByProperty: ({ node, condition, }: NodeDeleteByPropertyIn) => Promise<number>;
}
