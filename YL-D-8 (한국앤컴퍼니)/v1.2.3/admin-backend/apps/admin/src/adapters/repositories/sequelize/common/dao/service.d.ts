import { Model, Optional } from "sequelize";
import { Service, ServiceVO } from "yennefer-suite-core/dist/domains/admin/service";
import { YenneferServiceType } from "yennefer-suite-core/dist/domains/admin/service/common/yennefer-service";
interface ServiceCreation extends Optional<ServiceVO, "id"> {
}
declare class ServiceDAO extends Model<Service, ServiceCreation> {
    readonly id: number;
    nodeId: number;
    port: number;
    name: YenneferServiceType;
    description: string;
    static associate(): void;
}
export default ServiceDAO;
