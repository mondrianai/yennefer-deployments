export declare const hash: (value: string) => Promise<string>;
export declare const validateWithHash: (value: string, hash: string) => Promise<boolean>;
