/// <reference types="node" />
import { RmOptions, RmDirOptions, PathLike } from "fs";
export declare const asyncReadFile: (filepath: string) => Promise<string | Buffer>;
export declare const asyncReadDir: (dirpath: string) => Promise<Array<string>>;
export declare function asyncMkdir(path: PathLike): Promise<unknown>;
export declare function asyncExec(cmd: string): Promise<string>;
export declare function asyncRmdir(path: string, options?: RmDirOptions): Promise<unknown>;
export declare function asyncRm(path: string, options?: RmOptions): Promise<unknown>;
