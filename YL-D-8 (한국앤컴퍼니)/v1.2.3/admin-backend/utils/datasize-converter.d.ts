export declare const toByte: (data: string) => number;
export declare function convertMiBtoBytes(mib: string): number;
export default toByte;
