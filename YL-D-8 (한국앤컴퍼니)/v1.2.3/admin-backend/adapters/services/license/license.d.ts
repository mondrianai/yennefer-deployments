import { AxiosBase } from "../core/base";
declare class LicenseService extends AxiosBase {
    constructor();
    getLicenseResult: () => Promise<boolean>;
}
declare const _default: LicenseService;
export default _default;
