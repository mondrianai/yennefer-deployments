import { AxiosRequestConfig } from "axios";
import { JSendResponse } from "@yennefer/core/src/presenters/web-server/express/controller/base";
export declare type APIConfig = {
    token: string;
    nodeKey?: string;
    baseUrl?: string;
};
export declare class AxiosBase {
    private uri;
    constructor(uri?: string);
    private responseErrorHandler;
    protected download: <T>(path: string, endpoint: string, config?: AxiosRequestConfig | undefined) => Promise<JSendResponse<T>>;
    protected get: <T>(endpoint: string, config?: AxiosRequestConfig | undefined) => Promise<JSendResponse<T>>;
    protected post: <T>(endpoint: string, data?: unknown, config?: AxiosRequestConfig | undefined) => Promise<JSendResponse<T>>;
    protected put: <T>(endpoint: string, data?: unknown, config?: AxiosRequestConfig | undefined) => Promise<JSendResponse<T>>;
    protected delete: <T>(endpoint: string, config?: AxiosRequestConfig | undefined) => Promise<JSendResponse<T>>;
    protected customRequest: <T>(config: AxiosRequestConfig) => Promise<JSendResponse<T>>;
}
