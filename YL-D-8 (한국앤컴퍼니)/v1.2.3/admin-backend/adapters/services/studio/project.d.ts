import Project from "@yennefer/entities/project";
import ProjectSnapshot from "@yennefer/entities/project-snapshot";
import AssetSnapshot from "@yennefer/entities/asset-snapshot";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "@yennefer/core/src/common/base";
import { AxiosBase } from "yennefer-suite-core/dist/server/services/core/base";
import { APIConfig } from "../core/base";
declare class ProjectService extends AxiosBase {
    constructor();
    createProject: (projectSnapshot: ProjectSnapshot & {
        from: string;
        assetSnapshots: Array<AssetSnapshot>;
    }, config: APIConfig) => Promise<Project>;
    activateProjectById: (project: Project, config: APIConfig) => Promise<boolean>;
    deactivateProjectById: (project: Project, config: APIConfig) => Promise<boolean>;
    getProjectById: (project: Project, config: APIConfig) => Promise<Project>;
    getProjectListByProperties: (pagination: Pagination<Project>, config: APIConfig, isActivated?: boolean | undefined) => Promise<ListOutput<Project> | undefined>;
}
declare const _default: ProjectService;
export default _default;
