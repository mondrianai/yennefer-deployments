import AxiosEnvironmentService from "./environment";
import AxiosResourceService from "./resource";
import AxiosProjectService from "./project";
export declare const EnvironmentService: typeof AxiosEnvironmentService;
export declare const ResourceService: typeof AxiosResourceService;
export declare const ProjectService: typeof AxiosProjectService;
