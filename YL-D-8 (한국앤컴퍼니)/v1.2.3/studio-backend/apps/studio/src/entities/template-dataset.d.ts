declare type TemplateDatasetType = {
    templateId?: number;
    datasetId?: number;
};
declare class TemplateDataset {
    templateId?: number;
    datasetId?: number;
    constructor(parameters: TemplateDatasetType);
}
export default TemplateDataset;
