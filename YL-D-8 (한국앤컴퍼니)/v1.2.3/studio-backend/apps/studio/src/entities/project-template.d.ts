import { Tag } from "./tags";
export declare type ProjectTemplateParameterType = {
    id?: number;
    projectId?: number;
    title?: string;
    description?: string;
    environmentId?: number;
    containerImage?: string;
    versionTag?: string;
    ownerId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    tags?: Tag[] | null;
};
export declare class ProjectTemplate {
    id?: number;
    projectId?: number;
    title?: string;
    description?: string;
    environmentId?: number;
    containerImage?: string;
    versionTag?: string;
    ownerId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    tags: Tag[] | null;
    constructor(parameter: ProjectTemplateParameterType);
}
declare type TemplateDatasetRelationshipParams = {
    templateId: number;
    datasetId: number;
};
export declare class TemplateDataset {
    templateId?: number;
    datasetId?: number;
    constructor(params: Partial<TemplateDatasetRelationshipParams>);
}
export {};
