export declare type TemplateDataParameter = {
    id?: number;
    templateId?: number;
    title?: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size?: number | string;
    storePath?: string;
    createdAt?: Date;
    updatedAt?: Date;
    uploader?: number;
    uploadStatus?: string;
    duplicate?: boolean;
};
export declare class TemplateData {
    id?: number;
    templateId?: number;
    title?: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size?: number | string;
    storePath?: string;
    createdAt?: Date;
    updatedAt?: Date;
    uploader?: number;
    uploadStatus?: string;
    duplicate?: boolean;
    constructor(parameter: TemplateDataParameter);
    changeDescription(description: string): void;
}
