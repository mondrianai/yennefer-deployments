import { ProjectTemplate } from "./project-template";
declare type TagParams = {
    id: number;
    name: string;
};
export declare class Tag {
    id?: number;
    name?: string;
    constructor(params: Partial<TagParams>);
}
declare type TemplateTagRelationshipParams = {
    templateId: number;
    tagId: number;
    projectTemplate?: ProjectTemplate;
    tag?: Tag;
};
export declare class TemplateTag {
    templateId?: number;
    tagId?: number;
    readonly tag?: Tag;
    readonly projectTemplate?: ProjectTemplate;
    constructor(params: Partial<TemplateTagRelationshipParams>);
}
export default Tag;
