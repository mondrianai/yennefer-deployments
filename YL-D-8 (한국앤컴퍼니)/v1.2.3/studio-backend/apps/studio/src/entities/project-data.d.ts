export declare type ProjectDataParameterType = {
    id?: number;
    projectId?: number;
    title?: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size?: number | string;
    storePath?: string;
    createdAt?: Date;
    updatedAt?: Date;
    uploader?: number;
    uploadStatus?: string;
    duplicate?: boolean;
};
export declare class ProjectData {
    id?: number;
    projectId?: number;
    title?: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size?: number | string;
    storePath?: string;
    createdAt?: Date;
    updatedAt?: Date;
    uploader?: number;
    uploadStatus?: string;
    duplicate?: boolean;
    constructor(parameter: ProjectDataParameterType);
}
export default ProjectData;
