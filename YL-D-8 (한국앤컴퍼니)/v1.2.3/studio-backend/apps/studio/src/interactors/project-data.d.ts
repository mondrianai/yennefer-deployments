import { FileManagementSerivce } from "@adapters/services/file-management";
import BaseProjectRepository from "./interfaces/base-repositories/project";
import { BaseProjectDataRepository } from "./interfaces/base-repositories/project-data";
export default class ProjectDataInteractor {
    private projectDataRepository;
    private fileManagementService;
    private projectRepository;
    constructor(projectDataRepository: BaseProjectDataRepository, fileManagementService: FileManagementSerivce, projectRepository: BaseProjectRepository);
    getProjectDataListByBasePath(projectId: number, basePath: string): Promise<{
        directories: import("../entities/project-data").ProjectData[];
        files: import("../entities/project-data").ProjectData[];
    }>;
    getProjectDataMetadataById(projectId: number, datasetId: number): Promise<import("../entities/project-data").ProjectData>;
    updateProjectDataMetadata(dataId: number, dto: {
        description: string;
    }): Promise<import("../entities/project-data").ProjectData>;
    deleteProjectDataByPath(projectId: number, path: string, nodeName: string, token: string): Promise<number>;
}
