import CatalogDataset from "@yennefer/entities/catalog-dataset";
import { Pagination } from "@yennefer/entities/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
export default class StudioDatasetInteractor {
    getCatalogDatasetList: (catalogDataset: Pagination<CatalogDataset>, token: string) => Promise<ListOutput<CatalogDataset & {
        resources: Array<CatalogDataset>;
    }>>;
}
