import Project from "@yennefer/entities/project";
import ForkedProject from "@yennefer/entities/forked-project";
import User from "@yennefer/entities/user";
import Environment from "@yennefer/entities/environment";
import PublishedProject from "@yennefer/entities/published-project";
import ProjectSnapshot from "@yennefer/entities/project-snapshot";
import AssetSnapshot from "@yennefer/entities/asset-snapshot";
import CatalogDataset from "@yennefer/entities/catalog-dataset";
import DatasetResource from "@yennefer/entities/dataset-resource";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import ProjectDataset from "@yennefer/entities/project-dataset";
import UserProject from "@yennefer/entities/user-project";
import project from "@adapters/repositories/sequelize/pg/project";
import ProjectRepository from "./interfaces/base-repositories/project";
import { ResourceService } from "./interfaces/services/resource";
import PMSService from "./interfaces/services/pms/pms";
import { ProjectId } from "../core/domains/project";
import { UserId } from "../core/domains/user";
import { ResourceId } from "../core/domains/resource";
import EnvironmentInteractor from "./environment";
export declare type ProjectInteractorType = {
    projectRepository: ProjectRepository;
    resourceService: ResourceService;
    pmsService: PMSService;
    environmentInteractor: EnvironmentInteractor;
};
declare class ProjectInteractor {
    private projectRepository;
    private resourceService;
    private pmsService;
    private environmentInteractor;
    constructor(projectRepository: ProjectRepository, resourceService: ResourceService, pmsService: PMSService, environmentInteractor: EnvironmentInteractor);
    getById: (id: number, nodeName: string) => Promise<Project | null>;
    getProjectAuthToken: (parameters: {
        nodeName: string;
        token: string;
    }) => Promise<string>;
    createProject: (parameters: {
        project: Project;
        user: User;
        assetSnapshots?: Array<AssetSnapshot>;
        forkedProject?: ForkedProject;
        nodeName: string;
        token: string;
    }) => Promise<Project>;
    getProjectByProperty: (parameters: {
        project: Project;
        condition: keyof Project;
        nodeName: string;
        token: string;
    }) => Promise<{
        gpuResource: import("../core/domains/gpu-resource-info").default | null;
        id?: number | undefined;
        name?: string | undefined;
        description?: string | undefined;
        ownerId?: number | undefined;
        environmentId?: number | undefined;
        resourceId?: number | undefined;
        gpuResourceId?: number | undefined;
        nodeName?: string | undefined;
        lastActivatedAt?: Date | undefined;
        createdAt?: Date | undefined;
        updatedAt?: Date | undefined;
        isRunning?: boolean | undefined;
    }>;
    getProjectListByProperty: (parameters: {
        pagination: Pagination<Project>;
        project?: Project;
        condition?: keyof Project | "none";
        nodeName: string;
        token: string;
    }) => Promise<ListOutput<Project>>;
    checkRunning: (parameters: {
        projects: Array<Project>;
        nodeName: string;
        token: string;
    }) => Promise<Array<Project>>;
    setCookieProject: (parameters: {
        projectId: ProjectId;
        ownerId: UserId;
        token: string;
        nodeName: string;
    }) => Promise<Array<string> | null>;
    activateProject: (parameters: {
        projectId: ProjectId;
        ownerId: UserId;
        resourceId?: ResourceId;
        environment: Environment;
        token: string;
        nodeName: string;
    }) => Promise<Array<string> | null>;
    clearDatasets: (parameters: {
        projectId: ProjectId;
    }) => Promise<unknown[]>;
    deactivateProject: (parameters: {
        projectId: ProjectId;
        ownerId: UserId;
        nodeName: string;
        token: string;
    }) => Promise<boolean>;
    deactivateProjectsByOwnerId: (parameters: {
        userId: UserId;
        nodeName?: string;
        token: string;
    }) => Promise<boolean>;
    getEveryProjectCount: (parameters: {
        project: Project;
        token: string;
        nodeName?: string;
    }) => Promise<{
        total: number;
        activated: number;
        shared: number;
        published: number;
    }>;
    getAllActivatedProjectCount: (parameters: {
        nodeName: string;
        token: string;
    }) => Promise<number>;
    getAllProjectCount: ({ nodeName }: {
        nodeName: string;
    }) => Promise<number>;
    saveProject: (parameters: {
        project: Project;
        nodeName: string;
        token: string;
    }) => Promise<Project>;
    deleteProject: (parameters: {
        projectId: ProjectId;
        ownerId: UserId;
        token: string;
        nodeName: string;
    }) => Promise<void>;
    deleteProjectsByOwnerId: (parameters: {
        userId: UserId;
        token: string;
        nodeName?: string;
    }) => Promise<boolean>;
    adjustCollaborator: (parameters: {
        incrementalUsers: Array<User>;
        decrementalUsers: Array<User>;
        project: Project;
    }) => Promise<[number, number]>;
    getPublishedProject: (parameters: {
        project: Project;
        token: string;
    }) => Promise<{
        publishedProject: PublishedProject;
        projectSnapshot: ProjectSnapshot | null;
    } | null>;
    publishProject: (parameters: {
        project: Project;
        token: string;
    }) => Promise<{
        publishedProject: PublishedProject;
        projectSnapshot: ProjectSnapshot;
    }>;
    unpublishProject: (parameters: {
        project: Project;
        token: string;
    }) => Promise<PublishedProject>;
    linkDatasetToProjectFromHostCatalog: (parameters: {
        project: Project;
        catalogDataset: CatalogDataset;
    }) => Promise<boolean>;
    unlinkDatasetToProjectFromHostCatalog: (parameters: {
        project: Project;
        catalogDataset: CatalogDataset;
    }) => Promise<boolean>;
    getCatalogDatasetListByProjectId: (parameters: {
        pagination: Pagination<ProjectDataset>;
        projectDataset: ProjectDataset;
        token: string;
    }) => Promise<{
        count: number;
        data: (CatalogDataset & {
            resources: Array<DatasetResource>;
        })[];
    }>;
    getSharedProjectListByUserId: (parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
        token: string;
    }) => Promise<ListOutput<Project>>;
    getAllProjectListByUserId: (parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
        token: string;
    }) => Promise<ListOutput<Project>>;
    getPublishedProjectListByToken: (parameters: {
        pagination: Pagination<ProjectSnapshot>;
        token: string;
        nodeName: string;
    }) => Promise<ListOutput<Project>>;
    getActivatedProjectListByUserId: (parameters: {
        pagination: Pagination<Project>;
        user: User;
        token: string;
        nodeName: string;
    }) => Promise<ListOutput<Project>>;
    getProjectGPUResource: (project: Project, token: string) => Promise<import("../core/domains/gpu-resource-info").default | null>;
    getProjectsConnectedToDataset: (datasetId: number, token: string) => Promise<{
        count: number;
        rows: {
            owner: {
                id: number | undefined;
                name: string | undefined;
            };
        }[];
    }>;
}
declare const _default: ProjectInteractor;
export default _default;
