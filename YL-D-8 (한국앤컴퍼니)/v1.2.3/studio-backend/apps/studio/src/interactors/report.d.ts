import Report from "@yennefer/entities/report";
import ReportSnapshot from "@yennefer/entities/report-snapshot";
import { Pagination } from "@yennefer/entities/pagination";
import { ReportRepository } from "@adapters/repositories";
import { ReportSnapshotService } from "../adapters/services/sharepoint";
import { NotebookConverter } from "./interfaces/notebook-converter/base";
export declare type ReportInteractorType = {
    notebookConverters: {
        [key: string]: NotebookConverter;
    };
};
declare class ReportInteractor {
    protected reportRepository: typeof ReportRepository;
    protected reportSnapshotService: typeof ReportSnapshotService;
    protected notebookConverters: {
        [key: string]: NotebookConverter;
    };
    constructor({ notebookConverters }: ReportInteractorType);
    createReport: (report: Report) => Promise<Report>;
    getReportByProperty: (report: Report, condition: keyof Report) => Promise<Report | null>;
    getReportListByProperty: (pagination: Pagination<Report>, token: string, report?: Report | undefined, condition?: "createdAt" | "id" | "description" | "projectId" | "publisherId" | "title" | "sourceFile" | "htmlName" | "language" | "updatedAt" | "isUrlVisible" | "none" | undefined) => Promise<{
        count: number;
        reports: Array<Report & {
            isAccessible: boolean;
            snapshotId: number | null;
        }>;
    }>;
    saveReport: (report: Report) => Promise<Report | null>;
    deleteReport: (report: Report, token: string) => Promise<void>;
    createIPythonHTML: (parameters: {
        report: Report;
        machineName: string;
    }) => Promise<string>;
    createOrUpdateReportSnapshot: (reportSnapshot: ReportSnapshot, token: string) => Promise<ReportSnapshot>;
}
declare const _default: ReportInteractor;
export default _default;
