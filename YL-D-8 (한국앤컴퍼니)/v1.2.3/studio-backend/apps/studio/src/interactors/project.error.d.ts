import GPUResourceInfo from "core/domains/gpu-resource-info";
export default class MissingGPUResourceAuthorityError extends Error {
    projectId: number;
    gpuResourceId: number;
    gpuOrder: number;
    constructor(projectId: number, gpuResource: GPUResourceInfo);
}
