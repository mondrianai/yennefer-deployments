import User from "@yennefer/entities/user";
import Project from "@yennefer/entities/project";
import { Pagination } from "@yennefer/entities/pagination";
import { ProjectRepository, UserProjectRepository } from "@adapters/repositories";
import { UserService } from "../adapters/services/iam";
export default class UserInteractor {
    protected userService: typeof UserService;
    protected projectRepository: typeof ProjectRepository;
    protected userProjectRepository: typeof UserProjectRepository;
    constructor();
    getUserById: (user: User, token: string) => Promise<User | null>;
    getUserListByProperty: (pagination: Pagination<User>, token: string) => Promise<{
        count: number;
        users: User[];
    }>;
    getUserListByProjectId: (project: Project, token: string) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    mapUserListToProjectList: (parameters: {
        projects: Array<Project>;
        token: string;
    }) => Promise<{
        collaborators: {
            count: number;
            users: Array<User>;
        };
        id?: number | undefined;
        name?: string | undefined;
        description?: string | undefined;
        ownerId?: number | undefined;
        environmentId?: number | undefined;
        resourceId?: number | undefined;
        gpuResourceId?: number | undefined;
        nodeName?: string | undefined;
        lastActivatedAt?: Date | undefined;
        createdAt?: Date | undefined;
        updatedAt?: Date | undefined;
        isRunning?: boolean | undefined;
    }[]>;
    checkUserAuth: (condition: "User" | "Admin" | "Supervisor" | undefined, user: User, object?: Project | undefined, isOwner?: boolean) => Promise<boolean>;
}
