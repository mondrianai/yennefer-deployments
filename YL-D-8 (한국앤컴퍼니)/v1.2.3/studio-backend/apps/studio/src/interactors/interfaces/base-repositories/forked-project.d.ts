import ForkedProject from "@yennefer/entities/forked-project";
import { Options } from "../../../adapters/repositories";
export default abstract class BaseEnvironmentRepository {
    abstract createForkedProject(forkedProject: ForkedProject, options: Options): Promise<ForkedProject>;
}
