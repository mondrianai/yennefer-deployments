import ProjectDataset from "@yennefer/entities/project-dataset";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { Options } from "../../../adapters/repositories";
export default abstract class BaseStudioDatasetRepository {
    abstract createProjectDataset(projectDataset: ProjectDataset, options: Options): Promise<ProjectDataset>;
    abstract getProjectDatasetByProperty(projectDataset: ProjectDataset, condition: keyof ProjectDataset): Promise<ProjectDataset | null>;
    abstract getProjectDatasetListByProperty(pagination: Pagination<ProjectDataset>, projectDataset?: ProjectDataset, condition?: keyof ProjectDataset): Promise<ListOutput<ProjectDataset>>;
    abstract getProjectDatasetByPk(projectDataset: ProjectDataset): Promise<ProjectDataset | null>;
    abstract deleteStudioDatasetByPk(projectDataset: ProjectDataset): Promise<number>;
}
