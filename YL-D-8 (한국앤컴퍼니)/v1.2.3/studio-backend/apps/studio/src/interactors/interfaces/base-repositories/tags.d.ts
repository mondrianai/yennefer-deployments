import { Tag } from "../../../entities/tags";
import { Options } from "../../../adapters/repositories";
export interface TagsRepository {
    findOrCreate(tag: Tag, options?: Options): Promise<{
        tag: Tag;
        created: boolean;
    }>;
    bulkSelectOrCreate(tags: Tag[], options?: Options): Promise<Tag[]>;
    getTagList(): Promise<{
        count: number;
        rows: Tag[];
    }>;
}
