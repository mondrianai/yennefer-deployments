import Project from "@yennefer/entities/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { Options } from "../../../adapters/repositories";
import { ProjectId } from "../../../core/domains/project";
import { UserId } from "../../../core/domains/user";
export default interface BaseProjectRepository {
    createProject(parameters: {
        project: Project;
        options?: Options;
    }): Promise<Project>;
    getProjectByProperty(parameters: {
        project: Project;
        condition: keyof Project;
        nodeName: string;
    }): Promise<Project | null>;
    getProjectListByProperty(parameters: {
        pagination: Pagination<Project>;
        project?: Project | undefined;
        condition?: keyof Project | "none";
        nodeName: string;
    }): Promise<ListOutput<Project>>;
    getProjectListByProperties(parameters: {
        projects: Array<Project>;
        condition?: keyof Project | "none";
        nodeName: string;
        pagination?: Pagination<Project>;
    }): Promise<ListOutput<Project>>;
    getProjectListByIds(parameters: {
        pagination?: Pagination<Project>;
        nodeName: string;
        ids: Array<number>;
    }): Promise<ListOutput<Project>>;
    getProjectCountByProperties(parameters: {
        projects?: Array<Project>;
        condition?: keyof Project;
        nodeName: string;
    }): Promise<number>;
    getProjectCountByIds(parameters: {
        pagination?: Pagination<Project>;
        nodeName: string;
        ids: Array<number>;
    }): Promise<number>;
    saveProject(parameters: {
        project: Project;
    }): Promise<Project>;
    deleteProject(parameters: {
        projectId: ProjectId;
    }): Promise<void>;
    getProjectCountByOwnerId(parameters: {
        project: Project;
    }): Promise<number>;
    getAllProjectIdsByOwnerId(parameters: {
        userId: UserId;
        nodeName?: string;
    }): Promise<Array<number>>;
}
