import Report from "@yennefer/entities/report";
import { Pagination } from "@yennefer/entities/pagination";
export default abstract class BaseReportRepository {
    abstract createReport(report: Report): Promise<Report>;
    abstract getReportByProperty(report: Report, condition: keyof Report): Promise<Report | null>;
    abstract getReportListByProperty(pagination: Pagination<Report>, environment?: Report, condition?: keyof Report | "none"): Promise<{
        count: number;
        reports: Array<Report>;
    }>;
    abstract saveReport(report: Report): Promise<Report | null>;
    abstract deleteReport(report: Report): Promise<void>;
}
