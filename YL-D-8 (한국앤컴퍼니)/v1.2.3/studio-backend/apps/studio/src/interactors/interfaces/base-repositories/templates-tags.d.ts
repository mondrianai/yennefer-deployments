import { TemplateTag } from "../../../entities/tags";
import { Options } from "../../../adapters/repositories";
import TemplateTagDAO from "../../../adapters/repositories/sequelize/common/dao/template-tags";
export interface TemplatesTagsRepository {
    convertToEntity(dao: TemplateTagDAO): TemplateTag;
    convertToEntities(daoes: TemplateTagDAO[]): TemplateTag[];
    create(datasetTag: TemplateTag, option?: Options): Promise<TemplateTag>;
    findByTemplateId(datasetId: number): Promise<TemplateTag[]>;
    destroyByIds(datasetId: number, tagIds: number[]): Promise<number>;
    bulkCreate(datasetTags: TemplateTag[], option?: Options): Promise<TemplateTag[]>;
}
