import { FindTemplateDataByPathDto } from "../../../dtos/template-data/find-template-data-by-path.dto";
import { TemplateData } from "../../../entities/template-data";
import { Options } from "../../../adapters/repositories";
export interface TemplateDataRepository {
    findByPath(findTemplatesByPathDto: FindTemplateDataByPathDto): Promise<TemplateData[]>;
    findById(templateDataId: number): Promise<TemplateData | null>;
    save(templateData: TemplateData): Promise<TemplateData>;
    delete(templateId: number, fileId: number): Promise<number>;
    bulkCreate(templateData: TemplateData[], option?: Options): Promise<TemplateData[]>;
    findByTemplateId(projectId: number): Promise<TemplateData[]>;
}
