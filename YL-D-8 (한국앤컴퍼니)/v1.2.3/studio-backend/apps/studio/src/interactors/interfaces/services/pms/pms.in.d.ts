import { UserId } from "../../../../core/domains/user";
import { ProjectId } from "../../../../core/domains/project";
export declare type ActivateParams = {
    volumes?: {};
    assignedInfo?: {};
    image: string;
    ownerId: UserId;
    projectId: ProjectId;
    token: string;
    nodeName: string;
};
export declare type GetCookieParams = {
    ownerId: UserId;
    projectId: ProjectId;
    token: string;
    nodeName: string;
};
export declare type DeactivateParams = {
    projectId: ProjectId;
    ownerId: UserId;
    token: string;
    nodeName: string;
};
export declare type DeleteContainerParams = {
    projectId: ProjectId;
    ownerId: UserId;
    token: string;
    nodeName: string;
};
export declare type InitWorkspaceParams = {
    projectId: ProjectId;
    token: string;
    nodeName: string;
};
export declare type MapActivatedProjectIdsParams = {
    projectIds: Array<ProjectId>;
    token: string;
    nodeName: string;
};
export declare type GetActivatedProjectIdsByUserIdsParams = {
    userIds?: Array<UserId>;
    token: string;
    nodeName: string;
};
export declare type GetProjectAuthTokenParams = {
    token: string;
    nodeName: string;
};
