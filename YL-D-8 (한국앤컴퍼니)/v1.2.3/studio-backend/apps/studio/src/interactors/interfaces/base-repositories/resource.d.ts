import Resource from "@yennefer/entities/resource";
import { Pagination } from "@yennefer/entities/pagination";
export default abstract class BaseResourceRepository {
    abstract getResourceByProperty(resource: Resource, condition: keyof Resource): Promise<Resource | null>;
    abstract getResourceListByProperty(pagination: Pagination<Resource>, environment?: Resource, condition?: keyof Resource | "none"): Promise<Array<Resource>>;
}
