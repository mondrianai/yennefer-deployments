import UserProject from "@yennefer/entities/user-project";
import User from "@yennefer/entities/user";
import Project from "@yennefer/entities/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { Options } from "../../../adapters/repositories";
export default interface BaseUserRepository {
    createUserProject(user: User, project: Project, options: Options): Promise<UserProject>;
    getProjectIdsByUserId(user: User, nodeName?: string): Promise<Array<number>>;
    getUserIdByProjectId(project: Project): Promise<Array<number>>;
    relateUserProject(user: User, project: Project): Promise<boolean>;
    getSharedProjectCountByUserId(parameters: {
        user: User;
        nodeName: string;
    }): Promise<number>;
    getSharedProjectListByUserId(parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
    }): Promise<ListOutput<Project>>;
    getAllProjectListByUserId(parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
    }): Promise<ListOutput<Project>>;
    addUserListToProject(users: Array<User>, project: Project): Promise<number>;
    deleteUserListOnProject(users: Array<User>, project: Project): Promise<number>;
}
