import ProjectData from "entities/project-data";
import { Transaction } from "sequelize";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { Options } from "../../../adapters/repositories";
export interface BaseProjectDataRepository {
    create(entity: ProjectData): Promise<ProjectData>;
    findAll(): Promise<ListOutput<ProjectData>>;
    findByPath(projectId: number, basePath: string): Promise<ProjectData[]>;
    findByProjectId(projectId: number): Promise<ProjectData[]>;
    findOneById(id: number): Promise<ProjectData | null>;
    update(id: number, partialEntity: ProjectData, transaction?: Transaction): Promise<ProjectData>;
    delete(projectId: number, path: string, transaction?: Transaction): Promise<number>;
    bulkCreate(templateData: ProjectData[], option?: Options): Promise<ProjectData[]>;
}
