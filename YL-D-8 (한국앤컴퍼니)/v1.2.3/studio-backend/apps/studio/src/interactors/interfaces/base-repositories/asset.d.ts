import Asset from "@yennefer/entities/asset";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { Options } from "../../../adapters/repositories";
export default abstract class BaseAssetRepository {
    abstract createAsset(asset: Asset, options: Options): Promise<Asset>;
    abstract bulkCreateAssets(assets: Array<Asset>, options?: Options): Promise<Array<Asset>>;
    abstract getAssetByProperty(asset: Asset, condition: keyof Asset): Promise<Asset | null>;
    abstract getAssetListByProperty(pagination: Pagination<Asset>, asset?: Asset, condition?: keyof Asset | "none"): Promise<ListOutput<Asset>>;
    abstract deleteAsset(asset: Asset): Promise<void>;
}
