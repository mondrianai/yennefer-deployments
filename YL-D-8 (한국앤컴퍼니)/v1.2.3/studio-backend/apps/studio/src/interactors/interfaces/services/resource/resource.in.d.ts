import { ProjectId } from "../../../../core/domains/project";
import { NodeName } from "../../../../core/domains/node";
import { ResourceId } from "../../../../core/domains/resource";
export declare type GetByIdIn = {
    token: string;
    nodeName: NodeName | string;
    resourceId: ResourceId | number;
};
export declare type GetListIn = {
    token: string;
    nodeName: NodeName | string;
};
export declare type AssignResourceIn = {
    token: string;
    nodeName: NodeName | string;
    projectId: ProjectId | number;
    resourceId?: ResourceId | number;
};
export declare type FreeResourceIn = {
    token: string;
    nodeName: NodeName | string;
    projectId: ProjectId | number;
};
