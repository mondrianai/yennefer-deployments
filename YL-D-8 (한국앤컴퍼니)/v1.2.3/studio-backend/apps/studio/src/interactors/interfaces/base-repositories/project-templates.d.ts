import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { ProjectTemplate, TemplateDataset } from "../../../entities/project-template";
import { FindProjectTemplatesDto } from "../../../dtos/project-templates/find-project-templates.dto";
import PaginationDto from "../../../dtos/common/pagination.dto";
import { Options } from "../../../adapters/repositories";
export interface ProjectTemplatesRepository {
    findAll(findTemplatesDto: FindProjectTemplatesDto, paginationDto: PaginationDto): Promise<ListOutput<ProjectTemplate>>;
    findOne(templateId: number): Promise<ProjectTemplate | null>;
    save(template: ProjectTemplate, options?: Options): Promise<ProjectTemplate>;
    getConnectedDatasetIdsByTemplateId(projectTemplate: ProjectTemplate, paginationDto: PaginationDto): Promise<ListOutput<number>>;
    connectDataset(templateDatasets: TemplateDataset[], option?: Options): Promise<boolean>;
    disconnectDataset(template: ProjectTemplate, datasetIds: Array<number>): Promise<boolean>;
}
