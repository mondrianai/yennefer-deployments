import { TemplateDataRepository } from "./interfaces/base-repositories/template-data";
import { FindTemplateDataByPathDto } from "../dtos/template-data/find-template-data-by-path.dto";
import { UpdateTemplateDataMetadataDto } from "../dtos/template-data/update-template-metadata.dto";
import { DeleteTemplateDataByPathDto } from "../dtos/template-data/delete-template-data-by-path.dto";
import { FindTemplateDataByIdDto } from "../dtos/template-data/find-template-data-by-id.dto";
export declare class TemplateDataInteractor {
    private templateDataRepository;
    constructor(templateDataRepository: TemplateDataRepository);
    findDataByPath(findTemplateDataByPathDto: FindTemplateDataByPathDto): Promise<{
        directories: import("../entities/template-data").TemplateData[];
        files: import("../entities/template-data").TemplateData[];
    }>;
    findById(findTemplateDataByIdDto: FindTemplateDataByIdDto): Promise<import("../entities/template-data").TemplateData>;
    updateMetadata(updateTemplateDataMetadataDto: UpdateTemplateDataMetadataDto): Promise<import("../entities/template-data").TemplateData>;
    deleteByPath(deleteTemplateDataByPathDto: DeleteTemplateDataByPathDto): Promise<void>;
    deleteByTemplateId(templateId: number): Promise<void>;
}
