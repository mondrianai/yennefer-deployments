import { Request, Response } from "express";
import BaseController from "yennefer-suite-core/dist/server/controllers/base";
import { TemplateDataInteractor } from "../../../interactors/template-data";
import { ProjectTemplateInteractor } from "../../../interactors/project-templates";
export declare class ProjectTemplatesController extends BaseController {
    private projectTemplateInteractor;
    private templateDataInteractor;
    constructor(projectTemplateInteractor: ProjectTemplateInteractor, templateDataInteractor: TemplateDataInteractor);
    findAll(req: Request, res: Response): Promise<any>;
    createTemplate(req: Request, res: Response): Promise<any>;
    findOne(req: Request, res: Response): Promise<any>;
    findDataByPath(req: Request, res: Response): Promise<any>;
    findDataMetadataById(req: Request, res: Response): Promise<any>;
    createProjectByTemplate(req: Request, res: Response): Promise<any>;
}
