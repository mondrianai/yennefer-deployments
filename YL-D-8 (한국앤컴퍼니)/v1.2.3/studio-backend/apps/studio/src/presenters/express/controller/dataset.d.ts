import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class StudioDatasetController extends Base {
    private datasetInteractor;
    constructor();
    getCatalogDatasetList: (req: Request, res: Response) => Promise<any>;
}
declare const _default: StudioDatasetController;
export default _default;
