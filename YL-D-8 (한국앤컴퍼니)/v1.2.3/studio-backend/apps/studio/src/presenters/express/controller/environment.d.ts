import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class EnvironmentController extends Base {
    private environmentInteractor;
    constructor();
    getEnvironmentById: (req: Request, res: Response) => Promise<any>;
    getEnvironmentList: (req: Request, res: Response) => Promise<any>;
}
declare const _default: EnvironmentController;
export default _default;
