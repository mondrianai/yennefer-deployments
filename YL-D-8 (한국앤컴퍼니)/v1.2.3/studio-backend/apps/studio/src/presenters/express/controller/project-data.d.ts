import ProjectDataInteractor from "@interactors/project-data";
import { Request, Response } from "express";
import BaseController from "yennefer-suite-core/dist/server/controllers/base";
export default class ProjectDataController extends BaseController {
    private projectDataInteractor;
    constructor(projectDataInteractor: ProjectDataInteractor);
    findProjectData: (req: Request, res: Response) => Promise<any>;
    findOneById: (req: Request, res: Response) => Promise<any>;
    update: (req: Request, res: Response) => Promise<any>;
    delete: (req: Request, res: Response) => Promise<any>;
}
