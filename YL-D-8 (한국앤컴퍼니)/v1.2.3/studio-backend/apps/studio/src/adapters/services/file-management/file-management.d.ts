export interface FileManagementSerivce {
    deleteFileByPath(projectId: number, path: string, options: {
        nodeName: string;
        token: string;
    }): Promise<void>;
}
