import { ResourceService, GetByIdIn, GetListIn, AssignResourceIn, FreeResourceIn, GetByIdOut, GetListOut, AssignResourceOut, FreeResourceOut } from "@interactors/interfaces/services/resource";
import { AxiosBase } from "yennefer-suite-core/dist/server/services/core/base";
import GPUResourceInfo from "core/domains/gpu-resource-info";
export declare class AxiosResourceService extends AxiosBase implements ResourceService {
    getById: (parameters: GetByIdIn) => Promise<GetByIdOut>;
    getList: (parameters: GetListIn) => Promise<GetListOut>;
    assignResource: (parameters: AssignResourceIn) => Promise<AssignResourceOut>;
    freeResource: (parameters: FreeResourceIn) => Promise<FreeResourceOut>;
    getGPUResourceById(gpuResourceId: number, { token }: {
        token: string;
    }): Promise<GPUResourceInfo | null>;
    getGPUResourcesByIds(gpuResourceIds: number[], { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
    getGPUResourcesByUserId(userId: number, { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
}
export default AxiosResourceService;
