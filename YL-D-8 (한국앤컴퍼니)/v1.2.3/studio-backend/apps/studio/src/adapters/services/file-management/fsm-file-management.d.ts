import { FileManagementSerivce } from "./file-management";
export default class FSMFileManagementService implements FileManagementSerivce {
    private axiosInstances;
    constructor();
    deleteFileByPath(projectId: number, path: string, { nodeName, token }: {
        nodeName: string;
        token: string;
    }): Promise<void>;
}
