import AxiosProjectService from "./project";
import AxiosReportService from "./report";
export declare const PublishedProjectService: typeof AxiosProjectService;
export declare const ReportSnapshotService: typeof AxiosReportService;
