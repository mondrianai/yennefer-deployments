import PublishedProject from "@yennefer/entities/published-project";
import ProjectSnapshot from "@yennefer/entities/project-snapshot";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import Project from "@yennefer/entities/project";
import User from "@yennefer/entities/user";
import Environment from "@yennefer/entities/environment";
import Resource from "@yennefer/entities/resource";
import Asset from "@yennefer/entities/asset";
import { AxiosBase, APIConfig } from "yennefer-suite-core/dist/server/services/core/base";
declare class ProjectService extends AxiosBase {
    constructor();
    getPublishedProjectListWithSnapshot: (pagination: Pagination<ProjectSnapshot>, config: APIConfig, options?: {
        snapshot: ProjectSnapshot;
        condition: keyof ProjectSnapshot | "none";
    } | undefined) => Promise<{
        count: number;
        projects: PublishedProject[];
    } | null>;
    getPublishedProjectListByPublisherId: (pagination: Pagination<ProjectSnapshot>, config: APIConfig, options?: {
        snapshot: ProjectSnapshot;
        condition: keyof ProjectSnapshot | "none";
    } | undefined) => Promise<{
        count: number;
        data: Array<{
            publishedProject: PublishedProject;
        }>;
    } | null>;
    getPublishedProjectCountByPublisherId: (config: APIConfig) => Promise<number>;
    getProjectSnapshotById: (id: number, config: APIConfig) => Promise<{
        snapshot: ProjectSnapshot;
        user: User | null;
        environment: Environment | null;
        resource: Resource | null;
    } | null>;
    getPublishedProjectById: (project: Project, config: APIConfig) => Promise<{
        publishedProject: PublishedProject;
        projectSnapshot: ProjectSnapshot | null;
    } | null>;
    createProjectSnapshot: (project: Project & {
        assets: Array<Asset>;
    }, config: APIConfig) => Promise<ProjectSnapshot>;
    publishProjectToSharePoint: (projectSnapshot: ProjectSnapshot, config: APIConfig) => Promise<{
        publishedProject: PublishedProject;
        projectSnapshot: ProjectSnapshot;
    } | null>;
    unpublishProjectFromSharePoint: (projectId: number, config: APIConfig) => Promise<PublishedProject | null>;
}
declare const _default: ProjectService;
export default _default;
