import paginationDto from "dtos/common/pagination.dto";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { ProjectTemplate, TemplateDataset } from "../../../../entities/project-template";
import { ProjectTemplatesRepository } from "../../../../interactors/interfaces/base-repositories/project-templates";
import { Options } from "../..";
import { FindProjectTemplatesDto } from "../../../../dtos/project-templates/find-project-templates.dto";
export default class SequelizeProjectTemplatesRepository implements ProjectTemplatesRepository {
    findAll(findTemplatesDto: FindProjectTemplatesDto, paginationDto: paginationDto): Promise<ListOutput<ProjectTemplate>>;
    findOne(templateId: number): Promise<ProjectTemplate | null>;
    save(template: ProjectTemplate, options?: Options): Promise<ProjectTemplate>;
    getConnectedDatasetIdsByTemplateId(projectTemplate: ProjectTemplate, paginationDto: paginationDto): Promise<ListOutput<number>>;
    connectDataset(templateDatasets: TemplateDataset[], option?: Options): Promise<boolean>;
    disconnectDataset(template: ProjectTemplate, datasetIds: Array<number>): Promise<boolean>;
}
