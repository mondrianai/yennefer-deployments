import { Model, Association } from "sequelize";
import { TemplateDataset } from "entities/project-template";
import ProjectTemplateDAO from "./project-template";
declare class TemplateDatasetDAO extends Model<TemplateDataset, TemplateDataset> {
    readonly templateId: number;
    readonly datasetId: number;
    template?: ProjectTemplateDAO;
    static associations: {
        template: Association<TemplateDatasetDAO, ProjectTemplateDAO>;
    };
    static associate(models: any): void;
}
export default TemplateDatasetDAO;
