import { Model, Optional } from "sequelize";
import Resource from "@yennefer/entities/resource";
interface ResourceCreation extends Optional<Resource, "id"> {
}
declare class ResourceDAO extends Model<Resource, ResourceCreation> {
    readonly id: number;
    code: string;
    name: string;
    price: number;
    gpu: number;
    cpu: number;
    ram: string;
    static associate(): void;
}
export default ResourceDAO;
