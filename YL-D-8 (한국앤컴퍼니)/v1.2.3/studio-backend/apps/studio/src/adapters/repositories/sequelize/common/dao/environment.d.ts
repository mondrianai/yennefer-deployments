/// <reference types="node" />
import { Model, Optional } from "sequelize";
import Environment from "@yennefer/entities/environment";
interface EnvironmentCreation extends Optional<Environment, "id"> {
}
declare class EnvironmentDAO extends Model<Environment, EnvironmentCreation> {
    readonly id: number;
    name: string;
    processor?: string;
    description?: string;
    reference?: string;
    containerImage: string;
    backgroundImage: Buffer;
    author: string;
    static associate(): void;
}
export default EnvironmentDAO;
