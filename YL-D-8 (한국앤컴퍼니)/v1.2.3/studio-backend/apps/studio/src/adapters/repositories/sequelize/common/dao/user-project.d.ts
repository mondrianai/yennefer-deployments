import { Model, Optional, Association } from "sequelize";
import UserProject from "@yennefer/entities/user-project";
import Project from "@yennefer/entities/project";
import ProjectDAO from "./project";
interface UserProjectCreation extends Optional<UserProject, "userId" | "projectId"> {
}
declare class UserProjectDAO extends Model<UserProject, UserProjectCreation> {
    readonly projectId: number;
    readonly userId: number;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    project?: Project;
    static associations: {
        projects: Association<UserProjectDAO, ProjectDAO>;
    };
    static associate(models: any): void;
}
export default UserProjectDAO;
