import ForkedProject from "@yennefer/entities/forked-project";
import { ForkedProjectDAO } from "../common/dao";
import Base from "../../../../interactors/interfaces/base-repositories/forked-project";
import { Options } from "../..";
declare class ForkedProjectRepository extends Base {
    createForkedProject: (forkedProject: ForkedProject, options?: Options | undefined) => Promise<ForkedProject>;
    convertToEntity: (dao: ForkedProjectDAO) => ForkedProject;
    convertToEntities: (daoes: Array<ForkedProjectDAO>) => Array<ForkedProject>;
}
declare const _default: ForkedProjectRepository;
export default _default;
