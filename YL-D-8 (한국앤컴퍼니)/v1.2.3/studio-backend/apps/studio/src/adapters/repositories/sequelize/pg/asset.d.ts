import Asset from "@yennefer/entities/asset";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import AssetDAO from "../common/dao/asset";
import Base from "../../../../interactors/interfaces/base-repositories/asset";
import { Options } from "../..";
declare class AssetRepository extends Base {
    createAsset: (asset: Asset, options?: Options | undefined) => Promise<Asset>;
    bulkCreateAssets: (assets: Array<Asset>, options?: Options | undefined) => Promise<Asset[]>;
    getAssetByProperty: (asset: Asset, condition: keyof Asset) => Promise<Asset | null>;
    getAssetListByProperty: (pagination: Pagination<Asset>, asset?: Asset | undefined, condition?: "createdAt" | "id" | "projectId" | "updatedAt" | "none" | "mimetype" | "size" | "fileName" | undefined) => Promise<ListOutput<Asset>>;
    deleteAsset: (asset: Asset) => Promise<void>;
    convertToEntity: (dao: AssetDAO) => Asset;
    convertToEntities: (daoes: Array<AssetDAO>) => Array<Asset>;
}
declare const _default: AssetRepository;
export default _default;
