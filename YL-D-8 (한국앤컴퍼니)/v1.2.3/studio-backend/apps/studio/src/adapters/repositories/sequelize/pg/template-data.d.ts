import { TemplateDataRepository } from "@interactors/interfaces/base-repositories/template-data";
import { FindTemplateDataByPathDto } from "../../../../dtos/template-data/find-template-data-by-path.dto";
import { TemplateData } from "../../../../entities/template-data";
import TemplateDataDAO from "../common/dao/template-data";
import { Options } from "../../index";
export declare class SequelizeTemplateDataRepository implements TemplateDataRepository {
    convertToEntity(dao: TemplateDataDAO): TemplateData;
    convertToEntities(daoes: TemplateDataDAO[]): TemplateData[];
    findByPath(findTemplateDataByPathDto: FindTemplateDataByPathDto): Promise<TemplateData[]>;
    findById(fileId: number): Promise<TemplateData | null>;
    delete(templateId: number, fileId: number): Promise<number>;
    save(templateData: TemplateData): Promise<TemplateData>;
    bulkCreate(templateData: TemplateData[], option?: Options): Promise<TemplateData[]>;
    findByTemplateId(templateId: number): Promise<TemplateData[]>;
}
