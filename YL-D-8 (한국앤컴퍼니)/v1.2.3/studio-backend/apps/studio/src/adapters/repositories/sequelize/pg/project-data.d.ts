import { Transaction } from "sequelize";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import ProjectData from "../../../../entities/project-data";
import { BaseProjectDataRepository } from "../../../../interactors/interfaces/base-repositories/project-data";
import { ProjectDataDAO } from "../common/dao";
import { Options } from "../../index";
export default class ProjectDataRepository implements BaseProjectDataRepository {
    convertToEntity(dao: ProjectDataDAO): ProjectData;
    convertToEntities(daoes: ProjectDataDAO[]): ProjectData[];
    findOneById(id: number): Promise<ProjectData | null>;
    findAll(): Promise<ListOutput<ProjectData>>;
    findByPath(projectId: number, basePath: string): Promise<ProjectData[]>;
    findByProjectId(projectId: number): Promise<ProjectData[]>;
    create(entity: ProjectData, transaction?: Transaction): Promise<ProjectData>;
    update(id: number, partialEntity: ProjectData, transaction?: Transaction): Promise<ProjectData>;
    delete(projectId: number, path: string, transaction?: Transaction): Promise<number>;
    bulkCreate(projectData: ProjectData[], option?: Options): Promise<ProjectData[]>;
}
