import Resource from "@yennefer/entities/resource";
import { Pagination } from "@yennefer/entities/pagination";
import ResourceDAO from "../common/dao/resource";
import Base from "../../../../interactors/interfaces/base-repositories/resource";
declare class ResourceRepository extends Base {
    getResourceByProperty: (resource: Resource, condition: keyof Resource) => Promise<Resource | null>;
    getResourceListByProperty: (pagination: Pagination<Resource>, Resource?: Resource | undefined, condition?: "id" | "name" | "code" | "price" | "gpu" | "cpu" | "ram" | "none" | undefined) => Promise<Resource[]>;
    convertToEntity: (dao: ResourceDAO) => Resource;
    convertToEntities: (daoes: Array<ResourceDAO>) => Array<Resource>;
}
declare const _default: ResourceRepository;
export default _default;
