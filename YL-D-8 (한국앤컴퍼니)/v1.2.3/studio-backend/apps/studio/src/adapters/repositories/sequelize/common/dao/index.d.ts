import ForkedProjectDAOTemp from "./forked-project";
import ProjectDAOTemp from "./project";
import UserProjectDAOTemp from "./user-project";
import ReportDAOTemp from "./report";
import EnvironmentDAOTemp from "./environment";
import ResourceDAOTemp from "./resource";
import AssetDAOTemp from "./asset";
import ProjectDatasetDAOTemp from "./project-dataset";
import ProjectDataDAOTemp from "./project-data";
import ProjectTemplateDAOTemp from "./project-template";
import TemplateTagDAOTemp from "./template-tags";
import TagDAOTemp from "./tag";
import TemplateDataDAOTemp from "./template-data";
import TemplateDatasetDAOTemp from "./template-dataset";
declare const models: {
    project: typeof ProjectDAOTemp;
    forkedProject: typeof ForkedProjectDAOTemp;
    userProject: typeof UserProjectDAOTemp;
    environment: typeof EnvironmentDAOTemp;
    resource: typeof ResourceDAOTemp;
    report: typeof ReportDAOTemp;
    asset: typeof AssetDAOTemp;
    projectDataset: typeof ProjectDatasetDAOTemp;
    projectData: typeof ProjectDataDAOTemp;
    projectTemplate: typeof ProjectTemplateDAOTemp;
    templateTag: typeof TemplateTagDAOTemp;
    tag: typeof TagDAOTemp;
    templateData: typeof TemplateDataDAOTemp;
    templateDataset: typeof TemplateDatasetDAOTemp;
};
export declare class ProjectDAO extends ProjectDAOTemp {
}
export declare class ForkedProjectDAO extends ForkedProjectDAOTemp {
}
export declare class UserProjectDAO extends UserProjectDAOTemp {
}
export declare class ReportDAO extends ReportDAOTemp {
}
export declare class EnvironmentDAO extends EnvironmentDAOTemp {
}
export declare class ResourceDAO extends ResourceDAOTemp {
}
export declare class AssetDAO extends AssetDAOTemp {
}
export declare class ProjectDatasetDAO extends ProjectDatasetDAOTemp {
}
export declare class ProjectDataDAO extends ProjectDataDAOTemp {
}
export declare class ProjectTemplateDAO extends ProjectTemplateDAOTemp {
}
export declare class TemplateTagDAO extends TemplateTagDAOTemp {
}
export declare class TagDAO extends TagDAOTemp {
}
export declare class TemplateDataDAO extends TemplateDataDAOTemp {
}
export declare class TemplateDatasetDAO extends TemplateDatasetDAOTemp {
}
export default models;
