import { Model, Optional } from "sequelize";
import { Tag } from "../../../../../entities/tags";
interface TagCreation extends Optional<Tag, "id"> {
}
declare class TagDAO extends Model<Tag, TagCreation> {
    readonly id: number;
    name: string;
    static associate(models: any): void;
}
export default TagDAO;
