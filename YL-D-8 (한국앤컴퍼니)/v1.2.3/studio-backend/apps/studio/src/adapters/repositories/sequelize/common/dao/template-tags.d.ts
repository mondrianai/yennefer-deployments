import { Association, Model } from "sequelize";
import { TemplateTag } from "../../../../../entities/tags";
import TagDAO from "./tag";
declare class TemplateTagDAO extends Model<TemplateTag, TemplateTag> {
    templateId: number;
    tagId: number;
    static associations: {
        tags: Association<TemplateTagDAO, TagDAO>;
    };
    static associate(models: any): void;
}
export default TemplateTagDAO;
