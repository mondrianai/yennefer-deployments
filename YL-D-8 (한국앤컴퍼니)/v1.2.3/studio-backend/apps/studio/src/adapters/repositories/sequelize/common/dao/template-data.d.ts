import { Model, Optional } from "sequelize";
import { TemplateData } from "../../../../../entities/template-data";
declare class TemplateDataDAO extends Model<Omit<TemplateData, "changeDescription">, Optional<TemplateData, "id">> {
    readonly id: number;
    templateId: number;
    title: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size: string;
    storePath: string;
    createdAt: Date;
    updatedAt: Date;
    uploader?: number;
    static associate(models: any): void;
}
export default TemplateDataDAO;
