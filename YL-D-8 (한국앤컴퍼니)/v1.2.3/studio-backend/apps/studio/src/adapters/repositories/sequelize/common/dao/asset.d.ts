import { Model } from "sequelize";
import Asset from "@yennefer/entities/asset";
interface AssetCreation extends Asset {
}
declare class AssetDAO extends Model<Asset, AssetCreation> {
    readonly id: number;
    projectId: number;
    mimetype: string;
    size: number;
    fileName: string;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    static associate(): void;
}
export default AssetDAO;
