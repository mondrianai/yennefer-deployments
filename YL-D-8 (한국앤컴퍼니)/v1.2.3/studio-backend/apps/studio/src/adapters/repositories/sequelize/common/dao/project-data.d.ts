import ProjectData from "entities/project-data";
import { Model, Optional } from "sequelize";
declare class ProjectDataDAO extends Model<ProjectData, Optional<ProjectData, "id">> {
    readonly id: number;
    projectId: number;
    title: string;
    description?: string;
    accessUrl?: string;
    format?: string;
    size?: number;
    isDirectory?: boolean;
    storePath?: string;
    createdAt: Date;
    updatedAt: Date;
    uploadStatus?: string;
    uploader?: number;
    static associate(models: any): void;
}
export default ProjectDataDAO;
