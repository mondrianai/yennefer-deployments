import { Model, Optional } from "sequelize";
import Project from "@yennefer/entities/project";
import ResourceDAO from "./resource";
import EnvironmentDAO from "./environment";
export interface ProjectCreation extends Optional<Project, "id"> {
}
declare class ProjectDAO extends Model<Project, ProjectCreation> {
    readonly id: number;
    name: string;
    description?: string;
    ownerId: number;
    resourceId: number;
    environmentId: number;
    nodeName: string;
    lastActivatedAt: Date;
    isRunning: boolean;
    gpuResourceId?: number;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    resource?: ResourceDAO;
    environment?: EnvironmentDAO;
    static associate(models: any): void;
}
export default ProjectDAO;
