import { Tag } from "../../../../entities/tags";
import { Options } from "../../index";
import { TagsRepository } from "../../../../interactors/interfaces/base-repositories/tags";
export default class SequelizeTagRepository implements TagsRepository {
    private convertToEntity;
    private convertToEntities;
    findOrCreate(tag: Tag, options?: Options): Promise<{
        tag: Tag;
        created: boolean;
    }>;
    getTagList(): Promise<{
        count: number;
        rows: Tag[];
    }>;
    bulkSelectOrCreate(tags: Tag[], options?: Options): Promise<Tag[]>;
}
