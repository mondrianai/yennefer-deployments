import { ProjectTemplate } from "entities/project-template";
import { Association, Model, Optional } from "sequelize";
import { TagDAO } from ".";
declare class ProjectTemplateDAO extends Model<Omit<ProjectTemplate, "tags">, Optional<ProjectTemplate, "id">> {
    readonly id: number;
    projectId: number;
    title: string;
    description?: string;
    environmentId: number;
    containerImage: string;
    versionTag: string;
    ownerId: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date;
    tags?: TagDAO[];
    static associations: {
        tags: Association<ProjectTemplateDAO, TagDAO>;
    };
    static associate(models: any): void;
}
export default ProjectTemplateDAO;
