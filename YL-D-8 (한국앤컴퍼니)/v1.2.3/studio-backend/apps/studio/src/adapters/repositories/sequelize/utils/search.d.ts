declare const createSearchOption: <T>(entity: T | T[], condition: keyof T | (keyof T)[]) => {
    [c: string]: T[keyof T][];
} | {
    [c: string]: T[keyof T];
};
export default createSearchOption;
