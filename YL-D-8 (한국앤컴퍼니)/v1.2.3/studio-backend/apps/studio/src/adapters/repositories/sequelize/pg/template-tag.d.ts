import { TemplateTag } from "../../../../entities/tags";
import { Options } from "../../index";
import { TemplatesTagsRepository } from "../../../../interactors/interfaces/base-repositories/templates-tags";
import TemplateTagDAO from "../common/dao/template-tags";
export default class TemplateTagRepository implements TemplatesTagsRepository {
    convertToEntity(dao: TemplateTagDAO): TemplateTag;
    convertToEntities(daoes: TemplateTagDAO[]): TemplateTag[];
    create(templateTag: TemplateTag, option?: Options): Promise<TemplateTag>;
    findByTemplateId(templateId: number): Promise<TemplateTag[]>;
    destroyByIds(templateId: number, tagIds: number[]): Promise<number>;
    bulkCreate(templateTags: TemplateTag[], option?: Options): Promise<TemplateTag[]>;
}
