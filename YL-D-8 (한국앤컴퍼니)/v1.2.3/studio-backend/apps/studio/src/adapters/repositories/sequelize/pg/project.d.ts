import Project from "@yennefer/entities/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import Base from "../../../../interactors/interfaces/base-repositories/project";
import { ProjectDAO } from "../common/dao";
import { Options } from "../..";
import { ProjectId } from "../../../../core/domains/project";
import { UserId } from "../../../../core/domains/user";
declare class ProjectRepository implements Base {
    createProject: (parameters: {
        project: Project;
        options?: Options;
    }) => Promise<Project>;
    getProjectByProperty: (parameters: {
        project: Project;
        condition: keyof Project;
        nodeName: string;
    }) => Promise<Project | null>;
    getProjectListByProperty: (parameters: {
        pagination: Pagination<Project>;
        project?: Project | undefined;
        condition?: keyof Project | "none";
        nodeName: string;
    }) => Promise<ListOutput<Project>>;
    getProjectListByProperties: (parameters: {
        projects: Array<Project>;
        condition: keyof Project;
        nodeName: string;
        pagination?: Pagination<Project>;
    }) => Promise<ListOutput<Project>>;
    getProjectCountByProperties: (parameters: {
        projects?: Array<Project>;
        condition: keyof Project;
        nodeName: string;
    }) => Promise<number>;
    saveProject: (parameters: {
        project: Project;
    }) => Promise<Project>;
    deleteProject: (parameters: {
        projectId: ProjectId;
    }) => Promise<void>;
    getProjectCountByOwnerId: (parameters: {
        project: Project;
    }) => Promise<number>;
    getProjectListByIds: (parameters: {
        nodeName: string;
        ids: Array<number>;
        pagination?: Pagination<Project>;
    }) => Promise<{
        count: number;
        data: Project[];
    }>;
    getAllProjectIdsByOwnerId: (parameters: {
        userId: UserId;
        nodeName?: string;
    }) => Promise<number[]>;
    getProjectCountByIds: (parameters: {
        nodeName: string;
        ids: Array<number>;
        pagination?: Pagination<Project>;
    }) => Promise<number>;
    convertToEntity: (dao: ProjectDAO) => Project;
    convertToEntities: (daoes: Array<ProjectDAO>) => Array<Project>;
}
declare const _default: ProjectRepository;
export default _default;
