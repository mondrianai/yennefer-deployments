import Report from "@yennefer/entities/report";
import { Pagination } from "@yennefer/entities/pagination";
import { ReportDAO } from "../common/dao";
import Base from "../../../../interactors/interfaces/base-repositories/report";
declare class ReportRepository extends Base {
    createReport: (report: Report) => Promise<Report>;
    getReportByProperty: (report: Report, condition: keyof Report) => Promise<Report | null>;
    getReportListByProperty: (pagination: Pagination<Report>, report?: Report | undefined, condition?: "createdAt" | "id" | "description" | "projectId" | "publisherId" | "title" | "sourceFile" | "htmlName" | "language" | "updatedAt" | "isUrlVisible" | "none" | undefined) => Promise<{
        count: number;
        reports: Array<Report>;
    }>;
    saveReport: (report: Report) => Promise<Report | null>;
    deleteReport: (report: Report) => Promise<void>;
    convertToEntity: (dao: ReportDAO) => Report;
    convertToEntities: (daoes: Array<ReportDAO>) => Array<Report>;
}
declare const _default: ReportRepository;
export default _default;
