export declare class NodeName {
    readonly value: string;
    constructor(parameters: string);
    static validate(object: any): boolean;
}
export default NodeName;
