export declare class ProjectId {
    readonly value: number;
    constructor(value: number);
    static validate(object: any): boolean;
}
export default ProjectId;
