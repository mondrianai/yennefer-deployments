export declare class FindTemplateDataByIdDto {
    templateId: number;
    templateDataId: number;
    constructor(templateId: number, templateDataId: number);
}
