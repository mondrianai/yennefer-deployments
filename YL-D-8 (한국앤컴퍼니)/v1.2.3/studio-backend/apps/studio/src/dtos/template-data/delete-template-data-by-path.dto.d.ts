export declare class DeleteTemplateDataByPathDto {
    templateId: number;
    path: string;
    constructor(templateId: number, path: string);
}
