export declare class UpdateTemplateDataMetadataDto {
    templateId: number;
    dataId: number;
    description: string;
    constructor(templateId: number, dataId: number, description: string);
}
