export declare class FindTemplateDataByPathDto {
    templateId: number;
    path: string;
    constructor(templateId: number, path: string);
}
