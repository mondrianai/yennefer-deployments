export declare class ServiceAuthenticationCredentialsDto {
    token: string;
    nodeName?: string;
    constructor(token: string, nodeName?: string);
}
