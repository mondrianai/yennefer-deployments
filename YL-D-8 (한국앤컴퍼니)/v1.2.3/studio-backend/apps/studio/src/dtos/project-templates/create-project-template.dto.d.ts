export declare class CreateProjectTemplateDto {
    projectId: number;
    title: string;
    description: string;
    tags: string[];
    constructor(projectId: number, title: string, description: string, tags: string[]);
}
