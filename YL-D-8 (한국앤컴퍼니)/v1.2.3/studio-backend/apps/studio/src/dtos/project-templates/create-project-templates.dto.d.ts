export declare class CreateProjectTemplatesDTO {
    title: string;
    description: string;
    projectId: number;
    ownerId: number;
    tags: string[];
    constructor(title: string, description: string, projectId: number, ownerId: number, tags?: string[]);
}
