export default class HTTPError extends Error {
    code: number;
    constructor(code: number, ...props: Array<any>);
}
