declare type ProjectDatasetType = {
    projectId?: number;
    datasetId?: number;
};
declare class ProjectDataset {
    projectId?: number;
    datasetId?: number;
    constructor(parameters: ProjectDatasetType);
}
export default ProjectDataset;
