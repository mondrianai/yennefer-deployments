declare type ForkedProjectType = {
    projectId: number;
    from: string;
    publisherId: number;
    snapshotId: number;
};
declare class ForkedProject {
    projectId?: number;
    from?: string;
    publisherId?: number;
    snapshotId?: number;
    constructor(parameters: Partial<ForkedProjectType>);
}
export default ForkedProject;
