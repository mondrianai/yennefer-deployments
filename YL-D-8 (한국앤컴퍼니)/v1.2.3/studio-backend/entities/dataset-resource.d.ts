export declare type ResourceParameterType = {
    id?: number;
    datasetId?: number;
    title?: string;
    description?: string;
    downloadUrl?: string;
    accessUrl?: string;
    fileName: string;
    format?: string;
    size?: number;
    createdAt?: Date;
    updatedAt?: Date;
};
export declare class Resource {
    id?: number;
    datasetId?: number;
    title?: string;
    description?: string;
    downloadUrl?: string;
    accessUrl?: string;
    fileName?: string;
    format?: string;
    size?: number;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(parameter: ResourceParameterType);
}
export default Resource;
