interface ReportType {
    id: number;
    projectId: number;
    publisherId: number;
    title: string;
    description: string;
    isUrlVisible: boolean;
    sourceFile: string;
    htmlName: string;
    language: string;
    createdAt: Date;
    updatedAt: Date;
}
export default class Report {
    id?: number;
    projectId?: number;
    publisherId?: number;
    title?: string;
    description?: string;
    isUrlVisible?: boolean;
    sourceFile?: string;
    htmlName?: string;
    language?: string;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(params: Partial<ReportType>);
}
export {};
