declare type ProjectType = {
    id?: number;
    name?: string;
    description?: string;
    ownerId?: number;
    environmentId?: number;
    resourceId?: number;
    nodeName?: string;
    gpuResourceId?: number;
    lastActivatedAt?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    isRunning?: boolean;
};
declare class Project {
    id?: number;
    name?: string;
    description?: string;
    ownerId?: number;
    environmentId?: number;
    resourceId?: number;
    gpuResourceId?: number;
    nodeName?: string;
    lastActivatedAt?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    isRunning?: boolean | undefined;
    constructor(parameters: ProjectType);
    static isProject: (value: any) => value is Project;
}
export default Project;
