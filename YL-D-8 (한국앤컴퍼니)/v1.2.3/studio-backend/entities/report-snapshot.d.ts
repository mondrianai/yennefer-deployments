interface ReportSnapshotType {
    id: number;
    reportId: number;
    projectId: number;
    publisherId: number;
    title: string;
    description: string;
    isAccessible: boolean;
    sourceFile: string;
    htmlName: string;
    language: string;
    createdAt: Date;
    updatedAt: Date;
}
export default class ReportSnapshot {
    id?: number;
    reportId?: number;
    projectId?: number;
    publisherId?: number;
    title?: string;
    description?: string;
    isAccessible?: boolean;
    sourceFile?: string;
    htmlName?: string;
    language?: string;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(params: Partial<ReportSnapshotType>);
}
export {};
