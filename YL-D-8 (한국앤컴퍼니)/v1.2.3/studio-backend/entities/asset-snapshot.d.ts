declare type AssetSnapshotType = {
    id?: number;
    snapshotId?: number;
    mimetype?: string;
    size?: number;
    fileName?: string;
    createdAt?: Date;
    updatedAt?: Date;
};
declare class AssetSnapshot {
    id?: number;
    snapshotId?: number;
    mimetype?: string;
    size?: number;
    fileName?: string;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(parameters: AssetSnapshotType);
}
export default AssetSnapshot;
