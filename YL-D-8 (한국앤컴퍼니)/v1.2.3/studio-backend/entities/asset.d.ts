declare type AssetType = {
    id?: number;
    projectId?: number;
    mimetype?: string;
    size?: number;
    fileName?: string;
    createdAt?: Date;
    updatedAt?: Date;
};
declare class Asset {
    id?: number;
    projectId?: number;
    mimetype?: string;
    size?: number;
    fileName?: string;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(parameters: AssetType);
}
export default Asset;
