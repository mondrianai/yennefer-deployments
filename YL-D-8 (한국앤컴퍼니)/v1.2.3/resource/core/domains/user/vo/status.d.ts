export declare const USER_STATUS_LIST: string[];
export declare type UserStatusType = "AUTH_EMAIL" | "REQ_ALLOW" | "NORMAL" | "DISABLED" | "REQ_DELETED" | "DELETED";
export declare class UserStatus {
    readonly value: UserStatusType;
    constructor(status: UserStatusType | string);
    static validate(value: unknown): value is UserStatusType;
}
export default UserStatus;
