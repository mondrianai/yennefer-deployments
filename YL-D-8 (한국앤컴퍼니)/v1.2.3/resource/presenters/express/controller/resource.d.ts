import { Request, Response } from "express";
import BaseController from "yennefer-suite-core/dist/server/controllers/base";
declare class ResourceController extends BaseController {
    private resourceInteractor;
    constructor();
    assignResource: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    freeResource: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getResourceById: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getResourceList: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getAssignInfo: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
}
declare const _default: ResourceController;
export default _default;
