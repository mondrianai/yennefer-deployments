import { User, UserId } from "../core/domains/user";
import UserService from "./core/services/user";
export declare type UserInteractorType = {
    userService: UserService;
};
export declare class UserInteractor {
    userService: UserService;
    constructor({ userService }: UserInteractorType);
    getById: ({ userId, token }: {
        userId: UserId;
        token: string;
    }) => Promise<import("./core/services/user/user.out").UserGetByIdOut>;
    checkAuth: (condition: "User" | "Admin" | "Supervisor" | undefined, user: User) => Promise<boolean>;
}
declare const _default: UserInteractor;
export default _default;
