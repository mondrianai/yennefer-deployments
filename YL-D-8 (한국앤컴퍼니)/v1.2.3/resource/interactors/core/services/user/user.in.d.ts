import { UserId } from "yennefer-suite-core/dist/domains/iam/user/vo";
export declare type UserGetByIdIn = {
    userId: UserId;
    token: string;
};
declare const _default: {};
export default _default;
