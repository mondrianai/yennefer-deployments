import { ProjectId } from "yennefer-suite-core/dist/domains/studio/project";
export declare class ResourceOverflowError extends Error {
    private count;
    private type;
    constructor(count: number, type: string, ...params: Array<any>);
}
export interface ResourceManager {
    assignGPU(count: number, projectId: ProjectId | number): Array<number>;
    assignCPU(count: number, projectId: ProjectId | number): Array<number>;
    assignRAM(size: number | string, projectId: ProjectId | number): number;
    freeCPU(projectId: ProjectId | number): null | number;
    freeGPU(projectId: ProjectId | number): null | number;
    freeRAM(projectId: ProjectId | number): null | number;
    freeAll(projectId: ProjectId | number): void;
    readonly availableCpuCount: number;
    readonly availableGpuCount: number;
    readonly availableRamSize: number;
    readonly assignedInfo: {
        cpu: Array<[number, number | null]>;
        gpu: Array<[number, number | null]>;
        ram: Array<[number, number | null]>;
    };
}
export declare type ResourceManagers = {
    [key: string]: ResourceManager;
};
export default ResourceManager;
