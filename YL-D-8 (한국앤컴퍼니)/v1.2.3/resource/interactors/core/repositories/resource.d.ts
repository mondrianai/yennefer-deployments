import { Resource } from "yennefer-suite-core/dist/domains/studio/resource";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import { GetByIdIn, GetListIn } from "./resource.in";
export interface ResourceRepository {
    getById(parameter: GetByIdIn): Promise<Resource | null>;
    getList(parameter: GetListIn): Promise<ListOutput<Resource>>;
}
export default ResourceRepository;
