import { Resource } from "yennefer-suite-core/dist/domains/studio/resource";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import ResourceRepository from "../core/repositories/resource";
import { ResourceManagers } from "../core/resource-manager";
import { AssignResourceIn, FreeResourceIn, GetAssignInfo, GetByIdIn, GetListIn } from "./resource.in";
export declare type ResourceInteractorConstructor = {
    resourceRepository: ResourceRepository;
    resourceManagers: ResourceManagers;
};
export declare class ResourceInteractor {
    private resourceRepository;
    private resourceManagers;
    constructor({ resourceRepository, resourceManagers, }: ResourceInteractorConstructor);
    private getResourceManager;
    assignResource: (parameters: AssignResourceIn) => Promise<{
        cpu: string;
        gpu: string[];
        ram: string;
    }>;
    freeResource: (parameters: FreeResourceIn) => void;
    getAssignInfo: (parameters: GetAssignInfo) => {
        cpu: [number, number | null][];
        gpu: [number, number | null][];
        ram: [number, number | null][];
    };
    getById: (resourceId: GetByIdIn) => Promise<Resource | null>;
    getList: ({ pagination, nodeName, }: GetListIn) => Promise<ListOutput<Resource>>;
}
export default ResourceInteractor;
