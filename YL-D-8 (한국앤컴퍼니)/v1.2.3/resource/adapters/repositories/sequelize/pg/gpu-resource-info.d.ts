import { Transaction } from "@adapters/repositories/transaction";
import GPUResourceInfoDAO from "../common/dao/gpu-resource-info";
export default class GPUResourceInfoRepository {
    findById(resourceId: number): Promise<GPUResourceInfoDAO | null>;
    findAll(options?: {
        limit?: number;
        offset?: number;
        userId?: number;
        resourceIds?: number[];
    }): Promise<{
        rows: GPUResourceInfoDAO[];
        count: number;
    }>;
    updateMappingInfo(userId: number, deviceIds: number[], transactions?: Transaction): Promise<number[]>;
}
