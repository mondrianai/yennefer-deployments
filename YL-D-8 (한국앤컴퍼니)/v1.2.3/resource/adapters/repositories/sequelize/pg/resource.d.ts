import { Resource, ResourceId } from "yennefer-suite-core/dist/domains/studio/resource";
import { GetListIn } from "@interactors/core/repositories/resource.in";
import Base from "../../../../interactors/core/repositories/resource";
export declare class ResourceRepository implements Base {
    getById: (resourceId: ResourceId | number) => Promise<Resource | null>;
    getList: ({ pagination, nodeName }: GetListIn) => Promise<{
        count: number;
        data: Resource[];
    }>;
}
export default ResourceRepository;
