import { Model, Association } from "sequelize";
import GPUResourceInfoDAO from "./gpu-resource-info";
interface UserGPUResourceMap {
    userId: number;
    gpuResourceId: number;
}
declare class UserGPUResourceMapDAO extends Model<UserGPUResourceMap> {
    readonly userId: number;
    readonly gpuResourceId: number;
    static associations: {
        gpuResources: Association<UserGPUResourceMapDAO, GPUResourceInfoDAO>;
    };
    static associate(models: any): void;
}
export default UserGPUResourceMapDAO;
