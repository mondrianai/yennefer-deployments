import GPUResourceInfoDAO from "./gpu-resource-info";
import ResourceDAOTemp from "./resource";
import UserGPUResourceMapDAO from "./user-gpu-resource";
declare const models: {
    resource: typeof ResourceDAOTemp;
    gpuResource: typeof GPUResourceInfoDAO;
    userGpuResource: typeof UserGPUResourceMapDAO;
};
export declare class ResourceDAO extends ResourceDAOTemp {
}
export default models;
