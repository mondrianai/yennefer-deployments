import GPUResourceInfo from "core/domains/gpu-resource-info";
import { Model, Optional } from "sequelize";
import UserGPUResourceMapDAO from "./user-gpu-resource";
export interface GPUResourceInfoCreation extends Optional<GPUResourceInfo, "id"> {
}
declare class GPUResourceInfoDAO extends Model<GPUResourceInfo, GPUResourceInfoCreation> {
    readonly id: number;
    nodeName: string;
    deviceName: string;
    deviceUUID: string;
    order: number;
    user_gpu_resources: UserGPUResourceMapDAO[];
    static associate(models: any): void;
}
export default GPUResourceInfoDAO;
