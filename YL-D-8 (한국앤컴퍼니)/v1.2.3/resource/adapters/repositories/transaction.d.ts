import { Transaction as SequelizeTransaction } from "sequelize";
export declare type TransactionType = SequelizeTransaction;
export declare class Transaction {
    transaction?: TransactionType;
    start: () => Promise<TransactionType>;
    commit: () => Promise<boolean>;
    rollback: () => Promise<boolean>;
}
