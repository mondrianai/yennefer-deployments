import { ProjectId } from "yennefer-suite-core/dist/domains/studio/project";
import ResourceManagement from "../../interactors/core/resource-manager";
declare type ResourceManagerConstructor = {
    HOST: string;
    PORT: number | null;
    USER: string;
    PASSWORD: string | null;
    PRIVATE_KEY_PATH?: string | null;
};
declare class ResourceManagerImplementation implements ResourceManagement {
    private cpu;
    private gpu;
    private ram;
    private totalMem;
    private ssh;
    constructor(machineName: string | undefined, machineInfo: ResourceManagerConstructor | Array<ResourceManagerConstructor>);
    private connect;
    private eval;
    private init;
    private aggregateMapValue;
    get assignedInfo(): {
        cpu: [number, number | null][];
        gpu: [number, number | null][];
        ram: [number, number | null][];
    };
    get availableCpuCount(): number;
    get availableGpuCount(): number;
    get availableRamSize(): number;
    assignCPU(count: number, projectId: ProjectId | number): Array<number>;
    assignGPU(count: number, projectId: ProjectId | number): Array<number>;
    assignRAM(size: string | number, projectId: ProjectId | number): number;
    freeCPU(projectId: ProjectId | number): number | null;
    freeGPU(projectId: ProjectId | number): number | null;
    freeRAM(projectId: ProjectId | number): number | null;
    freeAll(projectId: ProjectId | number): void;
}
export default ResourceManagerImplementation;
