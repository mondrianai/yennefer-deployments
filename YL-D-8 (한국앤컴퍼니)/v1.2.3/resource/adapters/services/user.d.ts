import UserService from "@interactors/core/services/user";
import { UserGetByIdIn } from "@interactors/core/services/user/user.in";
import { UserGetByIdOut } from "@interactors/core/services/user/user.out";
import { AxiosBase } from "yennefer-suite-core/dist/server/services/core/base";
export declare class AxiosUserService extends AxiosBase implements UserService {
    getById: (parameters: UserGetByIdIn) => Promise<UserGetByIdOut>;
}
export default UserService;
