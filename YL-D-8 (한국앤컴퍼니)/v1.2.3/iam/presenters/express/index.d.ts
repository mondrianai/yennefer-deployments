import { Express } from "express";
declare class ExpressServer {
    framework: Express;
    port: number;
    constructor(port: number);
    startServer(): Promise<void>;
}
declare const _default: ExpressServer;
export default _default;
