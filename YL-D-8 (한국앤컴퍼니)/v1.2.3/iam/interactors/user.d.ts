import User, { UserType } from "@domains/user/entity";
import { Pagination, PaginationSearches } from "@domains/pagination";
import { CreateUserIn, GetUserIn, GetUserListIn, SaveUserIn, UserInteractorIface, UserRepositoryIface as UserRepository, WithdrawalUserRepositoryIface as WithdrawalUserRepository } from "@interactors/interfaces";
import { UserEmail, UserId, UserPassword } from "@domains/user";
declare class UserInteractor implements UserInteractorIface {
    protected userRepository: UserRepository;
    protected withdrawalRepository: WithdrawalUserRepository;
    constructor({ userRepository, withdrawalRepository, }: {
        userRepository: UserRepository;
        withdrawalRepository: WithdrawalUserRepository;
    });
    private hideDeletedUser;
    private hideDeletedUserSearches;
    create: (user: CreateUserIn) => Promise<User>;
    update: (user: SaveUserIn) => Promise<User>;
    getByProperty: <T extends keyof UserType>(parameter: Required<UserType>[T], condition: T) => Promise<User | null>;
    getListByProperty: <T extends keyof UserType>(pagination: Pagination<User>, user?: Partial<Pick<GetUserListIn, T>> | undefined, condition?: "none" | T | T[] | undefined) => Promise<{
        count: number;
        users: User[];
    }>;
    getListByProperties: (pagination: Pagination<User>, users?: Partial<GetUserListIn>[] | undefined, condition?: "id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces" | "none" | ("id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces")[] | undefined) => Promise<{
        count: number;
        users: User[];
    }>;
    getListByPropertySearches: <T extends keyof UserType>(pagination: PaginationSearches<User>, user?: Partial<Pick<GetUserListIn, T>> | undefined, condition?: "none" | T | T[] | undefined) => Promise<{
        count: number;
        users: User[];
    }>;
    getListByPropertiesSearches: (pagination: PaginationSearches<User>, users?: Partial<GetUserListIn>[] | undefined, condition?: "id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces" | "none" | ("id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces")[] | undefined) => Promise<{
        count: number;
        users: User[];
    }>;
    validatePassword: (email: UserEmail, password: UserPassword) => Promise<User>;
    checkUserAuth: (condition: "User" | "Admin" | "Supervisor" | undefined, user: User) => Promise<boolean>;
    getUserToken: (email: UserEmail, lastLoginTime?: Date | undefined) => string;
    sendPasswordResetEmail: (email: UserEmail, token: string) => Promise<boolean>;
    sendAuthEmail: (email: UserEmail, token: string) => Promise<boolean>;
    delete: (userId: UserId) => Promise<User>;
}
declare const _default: UserInteractor;
export default _default;
