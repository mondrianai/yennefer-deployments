import { Model } from "sequelize";
import { WithdrawalUserType, WithdrawalUser } from "@domains/user/withdrawal-user";
import UserDAO from "./user";
interface WithdrawalUserCreation extends WithdrawalUserType {
}
declare class WithdrawalUserDAO extends Model<WithdrawalUser, WithdrawalUserCreation> {
    readonly userId: number;
    date: Date;
    isForced: boolean;
    readonly user?: UserDAO;
    static associate(models: any): void;
}
export default WithdrawalUserDAO;
