declare type SortingType<T> = {
    sortBy?: keyof T | "createdAt";
    orderBy?: "desc" | "asc";
};
declare type searches<T> = [keyof T, string | Array<string> | Array<number>];
declare type PaginationTypeSearches<T> = {
    limit?: number;
    offset?: number | Date;
    search?: searches<T>[];
    head?: "before" | "after";
    sortBy?: keyof T | "createdAt";
    orderBy?: "desc" | "asc";
    exclude?: string;
};
declare type PaginationType<T> = {
    limit?: number;
    offset?: number | Date;
    search?: [keyof T, string | Array<string> | Array<number>];
    head?: "before" | "after";
    sortBy?: keyof T | "createdAt";
    orderBy?: "desc" | "asc";
    exclude?: string;
};
export declare class Sorting<T> {
    sortBy: keyof T | "createdAt";
    orderBy: "desc" | "asc";
    constructor(parameters: SortingType<T>);
}
export declare class Pagination<T> {
    limit: number;
    offset: number | Date;
    search?: [keyof T, string | Array<string> | Array<number>];
    sorting: Sorting<T>;
    head: "before" | "after";
    exclude?: {
        [key: string]: unknown;
    };
    constructor(parameters: PaginationType<T>);
    toQueryString: (json?: boolean) => string;
}
export declare class PaginationSearches<T> {
    limit: number;
    offset: number | Date;
    search?: searches<T>[];
    sorting: Sorting<T>;
    head: "before" | "after";
    exclude?: {
        [key: string]: unknown;
    };
    constructor(parameters: PaginationTypeSearches<T>);
    toQueryString: (json?: boolean) => string;
}
export {};
