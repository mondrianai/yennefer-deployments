export declare const USER_ROLE_LIST: string[];
export declare type UserRoleType = "VIEWER" | "CREATOR" | "ADMIN" | "SUPERVISOR";
export declare class UserRole {
    readonly value: UserRoleType;
    constructor(role: UserRoleType | string);
    static validate(value: unknown): value is UserRoleType;
}
export default UserRole;
