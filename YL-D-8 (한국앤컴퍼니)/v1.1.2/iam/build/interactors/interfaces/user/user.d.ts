import { User, UserEmail, UserId, UserPassword, UserType } from "@domains/user";
import { Pagination } from "@domains/pagination";
import { CreateUserIn, GetUserIn, GetUserListIn } from "./user.in";
import { UserOut } from "./user.out";
export interface UserInteractorIface {
    create(parameter: CreateUserIn): Promise<User>;
    getByProperty<T extends keyof GetUserIn>(user: GetUserIn[T], condition: T): Promise<User | null>;
    getListByProperty<T extends keyof Partial<UserType>>(pagination: Pagination<User>, user?: Partial<Pick<GetUserListIn, T>>, condition?: keyof User | Array<keyof User> | "none"): Promise<UserOut>;
    getListByProperties(pagination: Pagination<User>, users?: Array<Partial<GetUserListIn>>, condition?: keyof User | Array<keyof User> | "none"): Promise<UserOut>;
    update(user: User): Promise<User>;
    validatePassword(email: UserEmail, password: UserPassword): Promise<User>;
    /**
     *
     * @param condition "User" | "Admin" | "Supervisor"
     * @param user User
     * @param object Projct | Dataset | undefined
     *
     * User의 권한을 확인합니다. 만약, object 인자에 특정 값이 있을 경우 해당 User와 object가 Collaborator 인지 확인합니다.
     * condition에 특정 값을 넣지 않을 경우 default로 'User'가 Activate인지 확인을 진행합니다.
     *
     * false 반환시 조건에 만족하지 못하는 User 입니다.
     */
    checkUserAuth(condition: "User" | "Admin", user: User): Promise<boolean>;
    getUserToken(email: UserEmail, lastLoginTime?: Date): string;
    sendPasswordResetEmail(email: UserEmail, token: string): Promise<boolean>;
    sendAuthEmail(email: UserEmail, token: string): Promise<boolean>;
    delete(userId: UserId): Promise<User>;
}
