import { WithdrawalUser } from "@domains/user/withdrawal-user";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import { User, UserId } from "@domains/user";
import { WithdrawalUserOut } from "./withdrawal-user.out";
import { CreateWithdrawUserIn } from "./withdrawal-user.in";
export interface WithdrawUserInteractorIface {
    withdraw(user: CreateWithdrawUserIn): Promise<WithdrawalUser>;
    getListByUserProperty(pagination: {
        withdrawalPagination: Pagination<WithdrawalUser>;
        userPagination: Pagination<User>;
    }): Promise<ListOutput<WithdrawalUserOut>>;
    getById(userId: UserId): Promise<WithdrawalUser | null>;
    cancelWithdrawal(userId: UserId, { token }: {
        token: string;
    }): Promise<User>;
}
