import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import { WithdrawalUser } from "@domains/user/withdrawal-user";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { User, UserId } from "@domains/user";
import { Options } from "@adapters/repositories";
import { WithdrawalUserOut } from "../../../user/withdrawal-user/withdrawal-user.out";
import { CreateWithdrawUserIn } from "../../../user/withdrawal-user/withdrawal-user.in";
export default interface WithdrawalUserRepositoryIface {
    create(withdrawalUser: CreateWithdrawUserIn, options?: Options): Promise<WithdrawalUser>;
    getListByUserProperty(pagination: {
        withdrawalPagination: Pagination<WithdrawalUser>;
        userPagination: Pagination<User>;
    }): Promise<ListOutput<WithdrawalUserOut>>;
    getById(userId: UserId): Promise<WithdrawalUser | null>;
    delete(user: UserId, options?: Options): Promise<number>;
}
