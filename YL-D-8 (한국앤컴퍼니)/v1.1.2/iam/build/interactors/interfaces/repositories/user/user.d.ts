import { UserType, User, UserId } from "@domains/user";
import { Pagination, PaginationSearches } from "@domains/pagination";
import { CreateUserIn, DeleteUserIn, GetUserIn, GetUserListIn, SaveUserIn } from "@interactors/interfaces/user/user.in";
import { Options } from "@adapters/repositories";
export default interface UserRepositoryIface {
    createUser(user: CreateUserIn, options?: any): Promise<User>;
    getUserByProperty<T extends keyof GetUserIn>(parameter: GetUserIn[T], condition: T): Promise<User | null>;
    getUserListByProperty<T extends keyof Partial<UserType>>(pagination: Pagination<User>, user?: Partial<Pick<GetUserListIn, T>>, condition?: T | Array<T> | "none"): Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByProperties(pagination: Pagination<User>, users?: Array<Partial<GetUserListIn>>, condition?: keyof GetUserListIn | Array<keyof GetUserListIn> | "none"): Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByPropertySearches<T extends keyof Partial<UserType>>(pagination: PaginationSearches<User>, user?: Partial<Pick<GetUserListIn, T>>, condition?: T | Array<T> | "none"): Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByPropertiesSearches(pagination: PaginationSearches<User>, users?: Array<Partial<GetUserListIn>>, condition?: keyof GetUserListIn | Array<keyof GetUserListIn> | "none"): Promise<{
        count: number;
        users: Array<User>;
    }>;
    saveUser(user: SaveUserIn, options?: Options): Promise<User>;
    deleteUserFromAdmin(user: DeleteUserIn, options?: Options): Promise<User>;
    deleteUser(user: UserId): Promise<number>;
}
