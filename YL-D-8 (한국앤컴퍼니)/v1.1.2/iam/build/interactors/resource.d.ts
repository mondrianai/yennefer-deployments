import GPUResourceInfo from "core/domains/gpu-resource-info";
import { ResourceService } from "./interfaces/services/resource";
export declare type ResourceInteractorType = {
    resourceService: ResourceService;
};
declare class ResourceInteractor {
    resourceService: ResourceService;
    constructor({ resourceService }: ResourceInteractorType);
    updateUserGpuResources: (parameters: {
        userId: number;
        deviceIds: number[];
        token: string;
    }) => Promise<{
        userId: number;
        updatedAllocationMap: number[];
    }>;
    getUserGpuResources: (parameters: {
        userId: number;
        token: string;
    }) => Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
}
declare const _default: ResourceInteractor;
export default _default;
