import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import { WithdrawalUserRepositoryIface as Base } from "@interactors/interfaces";
import { User, UserId, WithdrawalUser } from "@domains/user";
import { WithdrawalUserDAO } from "../dao";
import { Options } from "..";
declare class WithdrawalUserRepository implements Base {
    create: (withdrawalUser: WithdrawalUser, options?: Options | undefined) => Promise<WithdrawalUser>;
    getListByUserProperty: ({ withdrawalPagination, userPagination, }: {
        withdrawalPagination: Pagination<WithdrawalUser>;
        userPagination: Pagination<User>;
    }) => Promise<{
        count: number;
        data: {
            date: Date;
            email: string;
            id: number;
            isForced: boolean;
            name: string;
            role: import("@domains/user").UserRoleType;
        }[];
    }>;
    getById: (userId: UserId) => Promise<WithdrawalUser | null>;
    delete: (userId: UserId, options?: Options | undefined) => Promise<number>;
    convertToEntity: (dao: WithdrawalUserDAO) => WithdrawalUser;
    convertToEntities: (daoes: Array<WithdrawalUserDAO>) => Array<WithdrawalUser>;
}
declare const _default: WithdrawalUserRepository;
export default _default;
