export declare class UserPassword {
    readonly value: string;
    constructor(password: string);
    equal(plainTextPassword: string): Promise<boolean>;
}
export default UserPassword;
