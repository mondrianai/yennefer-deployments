export declare class UserId {
    readonly value: number;
    constructor(id: number);
}
export default UserId;
