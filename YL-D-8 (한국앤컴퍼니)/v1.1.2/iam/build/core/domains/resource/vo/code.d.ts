export declare class ResourceCode {
    readonly value: string;
    constructor(value: string);
}
export default ResourceCode;
