import { UserRole, UserId, UserPassword, UserEmail } from "./vo";
declare type UserType = {
    id: UserId | number;
    password?: UserPassword | string;
    name: string;
    email: UserEmail | string;
    lastLoginTime?: Date;
    isAllowed?: boolean;
    phone?: string;
    position?: string;
    role?: UserRole | string;
    createdAt: Date;
    updatedAt: Date;
    approvedAt?: Date;
};
export declare class User {
    readonly id: UserId;
    password?: UserPassword;
    name: string;
    email: UserEmail;
    lastLoginTime?: Date;
    isAllowed: boolean;
    phone?: string;
    position?: string;
    role: UserRole;
    createdAt: Date;
    updatedAt: Date;
    approvedAt?: Date;
    constructor(parameters: UserType);
    get isSupervisor(): boolean;
    get isAdmin(): boolean;
}
export default User;
