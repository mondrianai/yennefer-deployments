import { Request, Response, NextFunction } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class UserController extends Base {
    private userInteractor;
    private withdrawalUserInteractor;
    constructor();
    createUser: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    createUserFromAdmin: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getUserBySelf: (req: Request, res: Response) => Response<any, Record<string, any>>;
    getUserById: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getUserList: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getUserListSearches: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    updateUserGpuResources: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    updateUser: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    requestPasswordReset: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    resetPasswordUsingToken: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    requestAuthEmail: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    changeStatusToReqAllowUsingToken: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    /**
     *
     * @param mode "User" | "Admin"
     *
     * 기본적으로 User에 대한 authenticate를 합니다.
     *
     * @example
     * ex) checkBaseAuthority("User") ...
     *
     */
    checkBaseAuthority: (mode: "User" | "Admin" | "Supervisor") => (req: Request, res: Response, next: NextFunction) => any;
    login: (mode: "User" | "Admin") => (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    delete: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    updateRole: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    updateStatus: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
}
declare const _default: UserController;
export default _default;
