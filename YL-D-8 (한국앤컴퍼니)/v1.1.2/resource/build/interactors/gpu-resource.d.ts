import GPUResourceInfoRepository from "@adapters/repositories/sequelize/pg/gpu-resource-info";
declare class GPUResourceInteractor {
    private gpuResourceInfoRepository;
    constructor(gpuResourceInfoRepository: GPUResourceInfoRepository);
    getGPUResources({ userId, resourceIds, }: {
        userId?: number;
        resourceIds?: number[];
    }): Promise<any>;
    getGPUResourceById(resourceId: number): Promise<import("../adapters/repositories/sequelize/common/dao/gpu-resource-info").default | null>;
    updateGPUAllocationMap(userId: number, deviceId: number[]): Promise<number[]>;
}
declare const _default: GPUResourceInteractor;
export default _default;
