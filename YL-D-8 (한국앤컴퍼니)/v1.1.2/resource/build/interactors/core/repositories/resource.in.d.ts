import { Resource, ResourceId, ResourceNodeName } from "yennefer-suite-core/dist/domains/studio/resource";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
export declare type GetByIdIn = ResourceId | number;
export declare type GetListIn = {
    nodeName: ResourceNodeName | string;
    pagination: Pagination<Resource>;
};
