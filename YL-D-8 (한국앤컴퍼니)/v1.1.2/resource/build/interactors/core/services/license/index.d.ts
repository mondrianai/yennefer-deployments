export interface LicenseService {
    getResult(): Promise<boolean>;
}
export default LicenseService;
