import { UserGetByIdIn } from "./user.in";
import { UserGetByIdOut } from "./user.out";
export interface UserService {
    getById(parameters: UserGetByIdIn): Promise<UserGetByIdOut>;
}
export default UserService;
