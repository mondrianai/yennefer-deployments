import { Resource, ResourceId, ResourceNodeName } from "yennefer-suite-core/dist/domains/studio/resource";
import { ProjectId } from "yennefer-suite-core/dist/domains/studio/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
export declare type AssignResourceIn = {
    projectId: ProjectId | number;
    resourceId: ResourceId | number;
    nodeName: ResourceNodeName | string;
};
export declare type FreeResourceIn = {
    projectId: ProjectId | number;
    nodeName: ResourceNodeName | string;
};
export declare type GetAssignInfo = {
    nodeName: ResourceNodeName | string;
};
export declare type GetByIdIn = ResourceId | number;
export declare type GetListIn = {
    pagination: Pagination<Resource>;
    nodeName: ResourceNodeName | string;
};
