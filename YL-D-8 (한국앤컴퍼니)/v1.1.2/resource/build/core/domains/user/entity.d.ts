import { UserRole, UserId, UserPassword, UserEmail, UserRoleType, UserStatusType, UserStatus } from "./vo";
export declare type UserType = {
    id: number;
    password?: string;
    name: string;
    email: string;
    lastLoginTime?: Date;
    phone?: string;
    position?: string;
    role: UserRoleType;
    status: UserStatusType;
    createdAt: Date;
    updatedAt: Date;
    approvedAt?: Date;
    deletedAt?: Date;
};
export declare class User {
    readonly id: UserId;
    password?: UserPassword;
    name: string;
    email: UserEmail;
    lastLoginTime?: Date;
    phone?: string;
    position?: string;
    role: UserRole;
    status: UserStatus;
    createdAt: Date;
    updatedAt: Date;
    approvedAt?: Date;
    deletedAt?: Date;
    constructor(parameters: UserType);
    get isSupervisor(): boolean;
    get isAdmin(): boolean;
    get noPass(): User;
    get privatePassword(): UserPassword;
    get value(): UserType;
}
export default User;
