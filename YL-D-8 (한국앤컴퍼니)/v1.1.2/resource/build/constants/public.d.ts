export declare const SERVER_DOMAIN: string;
export declare const SERVER_SUB_DOMAINS: string[];
export declare const CORS_WHITE_LIST: string[];
export declare const SERVER_PORT: number;
export declare const YENNEFER_IAM_PROTOCOL: "http" | "https";
export declare const YENNEFER_IAM_PORT: number;
export declare const YENNEFER_IAM_URL: string;
declare const _default: {
    readonly SERVER_DOMAIN: string;
    readonly SERVER_SUB_DOMAINS: string[];
    readonly SERVER_PORT: number;
    readonly YENNEFER_IAM_PROTOCOL: "http" | "https";
    readonly YENNEFER_IAM_PORT: number;
    readonly YENNEFER_IAM_URL: string;
    readonly CORS_WHITE_LIST: string[];
};
export default _default;
