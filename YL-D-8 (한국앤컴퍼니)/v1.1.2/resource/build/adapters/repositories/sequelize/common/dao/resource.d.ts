import { Model, Optional } from "sequelize";
import { Resource } from "yennefer-suite-core/dist/domains/studio/resource";
interface ResourceCreation extends Optional<Resource, "id"> {
}
declare class ResourceDAO extends Model<Resource, ResourceCreation> {
    readonly id: number;
    nodeName: string;
    code: string;
    name: string;
    price: number;
    gpu: number;
    cpu: number;
    ram: string;
    static associate(models: any): void;
}
export default ResourceDAO;
