import BaseController from "yennefer-suite-core/dist/server/controllers/base";
import { Request, Response } from "express";
declare class GPUResourceController extends BaseController {
    private gpuResourceInteractor;
    getGPUResourceList(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    getGPUResourceById(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
    updateGPUAllocationMap(req: Request, res: Response): Promise<Response<any, Record<string, any>>>;
}
declare const _default: GPUResourceController;
export default _default;
