import { NextFunction, Request, Response } from "express";
import BaseController from "yennefer-suite-core/dist/server/controllers/base";
declare class AuthenticatorMiddleware extends BaseController {
    private userInteractor;
    authenticate: (mode: "User" | "Admin") => (req: Request, res: Response, next: NextFunction) => any;
}
declare const _default: AuthenticatorMiddleware;
export default _default;
