declare class ExpressServer {
    private port;
    private framework;
    constructor(port: number);
    startServer(): Promise<void>;
}
declare const _default: ExpressServer;
export default _default;
