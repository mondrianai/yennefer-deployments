import Environment from "@yennefer/entities/environment";
import { AxiosBase, APIConfig } from "../core/base";
declare class EnvironmentService extends AxiosBase {
    constructor();
    getEnvironmentById: (environment: Environment, config: APIConfig) => Promise<Environment | null>;
}
declare const _default: EnvironmentService;
export default _default;
