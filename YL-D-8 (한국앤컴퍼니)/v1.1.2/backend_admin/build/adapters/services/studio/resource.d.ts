import Resource from "@yennefer/entities/resource";
import { AxiosBase, APIConfig } from "../core/base";
declare class ResourceService extends AxiosBase {
    constructor();
    getResourceById: (resource: Resource, config: APIConfig) => Promise<Resource | null>;
}
declare const _default: ResourceService;
export default _default;
