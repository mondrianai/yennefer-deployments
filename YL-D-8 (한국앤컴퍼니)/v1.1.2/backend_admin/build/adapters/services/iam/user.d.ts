import User from "@yennefer/entities/user";
import { Pagination } from "@yennefer/entities/pagination";
import { AxiosBase, APIConfig } from "../core/base";
declare class UserService extends AxiosBase {
    constructor();
    getUserById: (user: User, config: APIConfig) => Promise<User | null>;
    getUserListByProperty: (pagination: Pagination<User>, config: APIConfig) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    updateUser: (user: User, config: APIConfig) => Promise<User | null>;
}
declare const _default: UserService;
export default _default;
