import { NextFunction, Request, Response } from "express";
import Base from "./base";
declare class LicenseController extends Base {
    private licenseInteractor;
    constructor();
    validateLicense: (req: Request, res: Response, next: NextFunction) => Promise<null | undefined>;
}
declare const _default: LicenseController;
export default _default;
