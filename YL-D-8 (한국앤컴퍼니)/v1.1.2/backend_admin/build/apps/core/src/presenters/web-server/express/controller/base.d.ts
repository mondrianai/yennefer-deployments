import { Response } from "express";
export default abstract class BaseController {
    protected sendSuccess: <T>(res: Response, code: number, data: T) => Response<any>;
    protected sendFail: <T>(res: Response, code: number | undefined, data: T) => Response<any>;
    protected sendError: <T>(res: Response, code: number, message: string, data?: T | undefined) => Response<any>;
    protected ok: <T>(res: Response, data?: T | undefined) => Response<any>;
    protected fail: (res: Response, error: Error | string) => Response<any>;
    protected created: <T>(res: Response, data: T) => Response<any>;
    protected noContent: <T>(res: Response, data?: T | undefined) => Response<any>;
    protected clientError: <T>(res: Response, data: T) => Response<any>;
    protected unauthorized: (res: Response, message?: string | undefined) => Response<any>;
    protected paymentRequired: (res: Response, message?: string | undefined) => Response<any>;
    protected forbidden: (res: Response, message?: string | undefined) => Response<any>;
    protected notFound: (res: Response, message?: string | undefined) => Response<any>;
    protected conflict: (res: Response, message?: string | undefined) => Response<any>;
    protected tooMany: (res: Response, message?: string | undefined) => Response<any>;
}
export declare type JSendStatus = "error" | "failed" | "success";
export declare type JSendResponse<T> = JSendErrorResponse<T> | JSendSuccessResponse<T> | JSendFailureResponse<T>;
export interface JSendBaseObject {
    status: JSendStatus;
}
export interface JSendErrorResponse<T> extends JSendBaseObject {
    status: "error";
    message: string;
    code?: number;
    data?: T;
}
export interface JSendSuccessResponse<T> extends JSendBaseObject {
    status: "success";
    data: T | null;
}
export interface JSendFailureResponse<T> extends JSendBaseObject {
    status: "failed";
    data: T | null;
}
