import Base from "../base/node";
import { NodeCreateIn, NodeGetByPropertyIn, NodeGetListByPropertyIn, NodeUpdateByIdIn, NodeDeleteByPropertyIn } from "../base/node/node.in";
export default class NodeInteractor implements Base {
    create: (parameters: NodeCreateIn) => Promise<import("yennefer-suite-core/dist/domains/admin/node").Node>;
    getByProperty: (parameters: NodeGetByPropertyIn) => Promise<import("yennefer-suite-core/dist/domains/admin/node").Node>;
    getListByProperty: (parameters: NodeGetListByPropertyIn) => Promise<{
        count: number;
        data: import("yennefer-suite-core/dist/domains/admin/node").NodeWithServices[];
    }>;
    updateById: (parameters: NodeUpdateByIdIn) => Promise<import("yennefer-suite-core/dist/domains/admin/node").Node>;
    deleteByProperty: (parameters: NodeDeleteByPropertyIn) => Promise<number>;
}
