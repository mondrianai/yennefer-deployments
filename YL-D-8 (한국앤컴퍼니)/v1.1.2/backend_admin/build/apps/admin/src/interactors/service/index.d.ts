import Base from "../base/service";
import { ServiceCreateIn, ServiceGetByPropertyIn, ServiceUpdateByIdIn, ServiceDeleteByPropertyIn } from "../base/service/service.in";
export default class ServiceInteractor implements Base {
    create: (parameters: ServiceCreateIn) => Promise<import("yennefer-suite-core/dist/domains/admin/service").Service>;
    getByProperty: (parameters: ServiceGetByPropertyIn) => Promise<import("yennefer-suite-core/dist/domains/admin/service").Service>;
    updateById: (parameters: ServiceUpdateByIdIn) => Promise<import("yennefer-suite-core/dist/domains/admin/service").Service>;
    deleteByProperty: (parameters: ServiceDeleteByPropertyIn) => Promise<number>;
}
