import { NodeCreateIn, NodeGetByPropertyIn, NodeGetListByPropertyIn, NodeUpdateByIdIn, NodeDeleteByPropertyIn } from "./node.in";
import { NodeCreateOut, NodeGetByPropertyOut, NodeGetListByPropertyOut, NodeUpdateByIdOut, NodeDeleteByPropertyOut } from "./node.out";
export default interface NodeInteractorBase {
    create(parameters: NodeCreateIn): Promise<NodeCreateOut>;
    getByProperty(parameters: NodeGetByPropertyIn): Promise<NodeGetByPropertyOut>;
    getListByProperty(parameters: NodeGetListByPropertyIn): Promise<NodeGetListByPropertyOut>;
    updateById(parameters: NodeUpdateByIdIn): Promise<NodeUpdateByIdOut>;
    deleteByProperty(parameters: NodeDeleteByPropertyIn): Promise<NodeDeleteByPropertyOut>;
}
