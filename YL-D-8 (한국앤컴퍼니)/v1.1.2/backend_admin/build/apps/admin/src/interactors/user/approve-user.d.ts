import User from "@yennefer/entities/user";
export declare function approveUser(id: number, token: string): Promise<User | null>;
export declare function rejectUser(id: number, token: string): Promise<User | null>;
