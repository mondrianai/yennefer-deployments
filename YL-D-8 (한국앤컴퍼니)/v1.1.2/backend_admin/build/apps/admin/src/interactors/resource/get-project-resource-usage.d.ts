import User from "@yennefer/entities/user";
import Project from "@yennefer/entities/project";
import { Node } from "yennefer-suite-core/dist/domains/admin/node";
export default function getProjectWithResource(projectId: number, { token, node, }: {
    token: string;
    node: Node;
}): Promise<{
    project: Project;
    user: User;
    logs: import("../../adapters/databases/models/projectResourceLogs").default[];
} | null>;
