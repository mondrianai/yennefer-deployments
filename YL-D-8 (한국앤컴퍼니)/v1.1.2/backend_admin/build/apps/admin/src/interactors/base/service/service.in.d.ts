import { ServiceVO } from "yennefer-suite-core/dist/domains/admin/service";
export declare type ServiceCreateIn = {
    service: ServiceVO;
};
export declare type ServiceGetByPropertyIn = {
    service: ServiceVO;
    condition?: keyof ServiceVO;
};
export declare type ServiceUpdateByIdIn = {
    service: ServiceVO;
};
export declare type ServiceDeleteByPropertyIn = {
    service: ServiceVO;
    condition?: keyof ServiceVO;
};
