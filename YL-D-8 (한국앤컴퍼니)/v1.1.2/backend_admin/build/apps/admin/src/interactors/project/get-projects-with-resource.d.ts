import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import Project from "@yennefer/entities/project";
import { Node } from "yennefer-suite-core/dist/domains/admin/node";
/**
 *  Logic description
 *
 *  1. Get projects by query options
 *  2. Get resource usage data with the array of id which is extracted from above array.
 *  3. Check is the project alive.
 *  4. return
 */
export default function getAllProjectResourceUsage(pagination: Pagination<Project>, { token, node }: {
    token: string;
    node?: Node;
}, isActivated?: boolean): Promise<{
    count: number | undefined;
    projects: ({
        lastActivatedTime: Date | undefined;
        resourceUsage: {
            time: Date | undefined;
            cpu: {
                total: number;
                used: number;
            };
            memory: {
                total: number;
                used: number;
            };
            storage: {
                total: number;
                used: number;
            };
            gpu: {
                total: number;
                used: number;
            };
            traffic: {
                in: number;
                out: number;
            };
            nodeId: number | undefined;
        };
        id?: number | undefined;
        name?: string | undefined;
        description?: string | undefined;
        ownerId?: number | undefined;
        environmentId?: number | undefined;
        resourceId?: number | undefined;
        gpuResourceId?: number | undefined;
        nodeName?: string | undefined;
        lastActivatedAt?: Date | undefined;
        createdAt?: Date | undefined;
        updatedAt?: Date | undefined;
        isRunning?: boolean | undefined;
    } | {
        resourceUsage: null;
        id?: number | undefined;
        name?: string | undefined;
        description?: string | undefined;
        ownerId?: number | undefined;
        environmentId?: number | undefined;
        resourceId?: number | undefined;
        gpuResourceId?: number | undefined;
        nodeName?: string | undefined;
        lastActivatedAt?: Date | undefined;
        createdAt?: Date | undefined;
        updatedAt?: Date | undefined;
        isRunning?: boolean | undefined;
    })[] | undefined;
}>;
