export default function aggregateHardwareLog(): Promise<{
    time: Date;
    cpuTotal: number;
    cpuUsed: number;
    memTotal: number;
    memUsed: number;
    storageTotal: number;
    storageUsed: number;
    gpuTotal: number;
    gpuUsed: number;
} | null>;
