import User from "@yennefer/entities/user";
import { UserListFilter } from "../../entities/filter";
export default function getUserListExceptSupervisor(filter: UserListFilter, sortBy: keyof User, limit: number | undefined, token: string, search?: string): Promise<{
    count: number;
    users: User[];
}>;
