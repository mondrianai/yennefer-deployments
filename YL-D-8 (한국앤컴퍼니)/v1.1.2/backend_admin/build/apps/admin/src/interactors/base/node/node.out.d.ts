import { Node, NodeWithServices } from "yennefer-suite-core/dist/domains/admin/node";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
export declare type NodeCreateOut = Node;
export declare type NodeGetByPropertyOut = Node | NodeWithServices | null;
export declare type NodeGetListByPropertyOut = ListOutput<Node | NodeWithServices>;
export declare type NodeUpdateByIdOut = Node;
export declare type NodeDeleteByPropertyOut = number;
