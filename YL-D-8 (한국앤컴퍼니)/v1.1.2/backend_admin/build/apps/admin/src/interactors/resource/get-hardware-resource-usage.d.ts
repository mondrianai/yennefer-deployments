import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import { ResourceType } from "../../entities/resource";
export default function getHardwareResourceUsage(time: string, node?: Node): Promise<ResourceType[]>;
