import User from "@yennefer/entities/user";
export default function getUserById(id: number, token: string): Promise<User | null>;
