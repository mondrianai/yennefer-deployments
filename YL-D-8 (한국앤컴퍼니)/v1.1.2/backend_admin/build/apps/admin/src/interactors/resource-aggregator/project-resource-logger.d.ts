export default class ProjectLogAggregator {
    aggregate(baseTime?: Date): Promise<({
        gpuTotal: number;
        gpuUsed: number;
        storageTotal: number;
        storageUsed: number;
        memTotal: number;
        memUsed: number;
        cpuTotal: number;
        cpuUsed: number;
        trafficIn: number;
        trafficOut: number;
        time: Date;
        projectId: number;
    } | {
        uuid: string;
        gpuTotal: number;
        gpuUsed: number;
        storageTotal: number;
        storageUsed: number;
        memTotal: number;
        memUsed: number;
        cpuTotal: number;
        cpuUsed: number;
        trafficIn: number;
        trafficOut: number;
        time: Date;
        projectId: number;
    })[] | null>;
    private stats;
    private getStorages;
    private queryGpu;
}
