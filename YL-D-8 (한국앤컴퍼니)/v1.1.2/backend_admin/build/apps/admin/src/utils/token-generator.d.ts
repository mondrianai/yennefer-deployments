import User from "@yennefer/entities/user";
export declare function signToJWT(userId: number): string;
export declare function signWithUserInfo(user: User): string;
