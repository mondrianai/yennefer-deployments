import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import HardwareResourceLogModel from "../models/hardwareResourceLogs";
export interface HardwareResourceLogRepository {
    findAll: (time: string, node: Node) => Promise<HardwareResourceLogModel[]>;
}
