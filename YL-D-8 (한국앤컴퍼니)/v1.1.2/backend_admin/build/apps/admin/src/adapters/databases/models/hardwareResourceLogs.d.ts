import { Model } from "sequelize";
interface HardwareResourceLogAttributes {
    time: Date;
    cpuTotal: number;
    cpuUsed: number;
    memTotal: number;
    memUsed: number;
    storageTotal: number;
    storageUsed: number;
    gpuTotal: number;
    gpuUsed: number;
    nodeId: number;
}
interface HardwareResourceLogCreationAttributes extends HardwareResourceLogAttributes {
}
export default class HardwareResourceLogModel extends Model<HardwareResourceLogAttributes, HardwareResourceLogCreationAttributes> implements HardwareResourceLogAttributes {
    readonly time: Date;
    cpuTotal: number;
    cpuUsed: number;
    memTotal: number;
    memUsed: number;
    storageTotal: number;
    storageUsed: number;
    gpuTotal: number;
    gpuUsed: number;
    nodeId: number;
}
export {};
