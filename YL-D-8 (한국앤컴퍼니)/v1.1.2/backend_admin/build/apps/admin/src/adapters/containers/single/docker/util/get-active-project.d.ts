import Docker from "dockerode";
declare const getActiveProjectList: (docker: Docker) => () => Promise<(string | null)[]>;
export default getActiveProjectList;
