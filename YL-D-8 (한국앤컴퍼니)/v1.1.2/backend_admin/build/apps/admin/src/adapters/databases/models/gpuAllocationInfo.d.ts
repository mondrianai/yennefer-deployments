import { Model } from "sequelize";
interface GPUAllocationInfoAttributes {
    time: Date;
    gpuId: number;
    projectId: number | null;
    ownerId: number | null;
    total: number;
    used: number;
    nodeId: number;
}
interface GPUAllocationInfoCreationAttributes extends GPUAllocationInfoAttributes {
}
export default class GPUAllocationInfoModel extends Model<GPUAllocationInfoAttributes, GPUAllocationInfoCreationAttributes> implements GPUAllocationInfoAttributes {
    readonly time: Date;
    gpuId: number;
    projectId: number | null;
    ownerId: number | null;
    total: number;
    used: number;
    nodeId: number;
}
export {};
