import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import { GPUAllocationInfo } from "../../../entities/resource";
import GPUAllocationInfoModel from "../models/gpuAllocationInfo";
export interface GPUAllocationInfoRepository {
    findAll: (node: Node) => Promise<GPUAllocationInfoModel[]>;
    createGPUAllocationLog: (info: GPUAllocationInfo) => Promise<GPUAllocationInfoModel>;
    bulkCreateGPUAllocationLog: (infoList: GPUAllocationInfo[]) => Promise<GPUAllocationInfo[]>;
}
