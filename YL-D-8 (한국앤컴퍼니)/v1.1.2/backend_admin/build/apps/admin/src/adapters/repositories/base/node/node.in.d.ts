import { Node, NodeVO } from "yennefer-suite-core/dist/domains/admin/node";
import { ServiceVO } from "yennefer-suite-core/dist/domains/admin/service";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
export declare type NodeCreateIn = {
    node: NodeVO;
};
export declare type NodeGetByPropertyIn = {
    node: NodeVO;
    condition?: keyof NodeVO;
};
export declare type NodeGetListByPropertyIn = {
    pagination: Pagination<Node>;
    node?: NodeVO;
    condition?: keyof NodeVO;
    service?: ServiceVO;
};
export declare type NodeUpdateByIdIn = {
    node: NodeVO;
};
export declare type NodeDeleteByPropertyIn = {
    node: NodeVO;
    condition?: keyof NodeVO;
};
