import { Node } from "yennefer-suite-core/dist/domains/admin/node";
import { HardwareResourceLogRepository } from "./interface";
import HardwareResourceLogModel from "../models/hardwareResourceLogs";
export default class SequelizeHRLRepository implements HardwareResourceLogRepository {
    findAll(time: string, node?: Node): Promise<HardwareResourceLogModel[]>;
}
