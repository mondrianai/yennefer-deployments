import { Model, Optional } from "sequelize";
import { Node, NodeVO, ProtocolType } from "yennefer-suite-core/dist/domains/admin/node";
import Service from "yennefer-suite-core/dist/domains/admin/service/entity";
interface NodeCreation extends Optional<NodeVO, "id"> {
}
declare class NodeDAO extends Model<Node, NodeCreation> {
    readonly id: number;
    address: string;
    protocol: ProtocolType;
    name: string;
    description: string;
    services?: Array<Service>;
    static associate(): void;
}
export default NodeDAO;
