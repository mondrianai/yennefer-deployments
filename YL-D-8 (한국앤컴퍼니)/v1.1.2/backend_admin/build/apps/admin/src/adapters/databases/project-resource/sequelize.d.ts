import { ProjectResourceRepository } from "./interface";
import ProjectResourceLogsModel from "../models/projectResourceLogs";
import { QueryOption } from "../../../entities/filter";
export default class SequelizeProjectResourceRepository implements ProjectResourceRepository {
    findAllByProjectId: (projectId: number, options?: {
        limit?: number | undefined;
        offset?: number | undefined;
        nodeId?: number | undefined;
    } | undefined) => Promise<ProjectResourceLogsModel[]>;
    findAllWithCount(options?: QueryOption): Promise<{
        rows: ProjectResourceLogsModel[];
        count: number;
    }>;
    findLatestLogsByIdArray(ids: number[]): Promise<ProjectResourceLogsModel[]>;
}
