import { HTTPError } from "@yennefer/utils/errors";
import { Response } from "express";
declare type ErrorBody<T> = string | {
    message: string;
    code?: number;
    data?: T;
};
declare class ResponseHandler {
    ok<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    created<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    noContent(res: Response): void;
    clientError<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    unauthorized<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    forbidden<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    notFound<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    conflict<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    tooManyRequest<T extends Record<string, unknown>>(res: Response, data: T): jsend.JSendObject;
    internalError<T>(res: Response, error: ErrorBody<T>): jsend.JSendObject;
    unavailable<T>(res: Response, error: ErrorBody<T>): jsend.JSendObject;
    handleHTTPError(res: Response, error: HTTPError): Response<any, Record<string, any>>;
}
declare const _default: ResponseHandler;
export default _default;
