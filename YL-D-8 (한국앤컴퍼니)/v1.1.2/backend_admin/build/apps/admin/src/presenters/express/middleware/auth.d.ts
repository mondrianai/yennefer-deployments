import { NextFunction, Request, Response } from "express";
export declare const authenticate: (req: Request, res: Response, next: NextFunction) => any;
export declare function isApproved(req: Request, res: Response, next: NextFunction): void | jsend.JSendObject;
export declare function isAdmin(req: Request, res: Response, next: NextFunction): void | jsend.JSendObject;
export declare function isSupervisor(req: Request, res: Response, next: NextFunction): void | jsend.JSendObject;
export declare function checkMondrianKey(req: Request, res: Response, next: NextFunction): void | jsend.JSendObject;
