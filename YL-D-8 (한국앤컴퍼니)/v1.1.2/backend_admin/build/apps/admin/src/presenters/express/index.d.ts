import WebServer from "@yennefer/core/src/presenters/web-server/base";
declare class ExpressServer extends WebServer {
    constructor(port: number);
    startServer(): Promise<void>;
}
declare const _default: ExpressServer;
export default _default;
