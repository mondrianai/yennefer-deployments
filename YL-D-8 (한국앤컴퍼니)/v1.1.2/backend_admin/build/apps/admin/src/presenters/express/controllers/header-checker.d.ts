import { NextFunction, Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class HeaderCheckerController extends Base {
    checkHeader(req: Request, res: Response, next: NextFunction): void | jsend.JSendObject;
}
declare const _default: HeaderCheckerController;
export default _default;
