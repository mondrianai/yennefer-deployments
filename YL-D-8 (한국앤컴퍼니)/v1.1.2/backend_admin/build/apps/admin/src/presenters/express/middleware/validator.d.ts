import { NextFunction, Request, Response } from "express";
export declare const isValidId: (property: string) => (req: Request, res: Response, next: NextFunction) => void | jsend.JSendObject;
export declare const isValidPassword: (req: Request, res: Response, next: NextFunction) => void | jsend.JSendObject;
