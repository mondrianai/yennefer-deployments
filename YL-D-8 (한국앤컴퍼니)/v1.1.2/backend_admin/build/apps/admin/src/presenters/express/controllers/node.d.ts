import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class NodeController extends Base {
    create: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getListByService: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    updateById: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    deleteById: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
}
declare const _default: NodeController;
export default _default;
