declare type Config = {
    AGGREGATION_DELAY_MSEC: number;
    NODE_ID: number;
    INTERVAL?: boolean;
};
declare const config: Config;
export default config;
