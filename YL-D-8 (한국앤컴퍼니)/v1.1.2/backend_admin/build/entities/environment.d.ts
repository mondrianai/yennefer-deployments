/// <reference types="node" />
declare type EnvironmentType = {
    id?: number;
    name?: string;
    processor?: string;
    description?: string;
    reference?: string;
    containerImage?: string;
    backgroundImage?: Buffer;
    author?: string;
};
declare class Environment {
    id?: number;
    name?: string;
    processor?: string;
    description?: string;
    reference?: string;
    containerImage?: string;
    backgroundImage?: Buffer;
    author?: string;
    constructor(parameters: EnvironmentType);
}
export default Environment;
