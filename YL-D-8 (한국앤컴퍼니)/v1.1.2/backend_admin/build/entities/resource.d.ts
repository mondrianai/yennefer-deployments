declare type ResourceType = {
    id?: number;
    code?: string;
    name?: string;
    price?: number;
    gpu?: number;
    cpu?: number;
    ram?: string;
};
declare class Resource {
    id?: number;
    code?: string;
    name?: string;
    price?: number;
    gpu?: number;
    cpu?: number;
    ram?: string;
    constructor(parameters: ResourceType);
}
export default Resource;
