import Project from "./project";
interface Snapshot {
    id: number;
    title: string;
    description: string;
    environmentId: number;
    resourceId: number;
    publisherId: number;
    projectId: number;
    createdAt: Date;
    updatedAt: Date;
}
export default class ProjectSnapshot {
    id?: number;
    title?: string;
    description?: string;
    environmentId?: number;
    resourceId?: number;
    publisherId?: number;
    projectId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(params: Partial<Snapshot>);
    static convertProjectToSnapshot(project: Project): ProjectSnapshot;
}
export {};
