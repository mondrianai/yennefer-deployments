export declare const isFullUrl: (path: string) => boolean;
export declare const isEmail: (email: string) => boolean;
export declare const isCelluar: (celluar: string) => boolean;
export declare const isIPv4: (ip: string) => boolean;
export declare const validatePassword: (password: string) => boolean;
