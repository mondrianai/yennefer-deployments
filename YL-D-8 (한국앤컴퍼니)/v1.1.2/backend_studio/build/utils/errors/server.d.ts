export declare enum ServerErrorStatus {
    ERROR = "error",
    CRITICAL_ERROR = "critical_error",
    INVALID_ARGUMENTS = "invalid_arguments"
}
export declare class ServerError extends Error {
    status: ServerErrorStatus;
    constructor(message: string | undefined, status: ServerErrorStatus);
    static isServerError(e: unknown): e is ServerError;
}
