declare type UserProjectType = {
    userId?: number;
    projectId?: number;
    createdAt?: Date;
    updatedAt?: Date;
};
declare class UserProject {
    userId?: number;
    projectId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(parameters: UserProjectType);
}
export default UserProject;
