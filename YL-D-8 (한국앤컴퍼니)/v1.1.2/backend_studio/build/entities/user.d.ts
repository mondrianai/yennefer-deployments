export declare enum UserRole {
    VIEWER = 0,
    CREATOR = 1,
    ADMIN = 2,
    SUPERVISOR = 3
}
export declare enum UserStatus {
    AUTH_EMAIL = 0,
    REQ_ALLOW = 1,
    NORMAL = 2,
    DISABLED = 3,
    REQ_DELETED = 4,
    DELETED = 5
}
declare type UserType = {
    id?: number;
    password?: string;
    name?: string;
    email?: string;
    lastLoginTime?: Date;
    isAllowed?: boolean;
    phone?: string;
    position?: string;
    role?: keyof typeof UserRole;
    createdAt?: Date;
    updatedAt?: Date;
    approvedAt?: Date;
    status?: keyof typeof UserStatus;
};
declare class User {
    password?: string;
    id?: number;
    name?: string;
    email?: string;
    lastLoginTime?: Date;
    isAllowed?: boolean;
    phone?: string;
    position?: string;
    role?: keyof typeof UserRole;
    createdAt?: Date;
    updatedAt?: Date;
    approvedAt?: Date;
    status?: keyof typeof UserStatus;
    constructor(parameters: UserType);
    get noPass(): User;
    get privatePassword(): string;
    get isSupervisor(): boolean;
    get isAdmin(): boolean;
}
export declare type UserToken = {
    email: string;
    lastLoginTime: Date;
    requestTime: Date;
};
export default User;
