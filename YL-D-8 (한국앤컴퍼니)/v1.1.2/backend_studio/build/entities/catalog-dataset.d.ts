declare type CatalogDatasetType = {
    id?: number;
    userId?: number;
    topicId?: number | Array<number>;
    title?: string;
    description?: string;
    license?: string;
    size?: number;
    updatedAt?: Date;
    createdAt?: Date;
};
declare class CatalogDataset {
    id?: number;
    userId?: number;
    topicId?: number | number[];
    title?: string;
    description?: string;
    license?: string;
    size?: number;
    updatedAt?: Date;
    createdAt?: Date;
    constructor(parameter: CatalogDatasetType);
}
export default CatalogDataset;
