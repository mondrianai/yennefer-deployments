/// <reference types="node" />
interface HubUserType {
    id: number;
    name: string;
    admin: boolean;
    state: string;
    encryptedAuthState: Buffer;
    cookieId: string;
    lastActivity: Date;
    created: Date;
}
export default class HubUser {
    id?: number;
    name?: string;
    admin?: boolean;
    state?: string;
    encryptedAuthState?: Buffer;
    cookieId?: string;
    lastActivity?: Date;
    created?: Date;
    constructor(params: Partial<HubUserType>);
}
export {};
