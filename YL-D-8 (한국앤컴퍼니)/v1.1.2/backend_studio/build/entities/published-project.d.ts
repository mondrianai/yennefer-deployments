interface PublishedProjectType {
    snapshotId: number;
    projectId: number;
    isPublished: boolean;
    createdAt: Date;
    updatedAt: Date;
}
export default class PublishedProject {
    snapshotId?: number;
    projectId?: number;
    isPublished?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(params: Partial<PublishedProjectType>);
}
export {};
