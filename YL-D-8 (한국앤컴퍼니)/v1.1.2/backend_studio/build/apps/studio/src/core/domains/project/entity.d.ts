import { NodeName } from "../node";
import { ProjectId } from "./vo";
import { UserId } from "../user";
import { ResourceId } from "../resource";
export declare type ProjectType = {
    id: number;
    environmentId: number;
    resourceId: number;
    ownerId: number;
    name: string;
    description?: string;
    nodeName: string;
    lastActivatedAt?: Date;
    isRunning?: boolean;
    createdAt: Date;
    updatedAt: Date;
};
export declare class Project {
    readonly id: ProjectId;
    name: string;
    description?: string;
    ownerId: UserId;
    environmentId: number;
    resourceId: ResourceId;
    lastActivatedAt?: Date;
    nodeName: NodeName;
    isRunning: boolean;
    createdAt: Date;
    updatedAt: Date;
    constructor(parameters: ProjectType);
    get value(): Project;
    static validate: (object: any) => object is Project;
}
export default Project;
