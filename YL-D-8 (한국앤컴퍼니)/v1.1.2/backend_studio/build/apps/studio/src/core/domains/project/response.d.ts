import { JSendResponse } from "yennefer-suite-core/dist/server/jsend";
export declare type GetAllProjectCountsData = {
    activated: number;
    total: number;
};
export declare type GetAllProjectCountsResponse = JSendResponse<GetAllProjectCountsData>;
