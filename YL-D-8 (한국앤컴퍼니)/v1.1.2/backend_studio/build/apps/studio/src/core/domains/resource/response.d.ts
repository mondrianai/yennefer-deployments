import { JSendResponse } from "yennefer-suite-core/dist/server/jsend";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import Resource from "./entity";
export declare type GetByIdData = {
    resource: Resource;
} | null;
export declare type GetListData = ListOutput<Resource>;
export declare type AssignResourceData = {
    cpu: any;
    gpu: any;
    ram: any;
};
export declare type FreeResourceData = null;
export declare type GetByIdResponse = JSendResponse<GetByIdData>;
export declare type GetListResponse = JSendResponse<GetListData>;
export declare type AssignResourceResponse = JSendResponse<AssignResourceData>;
export declare type FreeResourceResponse = JSendResponse<FreeResourceData>;
