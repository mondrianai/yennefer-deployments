import { NodeName } from "../node";
import { ResourceId, ResourceCode } from "./vo";
export declare type ResourceType = {
    id: ResourceId | number;
    code: ResourceCode | string;
    name: string;
    nodeName: string | NodeName;
    price: number;
    gpu: number;
    cpu: number;
    ram: string;
};
export declare class Resource {
    readonly id: ResourceId | number;
    code: ResourceCode | string;
    name: string;
    nodeName: NodeName | string;
    price: number;
    gpu: number;
    cpu: number;
    ram: string;
    constructor({ id, code, name, nodeName, price, gpu, cpu, ram, }: ResourceType);
    get value(): Resource;
}
export default Resource;
