import Environment from "@yennefer/entities/environment";
import { Pagination } from "@yennefer/entities/pagination";
import EnvironmentDAO from "../common/dao/environment";
import Base from "../../../../interactors/interfaces/base-repositories/environment";
declare class EnvironmentRepository extends Base {
    getEnvironmentByProperty: (environment: Environment, condition: keyof Environment) => Promise<Environment | null>;
    getEnvironmentListByProperty: (pagination: Pagination<Environment>, environment?: Environment | undefined, condition?: "id" | "name" | "description" | "none" | "processor" | "reference" | "containerImage" | "backgroundImage" | "author" | undefined) => Promise<Array<Environment>>;
    convertToEntity: (dao: EnvironmentDAO) => Environment;
    convertToEntities: (daoes: Array<EnvironmentDAO>) => Array<Environment>;
}
declare const _default: EnvironmentRepository;
export default _default;
