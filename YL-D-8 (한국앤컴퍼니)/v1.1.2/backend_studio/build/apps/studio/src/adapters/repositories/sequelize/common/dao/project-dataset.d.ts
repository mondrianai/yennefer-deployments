import { Model, Optional, Association } from "sequelize";
import ProjectDataset from "@yennefer/entities/project-dataset";
import ProjectDAO from "./project";
interface ProjectDatasetCreation extends Optional<ProjectDataset, "datasetId" | "projectId"> {
}
declare class ProjectDatasetDAO extends Model<ProjectDataset, ProjectDatasetCreation> {
    readonly studioDatasetId: number;
    readonly projectId: number;
    static associations: {
        projects: Association<ProjectDatasetDAO, ProjectDAO>;
    };
    static associate(models: any): void;
}
export default ProjectDatasetDAO;
