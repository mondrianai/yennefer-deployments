import { PMSService, ActivateParams, GetCookieParams, DeactivateParams, DeleteContainerParams, GetActivatedProjectIdsByUserIdsParams, InitWorkspaceParams, MapActivatedProjectIdsResult, MapActivatedProjectIdsParams, GetProjectAuthTokenParams } from "@interactors/interfaces/services/pms";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
export declare class AxiosPMSService implements PMSService {
    private url;
    private nodeName;
    constructor(url: string, nodeName: string);
    private responseErrorHandler;
    getCookie: (parameters: GetCookieParams) => Promise<any>;
    activate: (parameters: ActivateParams) => Promise<any>;
    deactivate: (parameters: DeactivateParams) => Promise<boolean>;
    deleteContainer: (parameters: DeleteContainerParams) => Promise<boolean>;
    initWorkspace: (parameters: InitWorkspaceParams) => Promise<boolean>;
    mapActivatedProjectIds: (parameters: MapActivatedProjectIdsParams) => Promise<MapActivatedProjectIdsResult>;
    getActivatedProjectIdsByUserIds: (parameters: GetActivatedProjectIdsByUserIdsParams) => Promise<ListOutput<number>>;
    getProjectAuthToken: (parameters: GetProjectAuthTokenParams) => Promise<string>;
}
export default AxiosPMSService;
