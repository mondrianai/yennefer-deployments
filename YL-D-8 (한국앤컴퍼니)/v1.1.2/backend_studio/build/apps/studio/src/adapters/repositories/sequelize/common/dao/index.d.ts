import ForkedProjectDAOTemp from "./forked-project";
import ProjectDAOTemp from "./project";
import UserProjectDAOTemp from "./user-project";
import ReportDAOTemp from "./report";
import EnvironmentDAOTemp from "./environment";
import ResourceDAOTemp from "./resource";
import AssetDAOTemp from "./asset";
import ProjectDatasetDAOTemp from "./project-dataset";
declare const models: {
    project: typeof ProjectDAOTemp;
    forkedProject: typeof ForkedProjectDAOTemp;
    userProject: typeof UserProjectDAOTemp;
    environment: typeof EnvironmentDAOTemp;
    resource: typeof ResourceDAOTemp;
    report: typeof ReportDAOTemp;
    asset: typeof AssetDAOTemp;
    projectDataset: typeof ProjectDatasetDAOTemp;
};
export declare class ProjectDAO extends ProjectDAOTemp {
}
export declare class ForkedProjectDAO extends ForkedProjectDAOTemp {
}
export declare class UserProjectDAO extends UserProjectDAOTemp {
}
export declare class ReportDAO extends ReportDAOTemp {
}
export declare class EnvironmentDAO extends EnvironmentDAOTemp {
}
export declare class ResourceDAO extends ResourceDAOTemp {
}
export declare class AssetDAO extends AssetDAOTemp {
}
export declare class ProjectDatasetDAO extends ProjectDatasetDAOTemp {
}
export default models;
