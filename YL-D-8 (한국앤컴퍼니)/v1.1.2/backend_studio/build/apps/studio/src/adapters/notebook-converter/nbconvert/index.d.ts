import { NotebookConverter, NbConvertOptions } from "@interactors/interfaces/notebook-converter/base";
import { Client as FsmClient } from "yennefer-suite-core/dist/server/fsm-client";
export declare type NbConverterType = {
    port: number;
    baseUrl: string;
    protocol: "http" | "https";
};
export declare class NbConverter implements NotebookConverter {
    fsmClient: FsmClient;
    constructor({ port, baseUrl, protocol, }: NbConverterType);
    convert(notebookPath: string, output: string, options?: NbConvertOptions): Promise<void>;
}
