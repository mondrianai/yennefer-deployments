import ProjectDataset from "@yennefer/entities/project-dataset";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { ProjectDatasetDAO } from "../common/dao";
import Base from "../../../../interactors/interfaces/base-repositories/project-dataset";
import { Options } from "../..";
declare class ProjectDatasetRepository extends Base {
    createProjectDataset: (projectDataset: ProjectDataset, options?: Options | undefined) => Promise<ProjectDataset>;
    getProjectDatasetByPk: (projectDataset: ProjectDataset) => Promise<ProjectDataset | null>;
    getProjectDatasetByProperty: (projectDataset: ProjectDataset, condition: keyof ProjectDataset) => Promise<ProjectDataset | null>;
    getProjectDatasetListByProperty: (pagination: Pagination<ProjectDataset>, projectDataset?: ProjectDataset | undefined, condition?: "projectId" | "none" | "datasetId" | undefined) => Promise<ListOutput<ProjectDataset>>;
    deleteStudioDatasetByPk: (projectDataset: ProjectDataset, options?: Options | undefined) => Promise<number>;
    convertToEntity: (dao: ProjectDatasetDAO) => ProjectDataset;
    convertToEntities: (daoes: Array<ProjectDatasetDAO>) => Array<ProjectDataset>;
}
declare const _default: ProjectDatasetRepository;
export default _default;
