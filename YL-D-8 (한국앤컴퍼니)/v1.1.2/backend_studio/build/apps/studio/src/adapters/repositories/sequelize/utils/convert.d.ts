import { FindOptions } from "sequelize";
import { Pagination } from "@yennefer/entities/pagination";
declare const convertPaginationToFindOptions: <T>(pagination: Pagination<T>, defaultOptions?: FindOptions<T>, isOrdered?: boolean, isSearch?: boolean) => FindOptions<any>;
export default convertPaginationToFindOptions;
