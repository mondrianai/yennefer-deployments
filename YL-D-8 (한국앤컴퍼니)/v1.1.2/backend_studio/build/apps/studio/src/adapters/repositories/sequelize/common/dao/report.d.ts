import { Model, Optional } from "sequelize";
import Report from "@yennefer/entities/report";
interface ReportCreation extends Optional<Report, "id"> {
}
declare class ReportDAO extends Model<Report, ReportCreation> {
    readonly id: number;
    projectId: number;
    publisherId: number;
    title: string;
    description: string;
    isUrlVisible: boolean;
    sourceFile: string;
    htmlName: string;
    language: string;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    static associate(models: any): void;
}
export default ReportDAO;
