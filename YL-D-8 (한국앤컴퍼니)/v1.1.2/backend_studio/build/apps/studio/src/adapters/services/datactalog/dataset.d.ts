import CatalogDataset from "@yennefer/entities/catalog-dataset";
import DatasetResource from "@yennefer/entities/dataset-resource";
import { Pagination } from "@yennefer/entities/pagination";
import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import { AxiosBase, APIConfig } from "yennefer-suite-core/dist/server/services/core/base";
declare class ProjectService extends AxiosBase {
    constructor();
    getCatalogDatasetById: (catalogDataset: CatalogDataset, config: APIConfig) => Promise<CatalogDataset & {
        resources: Array<CatalogDataset>;
    }>;
    getCatalogDatasetList: (pagination: Pagination<CatalogDataset>, config: APIConfig) => Promise<ListOutput<CatalogDataset & {
        resources: Array<DatasetResource>;
    }>>;
}
declare const _default: ProjectService;
export default _default;
