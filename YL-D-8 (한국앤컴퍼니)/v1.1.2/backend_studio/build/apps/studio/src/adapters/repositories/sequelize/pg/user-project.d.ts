import UserProject from "@yennefer/entities/user-project";
import User from "@yennefer/entities/user";
import Project from "@yennefer/entities/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { UserProjectDAO } from "../common/dao";
import Base from "../../../../interactors/interfaces/base-repositories/user-project";
import { Options } from "../..";
declare class UserProjectRepository implements Base {
    createUserProject: (user: User, project: Project, options?: Options | undefined) => Promise<UserProject>;
    getProjectIdsByUserId: (user: User, nodeName?: string | undefined) => Promise<Array<number>>;
    getUserIdByProjectId: (project: Project) => Promise<Array<number>>;
    relateUserProject: (user: User, project: Project) => Promise<boolean>;
    addUserListToProject: (users: Array<User>, project: Project) => Promise<number>;
    deleteUserListOnProject: (users: Array<User>, project: Project) => Promise<number>;
    getSharedProjectCountByUserId: (parameters: {
        user: User;
        nodeName: string;
    }) => Promise<number>;
    getSharedProjectListByUserId: (parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
    }) => Promise<ListOutput<Project>>;
    getAllProjectListByUserId: (parameters: {
        pagination: Pagination<UserProject>;
        user: User;
        nodeName: string;
    }) => Promise<ListOutput<Project>>;
    convertToEntity: (dao: UserProjectDAO) => UserProject;
    convertToEntities: (daoes: Array<UserProjectDAO>) => Array<UserProject>;
}
declare const _default: UserProjectRepository;
export default _default;
