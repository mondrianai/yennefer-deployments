import ReportSnapshot from "@yennefer/entities/report-snapshot";
import { Pagination } from "@yennefer/entities/pagination";
import { AxiosBase, APIConfig } from "yennefer-suite-core/dist/server/services/core/base";
declare class ReportService extends AxiosBase {
    constructor();
    getPublishedReportList: (pagination: Pagination<ReportSnapshot>, config: APIConfig) => Promise<{
        counter: number;
        reports: ReportSnapshot[];
    } | null>;
    createReportSnapshot: (reportSnapshot: ReportSnapshot, config: APIConfig) => Promise<ReportSnapshot | null>;
    getReportSnapshotById: (id: number, config: APIConfig) => Promise<ReportSnapshot | null>;
    updateReportSnapshot: (report: ReportSnapshot, config: APIConfig) => Promise<ReportSnapshot | null>;
    deleteReportSnapshot: (id: number, config: APIConfig) => Promise<{} | null>;
}
declare const _default: ReportService;
export default _default;
