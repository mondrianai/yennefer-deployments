import { Model } from "sequelize";
import ForkedProject from "@yennefer/entities/forked-project";
interface ForkedProjectCreation extends ForkedProject {
}
declare class ForkedProjectDAO extends Model<ForkedProject, ForkedProjectCreation> {
    projectId: number;
    from: string;
    publisherId: number;
    snapshotId: number;
    static associate(): void;
}
export default ForkedProjectDAO;
