import Environment from "@yennefer/entities/environment";
import { EnvironmentRepository } from "@adapters/repositories";
import { Pagination } from "@yennefer/entities/pagination";
export default class EnvironmentInteractor {
    protected environmentRepository: typeof EnvironmentRepository;
    constructor();
    getEnvironmentByProperty: (environment: Environment, condition: keyof Environment) => Promise<Environment | null>;
    getEnvironmentListByProperty: (pagination: Pagination<Environment>, environment?: Environment | undefined, condition?: "id" | "name" | "description" | "none" | "processor" | "reference" | "containerImage" | "backgroundImage" | "author" | undefined) => Promise<Array<Environment>>;
}
