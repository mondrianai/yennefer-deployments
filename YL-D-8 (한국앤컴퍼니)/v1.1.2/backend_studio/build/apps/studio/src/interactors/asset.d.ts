import Asset from "@yennefer/entities/asset";
import Project from "@yennefer/entities/project";
import { Pagination } from "yennefer-suite-core/dist/server/pagination";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
export default class AssetInteractor {
    createAsset: (asset: Asset) => Promise<Asset>;
    getAssetByProperty: (asset: Asset, condition: keyof Asset) => Promise<Asset | null>;
    getAssetListByProperty: (pagination: Pagination<Asset>, asset?: Asset | undefined, condition?: "createdAt" | "id" | "projectId" | "updatedAt" | "none" | "mimetype" | "size" | "fileName" | undefined) => Promise<ListOutput<Asset>>;
    deleteAsset: (asset: Asset, project: Project) => Promise<void>;
}
