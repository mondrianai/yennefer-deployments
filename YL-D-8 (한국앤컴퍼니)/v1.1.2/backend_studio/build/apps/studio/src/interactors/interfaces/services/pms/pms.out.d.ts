import ListOutput from "yennefer-suite-core/dist/server/list-output";
export declare type ActivateResult = Array<string> | null;
export declare type DeactivateResult = boolean;
export declare type DeleteContainerResult = boolean;
export declare type InitWorkspaceResult = boolean;
export declare type MapActivatedProjectIdsResult = {
    [key: string]: boolean;
};
export declare type GetActivatedProjectIdsByUserIdsResult = ListOutput<number>;
export declare type GetProjectAuthTokenResult = string;
