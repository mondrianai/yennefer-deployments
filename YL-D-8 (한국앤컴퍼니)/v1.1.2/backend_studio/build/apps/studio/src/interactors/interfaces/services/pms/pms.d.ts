import { ActivateParams, DeactivateParams, DeleteContainerParams, GetActivatedProjectIdsByUserIdsParams, GetProjectAuthTokenParams, InitWorkspaceParams, MapActivatedProjectIdsParams, GetCookieParams } from "./pms.in";
import { ActivateResult, DeactivateResult, DeleteContainerResult, GetActivatedProjectIdsByUserIdsResult, GetProjectAuthTokenResult, InitWorkspaceResult, MapActivatedProjectIdsResult } from "./pms.out";
export interface PMSService {
    activate(parameters: ActivateParams): Promise<ActivateResult>;
    getCookie(parameters: GetCookieParams): Promise<ActivateResult>;
    deactivate(parameters: DeactivateParams): Promise<DeactivateResult>;
    deleteContainer(parameters: DeleteContainerParams): Promise<DeleteContainerResult>;
    getProjectAuthToken(parameters: GetProjectAuthTokenParams): Promise<GetProjectAuthTokenResult>;
    getActivatedProjectIdsByUserIds(parameters: GetActivatedProjectIdsByUserIdsParams): Promise<GetActivatedProjectIdsByUserIdsResult>;
    mapActivatedProjectIds(parameters: MapActivatedProjectIdsParams): Promise<MapActivatedProjectIdsResult>;
    initWorkspace(parameters: InitWorkspaceParams): Promise<InitWorkspaceResult>;
}
export default PMSService;
