import Environment from "@yennefer/entities/environment";
import { Pagination } from "@yennefer/entities/pagination";
export default abstract class BaseEnvironmentRepository {
    abstract getEnvironmentByProperty(environment: Environment, condition: keyof Environment): Promise<Environment | null>;
    abstract getEnvironmentListByProperty(pagination: Pagination<Environment>, environment?: Environment, condition?: keyof Environment | "none"): Promise<Array<Environment>>;
}
