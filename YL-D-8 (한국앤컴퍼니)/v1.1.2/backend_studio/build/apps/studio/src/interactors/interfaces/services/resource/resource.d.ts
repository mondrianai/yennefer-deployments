import GPUResourceInfo from "core/domains/gpu-resource-info";
import { AssignResourceIn, FreeResourceIn, GetByIdIn, GetListIn } from "./resource.in";
import { AssignResourceOut, FreeResourceOut, GetByIdOut, GetListOut } from "./resource.out";
export interface ResourceService {
    getById(parameters: GetByIdIn): Promise<GetByIdOut>;
    getList(parameters: GetListIn): Promise<GetListOut>;
    assignResource(parameters: AssignResourceIn): Promise<AssignResourceOut>;
    freeResource(parameters: FreeResourceIn): Promise<FreeResourceOut>;
    getGPUResourceById(gpuResourceId: number, { token }: {
        token: string;
    }): Promise<GPUResourceInfo | null>;
    getGPUResourcesByIds(gpuResourceIds: number[], { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
    getGPUResourcesByUserId(userId: number, { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
}
export default ResourceService;
