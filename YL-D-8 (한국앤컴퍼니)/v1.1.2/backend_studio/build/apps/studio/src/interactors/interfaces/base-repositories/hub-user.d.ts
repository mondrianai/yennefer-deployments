import HubUser from "@yennefer/entities/hub-user";
export default abstract class BaseHubUserRepository {
    abstract getHubUserByProperty(hubUser: HubUser, condition: keyof HubUser): Promise<HubUser | null>;
}
