import { Request, Response, NextFunction } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class AuthController extends Base {
    private userInteractor;
    private projectInteractor;
    private environmentInteractor;
    constructor();
    authenticate: (mode: "User" | "Admin", special?: ["Project", "Collaborator" | "Owner"] | undefined) => (req: Request, res: Response, next: NextFunction) => any;
}
declare const _default: AuthController;
export default _default;
