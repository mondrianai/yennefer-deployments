import { NextFunction, Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class AssetController extends Base {
    private assetInteractor;
    constructor();
    downloadAsset: (req: Request, res: Response, next: NextFunction) => Promise<void>;
    createAsset: (req: Request, res: Response) => Promise<any>;
    getAssetById: (req: Request, res: Response) => Promise<any>;
    getAssetListByProjectId: (req: Request, res: Response) => Promise<any>;
    deleteAsset: (req: Request, res: Response) => Promise<any>;
}
declare const _default: AssetController;
export default _default;
