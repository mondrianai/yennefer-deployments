import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class ReportController extends Base {
    private reportInteractor;
    private userInteractor;
    constructor();
    createReport: (req: Request, res: Response) => Promise<any>;
    getReportById: (req: Request, res: Response) => Promise<any>;
    getReportByToken: (req: Request, res: Response) => Promise<any>;
    getReportListByProjectId: (req: Request, res: Response) => Promise<any>;
    updateReport: (req: Request, res: Response) => Promise<any>;
    deleteReport: (req: Request, res: Response) => Promise<any>;
    changeReportVisible: (req: Request, res: Response) => Promise<any>;
    changeAccessibleOfReportSnapshot: (req: Request, res: Response) => Promise<any>;
    overwriteReport: (req: Request, res: Response) => Promise<any>;
}
declare const _default: ReportController;
export default _default;
