import { Request, Response, NextFunction } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
declare class AuthCtrl extends Base {
    authenticate: (req: Request, res: Response, next: NextFunction) => any;
}
declare const _default: AuthCtrl;
export default _default;
