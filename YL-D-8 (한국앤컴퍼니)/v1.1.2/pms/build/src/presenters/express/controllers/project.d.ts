import { Request, Response } from "express";
import Base from "yennefer-suite-core/dist/server/controllers/base";
import { ProjectIter } from "../../../interactors/interfaces/project";
declare type ProjectCtrlType = {
    projectIter: ProjectIter;
};
export declare class ProjectCtrl extends Base {
    private projectIter;
    constructor({ projectIter }: ProjectCtrlType);
    private checkProjectAuth;
    private commonErrorException;
    getProjectAuthToken: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    createCookieByProjectOwnerId: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    activate: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    deactivate: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    delete: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    getActivatedList: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
    initWorkspace: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
}
declare const _default: ProjectCtrl;
export default _default;
