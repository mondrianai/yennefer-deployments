/// <reference types="node" />
import { ExecException } from "child_process";
export declare class ExecExceptionError extends Error implements ExecException {
    cmd?: string;
    killed?: boolean;
    code?: number;
    signal?: NodeJS.Signals;
    constructor(error: ExecException);
}
export declare function exec(command: string): Promise<string>;
declare const _default: {
    exec: typeof exec;
    ExecExceptionError: typeof ExecExceptionError;
};
export default _default;
