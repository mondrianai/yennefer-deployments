/**
 * Implement the python tornado cookie
 */
/**
 * Create the signature
 * @param  {Array} ary The array contains the unhashed values
 * @return {String} The cryptoed value
 */
export declare const createSignatureV1: (ary: Array<string>, secret: string) => string;
export declare const createSignatureV2: (s: string, secret: string) => string;
/**
 * Decode signed value version 1
 * @param  {String} name       Cookie name
 * @param  {String} value      Cookie value
 * @return {String}
 */
export declare const decodeSignedValueV1: (name: string, value: string, secret: string, days?: number) => string | null;
/**
 * Decode signed value version 2
 * @param  {String} name       Cookie name
 * @param  {String} value      Cookie value
 * @return {String}
 */
export declare const decodeSignedValueV2: (name: string, value: string, secret: string, days?: number) => string | null;
