import { ListOutput } from "yennefer-suite-core/dist/server/list-output";
import SpawnerDAO from "./dao/spawner";
import { ProjectId } from "../../../../core/domains/project";
import { UserId } from "../../../../core/domains/user";
import Base from "../../../../interactors/adapters/repositories/spawner";
export default class SpawnerRepo implements Base {
    spawnerDAO: typeof SpawnerDAO;
    constructor();
    isRunning: (projectId: ProjectId) => Promise<boolean>;
    mapActivatedProjectIds: (projectIds: Array<ProjectId>) => Promise<{}>;
    getActivatedProjectIdsByUserIds: (userIds: Array<UserId> | null) => Promise<{
        count: number;
        data: number[];
    }>;
    filterActivatedProjectIds: (projects: Array<ProjectId>) => Promise<ListOutput<number>>;
    getAllActivatedProjectIds: () => Promise<ListOutput<number>>;
}
