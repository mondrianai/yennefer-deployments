import { Model, Optional } from "sequelize";
import { Spawner } from "../../../../../core/domains/spawner";
interface SpawnerCreation extends Optional<Spawner, "id"> {
}
export declare class SpawnerDAO extends Model<Spawner, SpawnerCreation> {
    readonly id: number;
    readonly userId: number;
    readonly serverId: number;
    readonly state: string;
    readonly name: string;
    readonly started: Date;
    readonly lastActivity: Date;
    readonly userOptions: string;
    static associate(): void;
}
declare class DAO extends SpawnerDAO {
}
export default DAO;
