import Docker from "dockerode";
import { Container, ContainerType, StartType, StopType, RemoveType } from "../../../interactors/adapters/container";
export default class DockerContainer implements Container {
    instance: Docker;
    namePrefix?: string;
    constructor(parameters?: Partial<ContainerType>);
    start: (parameters: StartType) => Promise<any>;
    stop: (parameters: StopType) => Promise<any>;
    remove: (parameters: RemoveType) => Promise<void>;
    /**
     * 이름으로 컨테이너 필터링 해서 출력
     * @param userIdNumber {string} 사용자 ID
     * @param projectNumber {string} 프로젝트 ID
     * @returns 주피터 컨테이너
     */
    private getByName;
    /**
     * 이름으로 컨테이너 필터링 해서 출력
     * @param userIdNumber {string} 사용자 ID
     * @param projectNumber {string} 프로젝트 ID
     * @returns 주피터 컨테이너
     */
    private getContainerByUserIdAndProjectNumber;
}
