import { NotebookConverter, NbConvertOptions } from "../../../interactors/adapters/notebook-converter/base";
export declare type NbConverterType = {
    port: number;
    baseUrl: string;
    protocol: "http" | "https";
};
export declare class NbConverter implements NotebookConverter {
    convert(notebookPath: string, output: string, options?: NbConvertOptions): Promise<void>;
}
