import { AxiosBase } from "yennefer-suite-core/dist/server/services/core/base";
import { ResourceService, GetByIdIn, GetListIn, AssignResourceIn, FreeResourceIn, GetByIdOut, GetListOut, AssignResourceOut, FreeResourceOut } from "../../interactors/adapters/services/resource";
export declare class AxiosResourceService extends AxiosBase implements ResourceService {
    getById: (parameters: GetByIdIn) => Promise<GetByIdOut>;
    getList: (parameters: GetListIn) => Promise<GetListOut>;
    assignResource: (parameters: AssignResourceIn) => Promise<AssignResourceOut>;
    freeResource: (parameters: FreeResourceIn) => Promise<FreeResourceOut>;
}
export default AxiosResourceService;
