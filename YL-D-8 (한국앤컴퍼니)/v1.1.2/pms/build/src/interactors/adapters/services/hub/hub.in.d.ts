import { AssignedInfo, ContainerImage, ProjectId, VolumeInfos } from "../../../../core/domains/project";
import { UserId } from "../../../../core/domains/user";
export declare type LoginParams = UserId;
export declare type ActivateParams = {
    userId: UserId;
    projectId: ProjectId;
    image: ContainerImage;
    cookies: string;
    volumes?: VolumeInfos;
    assignedInfo?: AssignedInfo;
};
export declare type DeactivateParams = {
    userId: UserId;
    projectId: ProjectId;
    cookies: string;
    remove?: boolean;
};
