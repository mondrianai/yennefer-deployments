import { ActivateParams, DeactivateParams, LoginParams } from "./hub.in";
import { ActivateReturnType, DeactivateReturnType } from "./hub.out";
export interface HubService {
    activate(parameters: ActivateParams): Promise<ActivateReturnType>;
    deactivate(parameters: DeactivateParams): Promise<DeactivateReturnType>;
    login(parameters: LoginParams): Promise<void>;
}
export default HubService;
