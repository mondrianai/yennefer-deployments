/// <reference types="node" />
import { PathLike } from "fs";
export declare type NbConvertOptions = {
    to?: "asciidoc" | "custom" | "html" | "latex" | "markdown" | "notebook" | "pdf" | "python" | "rst" | "script" | "slides";
    outputDir?: string;
    template?: PathLike;
    config?: PathLike;
    stdout?: boolean;
};
export interface NotebookConverter {
    convert(notebookPath: string, output: string, options: NbConvertOptions): Promise<void>;
}
