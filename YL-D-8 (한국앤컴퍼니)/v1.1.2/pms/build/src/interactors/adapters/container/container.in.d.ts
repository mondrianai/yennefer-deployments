export declare type StartType = {
    name: string;
    options?: {};
};
export declare type StopType = {
    name: string;
    options?: {};
};
export declare type RemoveType = {
    userId: number;
    name: string;
    options?: {};
};
