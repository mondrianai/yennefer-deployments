import { StartType, StopType, RemoveType } from "./container.in";
export declare type ConnectionProtocol = "http" | "https" | "ssh";
export declare type ContainerType = {
    host: string;
    port: number;
    protocol: ConnectionProtocol;
    username: string;
    prefix: string;
};
export interface Container {
    start: (parameters: StartType) => Promise<unknown>;
    stop: (parameters: StopType) => Promise<unknown>;
    remove: (parameters: RemoveType) => Promise<unknown>;
}
