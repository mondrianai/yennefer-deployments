import { User as HubUser } from "../../../../core/domains/hub-user";
import { GetHubUserByUserIdParams, UpdateLastActivityParams } from "./hub-user.in";
export interface HubUserRepo {
    getHubUserByUserId(parameters: GetHubUserByUserIdParams): Promise<HubUser | null>;
    updateLastActivity(parameters: UpdateLastActivityParams): Promise<HubUser>;
}
export default HubUserRepo;
