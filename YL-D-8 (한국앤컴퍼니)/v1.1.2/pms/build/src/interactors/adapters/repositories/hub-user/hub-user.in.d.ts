export declare type GetHubUserByUserIdParams = string;
export declare type UpdateLastActivityParams = {
    userId: string;
    date: Date;
};
