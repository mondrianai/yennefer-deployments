import { ProjectId } from "../core/domains/project";
import { ProjectIter, ProjectIterType, ActivateParams, DeactivateParams, DeleteParams, GetActivatedProjectIdsByUserId, MapActivatedProjectIdsParams, CreateCookieForHubAuthParams } from "./interfaces/project";
export declare class ProjectIntrImpl implements ProjectIter {
    private container;
    private hubUserRepo;
    private spawnerRepo;
    private hubService;
    constructor({ hubUserRepo, spawnerRepo, hubService, container, }: ProjectIterType);
    private getJhubCookieSecret;
    activate: (parameters: ActivateParams) => Promise<true>;
    deactivate: (parameters: DeactivateParams) => Promise<true>;
    delete: (parameters: DeleteParams) => Promise<boolean>;
    clearDatasets: (parameters: {
        projectId: ProjectId;
    }) => Promise<unknown[]>;
    mapActivatedProjectIds: (parameters: MapActivatedProjectIdsParams) => Promise<{
        [key: string]: boolean;
    }>;
    makeRequiredFolder: (path: string) => Promise<void>;
    changeFolderToAccessable: (path: string) => Promise<void>;
    getIPynbFileList: (parameters: {
        path: string;
    }) => Promise<string[]>;
    getActivatedProjectIdsByUserId: (parameters: GetActivatedProjectIdsByUserId) => Promise<import("yennefer-suite-core/dist/server/list-output").ListOutput<number>>;
    createCookieForHubAuth: (parameters: CreateCookieForHubAuthParams) => Promise<string | null>;
}
declare const _default: ProjectIntrImpl;
export default _default;
