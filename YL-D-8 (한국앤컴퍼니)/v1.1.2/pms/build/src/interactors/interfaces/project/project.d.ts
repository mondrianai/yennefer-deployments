import ListOutput from "yennefer-suite-core/dist/server/list-output";
import { ActivateParams, DeactivateParams, DeleteParams, GetIPynbFileListParams, MapActivatedProjectIdsParams, CreateCookieForHubAuthParams, GetActivatedProjectIdsByUserId } from "./project.in";
import { HubUserRepo } from "../../adapters/repositories/hub-user";
import { SpawnerRepo } from "../../adapters/repositories/spawner";
import { HubService } from "../../adapters/services/hub";
import { Container } from "../../adapters/container";
export interface ProjectIter {
    activate: (parameters: ActivateParams) => Promise<boolean>;
    deactivate: (parameters: DeactivateParams) => Promise<boolean>;
    delete: (parameters: DeleteParams) => Promise<boolean>;
    getIPynbFileList(parameters: GetIPynbFileListParams): Promise<Array<string>>;
    getActivatedProjectIdsByUserId(parameters: GetActivatedProjectIdsByUserId): Promise<ListOutput<number>>;
    changeFolderToAccessable(path: string): Promise<void>;
    makeRequiredFolder(path: string): Promise<void>;
    mapActivatedProjectIds(parameters: MapActivatedProjectIdsParams): Promise<{
        [key: string]: boolean;
    }>;
    createCookieForHubAuth(parameters: CreateCookieForHubAuthParams): Promise<string | null>;
}
export declare type ProjectIterType = {
    hubUserRepo: HubUserRepo;
    spawnerRepo: SpawnerRepo;
    hubService: HubService;
    container: Container;
};
