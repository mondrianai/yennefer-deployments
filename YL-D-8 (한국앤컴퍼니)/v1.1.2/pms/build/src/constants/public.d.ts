export declare const CORS_WHITE_LIST: string[];
export declare const SERVER_PORT: number;
export declare const NODE_NAME: string;
export declare const HUB_BASE_URL: string;
declare const _default: {
    readonly CORS_WHITE_LIST: string[];
    readonly NODE_NAME: string;
    readonly SERVER_PORT: number;
    readonly HUB_BASE_URL: string;
};
export default _default;
