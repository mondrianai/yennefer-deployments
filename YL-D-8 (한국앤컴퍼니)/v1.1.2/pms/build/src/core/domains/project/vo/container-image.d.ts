export declare class ContainerImage {
    readonly value: string;
    constructor(parameter: string);
    static validate(object: any): boolean;
}
export default ContainerImage;
