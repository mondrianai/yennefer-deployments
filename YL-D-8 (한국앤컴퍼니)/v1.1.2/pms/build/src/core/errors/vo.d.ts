export declare class DomainModelValidationError extends Error {
    constructor(object: string);
}
export default DomainModelValidationError;
