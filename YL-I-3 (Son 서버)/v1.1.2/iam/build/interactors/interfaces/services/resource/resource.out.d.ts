import { Resource } from "yennefer-suite-core/dist/domains/studio/resource";
import ListOutput from "yennefer-suite-core/dist/server/list-output";
export declare type GetByIdOut = Promise<Resource | null>;
export declare type GetListOut = ListOutput<Resource>;
export declare type AssignResourceOut = {
    cpu: any;
    gpu: any;
    ram: any;
};
export declare type FreeResourceOut = boolean;
