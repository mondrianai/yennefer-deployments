import { UserType } from "@domains/user";
import { WithdrawalUserType } from "@domains/user/withdrawal-user";
export declare type WithdrawalUserOut = Omit<WithdrawalUserType, "userId"> & Pick<UserType, "email" | "name" | "id" | "role">;
