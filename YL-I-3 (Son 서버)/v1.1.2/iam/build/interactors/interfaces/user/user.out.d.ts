import { User } from "@domains/user";
export declare type UserOut = {
    count: number;
    users: Array<User>;
};
