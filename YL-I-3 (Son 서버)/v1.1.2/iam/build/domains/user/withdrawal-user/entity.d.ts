import { UserId } from "../vo/id";
export declare type WithdrawalUserType = {
    userId: number;
    isForced: boolean;
    date: Date;
};
export declare class WithdrawalUser {
    readonly userId: UserId;
    isForced: boolean;
    date: Date;
    constructor(parameters: WithdrawalUserType);
    get value(): WithdrawalUserType;
}
export default WithdrawalUser;
