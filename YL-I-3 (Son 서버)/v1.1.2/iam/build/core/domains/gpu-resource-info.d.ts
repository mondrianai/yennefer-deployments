export interface GPUResourceInfoParmas {
    id: number;
    nodeName: string;
    deviceUUID: string;
    deviceName: string;
    order: number;
}
export default class GPUResourceInfo {
    id: number;
    nodeName: string;
    deviceName: string;
    deviceUUID: string;
    order: number;
    constructor({ id, nodeName, deviceName, deviceUUID, order, }: GPUResourceInfoParmas);
}
