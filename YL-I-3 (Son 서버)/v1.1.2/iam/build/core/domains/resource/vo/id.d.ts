export declare class ResourceId {
    readonly value: number;
    constructor(value: number);
    static validate(object: any): boolean;
}
export default ResourceId;
