import { FindOptions } from "sequelize";
import { PaginationSearches } from "@domains/pagination";
declare const convertPaginationToFindOptionsSearches: <T>(pagination: PaginationSearches<T>, defaultOptions?: FindOptions<T>, isOrdered?: boolean, isSearch?: boolean) => FindOptions<any>;
export default convertPaginationToFindOptionsSearches;
