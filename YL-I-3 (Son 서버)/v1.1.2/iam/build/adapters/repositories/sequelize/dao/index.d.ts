import UserDAOTemp from "./user";
import WithdrawalUserTemp from "./withdrawal-user";
declare const models: {
    user: typeof UserDAOTemp;
    withdrawalUser: typeof WithdrawalUserTemp;
};
export declare class UserDAO extends UserDAOTemp {
}
export declare class WithdrawalUserDAO extends WithdrawalUserTemp {
}
export default models;
