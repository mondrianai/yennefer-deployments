import { CreateOptions } from "sequelize";
import { UserType, User, UserId } from "@domains/user";
import { Pagination, PaginationSearches } from "@domains/pagination";
import { CreateUserIn, DeleteUserIn, GetUserIn, GetUserListIn, SaveUserIn, UserRepositoryIface as Base } from "@interactors/interfaces";
import { UserDAO } from "../dao";
import { Options } from "..";
declare class UserRepository implements Base {
    createUser: (user: CreateUserIn, optinos?: CreateOptions<any> | undefined) => Promise<User>;
    getUserByProperty: <T extends keyof UserType>(parameter: Required<UserType>[T], condition: T) => Promise<User | null>;
    getUserListByProperty: <T extends keyof UserType>(pagination: Pagination<User>, user?: Partial<Pick<GetUserListIn, T>> | undefined, condition?: T | T[] | "none" | undefined) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByProperties: (pagination: Pagination<User>, users?: Partial<GetUserListIn>[] | undefined, condition?: "id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces" | "none" | ("id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces")[] | undefined) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByPropertySearches: <T extends keyof UserType>(pagination: PaginationSearches<User>, user?: Partial<Pick<GetUserListIn, T>> | undefined, condition?: "none" | T | T[] | undefined) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    getUserListByPropertiesSearches: (pagination: PaginationSearches<User>, users?: Partial<GetUserListIn>[] | undefined, condition?: "id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces" | "none" | ("id" | "password" | "name" | "email" | "lastLoginTime" | "phone" | "position" | "role" | "status" | "createdAt" | "updatedAt" | "approvedAt" | "deletedAt" | "gpuResoreces")[] | undefined) => Promise<{
        count: number;
        users: Array<User>;
    }>;
    saveUser: (user: SaveUserIn, options?: Options | undefined) => Promise<User>;
    deleteUserFromAdmin: (user: DeleteUserIn, options?: Options | undefined) => Promise<User>;
    deleteUser: (userId: UserId) => Promise<number>;
    convertToEntity: (dao: UserDAO) => User;
    convertToEntities: (daoes: Array<UserDAO>) => Array<User>;
}
declare const _default: UserRepository;
export default _default;
