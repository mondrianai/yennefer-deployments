import SequelizeUserRepository from "./sequelize/pg/user";
import SequelizeWithdrawalUserRepository from "./sequelize/pg/withdrawal-user";
import { TransactionType } from "./sequelize/transaction";
export * from "./sequelize/transaction";
export declare type Options = {
    transaction?: TransactionType;
};
export declare const UserRepository: typeof SequelizeUserRepository;
export declare const WithdrawalUserRepository: typeof SequelizeWithdrawalUserRepository;
