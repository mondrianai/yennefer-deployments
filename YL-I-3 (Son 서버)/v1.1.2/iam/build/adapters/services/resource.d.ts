import GPUResourceInfo from "@core/domains/gpu-resource-info";
import { ResourceService } from "@interactors/interfaces/services/resource";
import { AxiosBase } from "yennefer-suite-core/dist/server/services/core/base";
export default class AxiosResourceService extends AxiosBase implements ResourceService {
    updateUserGpuResources(userId: number, deviceIds: number[], token: string): Promise<{
        userId: number;
        updatedAllocationMap: number[];
    }>;
    getGPUResourcesByUserId(userId: number, { token }: {
        token: string;
    }): Promise<{
        count: number;
        rows: GPUResourceInfo[];
    }>;
}
